FROM ubuntu:24.04

RUN apt-get update \
 && apt-get install -y \
      bash-completion \
      build-essential \
      curl \
      gdb \
      git \
      lcov \
      pipx \
      python3 \
      python-is-python3 \
      software-properties-common \
      sudo \
      unzip \
      wget \
 && rm -rf /var/lib/apt/lists/*

RUN wget -qO- https://apt.llvm.org/llvm-snapshot.gpg.key | tee /etc/apt/trusted.gpg.d/apt.llvm.org.asc
RUN add-apt-repository deb http://apt.llvm.org/noble/ llvm-toolchain-noble-19 main \
RUN apt-get update \
 && apt-get install -y \
      clang-19 \
      clang-format-19 \
      clang-tidy-19 \
      clang-tools-19 \
      clangd-19 \
      lld-19 \
      lldb-19 \
 && rm -rf /var/lib/apt/lists/* 

RUN update-alternatives \
      --install /usr/bin/clang              clang              /usr/bin/clang-19 100          \
      --slave   /usr/bin/clang-scan-deps    clang-scan-deps    /usr/bin/clang-scan-deps-19    \
      --slave   /usr/bin/clang++            clang++            /usr/bin/clang++-19            \
      --slave   /usr/bin/clangd             clangd             /usr/bin/clangd-19             \
      --slave   /usr/bin/clang-format       clang-format       /usr/bin/clang-format-19       \
      --slave   /usr/bin/clang-tidy         clang-tidy         /usr/bin/clang-tidy-19         \
      --slave   /usr/bin/lld                lld                /usr/bin/lld-19                \
      --slave   /usr/bin/lldb               lldb               /usr/bin/lldb-19               \
      --slave   /usr/bin/llvm-cov           llvm-cov           /usr/bin/llvm-cov-19          \
      --slave   /usr/bin/llvm-profdata      llvm-profdata      /usr/bin/llvm-profdata-19     \
      --slave   /usr/bin/clang-tidy-diff    clang-tidy-diff    /usr/bin/clang-tidy-diff-19.py \
      --slave   /usr/bin/run-clang-tidy     run-clang-tidy     /usr/bin/run-clang-tidy-19.py

RUN add-apt-repository -y ppa:ubuntu-toolchain-r/test \
 && apt-get update \
 && apt-get install -y \
      gcc-14 \
      g++-14 \
 && rm -rf /var/lib/apt/lists/*  

RUN update-alternatives \
      --install /usr/bin/gcc  gcc  /usr/bin/gcc-14 100 \
      --slave   /usr/bin/g++  g++  /usr/bin/g++-14

RUN mkdir -p /tmp/ccache \
 && curl -L https://github.com/ccache/ccache/releases/download/v4.10.2/ccache-4.10.2-linux-x86_64.tar.xz | tar xJ --strip-components=1  --directory=/tmp/ccache \
 && cp /tmp/ccache/ccache /usr/local/bin/ \
 && rm -r /tmp/ccache

RUN curl -L https://github.com/Kitware/CMake/releases/download/v3.30.5/cmake-3.30.5-linux-x86_64.tar.gz | tar xz --strip-components=1 --directory=/usr/local

RUN curl -s -L -o /tmp/ninja-linux.zip https://github.com/ninja-build/ninja/releases/download/v1.12.1/ninja-linux.zip  \
 && unzip /tmp/ninja-linux.zip -d /usr/local/bin \
 && rm /tmp/ninja-linux.zip

RUN deluser --remove-home ubuntu
RUN useradd -ms /bin/bash -G sudo devel

RUN echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

USER devel

RUN pipx install conan
ENV PATH="/home/devel/.local/bin:$PATH"

RUN cd /tmp \
 && git clone https://github.com/eriwen/lcov-to-cobertura-xml.git \
 && cd lcov-to-cobertura-xml \
 && git fetch https://github.com/chinloyal/lcov-to-cobertura-xml.git chinloyal-patch-1 \
 && git cherry-pick -n cfbcabf2adcf1d43e9e45fd2efe22f3f94a9c6bc \
 && cp lcov_cobertura/lcov_cobertura.py /home/devel/.local/bin/lcov_cobertura \
 && cd .. \
 && rm -rf lcov-to-cobertura-xml

ENV CC=clang CXX=clang++

ENV CMAKE_CXX_COMPILER_LAUNCHER=ccache
RUN mkdir -p ~/.cache/ccache

