#pragma once

#include <libassert/assert.hpp>

#include <concepts>
#include <limits>
#include <type_traits>

template <std::integral Dst, std::integral Src> constexpr Dst integerCast(Src const &x) {
  DEBUG_ASSERT(std::in_range<Dst>(x), x, std::numeric_limits<Dst>::lowest(),
               std::numeric_limits<Dst>::max());
  return static_cast<Dst>(x);
}

template <std::integral Src> constexpr auto castToSigned(Src const &x) {
  return integerCast<std::make_signed_t<Src>>(x);
}

template <std::integral Src> constexpr auto castToUnsigned(Src const &x) {
  return integerCast<std::make_unsigned_t<Src>>(x);
}