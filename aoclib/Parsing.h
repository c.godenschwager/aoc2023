#pragma once

#include <libassert/assert.hpp>

#include <algorithm>
#include <charconv>
#include <concepts>
#include <iterator>
#include <ranges>
#include <spanstream>
#include <string_view>
#include <vector>

template <typename... T> void parse(std::string_view const s, T &...values) {
  std::ispanstream iss(s);
  (iss >> ... >> values);
}

template <typename T> std::vector<T> parseRange(std::string_view const s) {
  std::ispanstream iss(s);
  return {std::istream_iterator<T>(iss), std::istream_iterator<T>{}};
}

template <typename T, typename OutIt> OutIt parseRange(std::string_view const s, OutIt out) {
  std::ispanstream iss(s);
  return std::copy(std::istream_iterator<T>(iss), std::istream_iterator<T>{}, out);
}

template <std::integral T> T parseInt(std::string_view const s) {
  T out{};
  auto [ptr, ec] = std::from_chars(s.begin(), s.end(), out);
  ASSUME(ec == std::errc(), s);
  return out;
}

template <std::integral T> T parseInt(std::ranges::input_range auto &&r) {
  std::string_view s(r);
  return parseInt<T>(s);
}

template <std::integral T> T parseHexInt(std::string_view const s) {
  T out{};
  auto [ptr, ec] = std::from_chars(s.begin(), s.end(), out, 16);
  ASSUME(ec == std::errc(), s);
  return out;
}

template <std::integral T>
std::vector<T> parseIntegerRange(std::string_view const s, char const delim = ',') {
  std::vector<T> result;
  for (auto it = s.begin(); it < s.end(); ++it) {
    auto startIt = it;
    it = std::find(startIt, s.end(), delim);
    result.push_back(parseInt<T>({startIt, it}));
  }
  return result;
}

inline std::vector<std::string_view> split(std::string_view const s, char const delim = ',') {
  std::vector<std::string_view> result;
  for (auto it = s.begin(); it < s.end(); ++it) {
    auto startIt = it;
    it = std::find(startIt, s.end(), delim);
    result.emplace_back(startIt, it);
  }
  return result;
}