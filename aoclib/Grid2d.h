#pragma once

#include "Vector.h"

#include <absl/container/flat_hash_map.h>
#include <absl/hash/hash.h>
#include <experimental/mdspan>
#include <fmt/ostream.h>
#include <libassert/assert.hpp>

#include <array>
#include <iosfwd>
#include <limits>
#include <vector>

namespace stdex = std::experimental;

template <typename T> class Grid2d;

template <typename T> class Grid2dSpan {
public:
  static constexpr size_t numDimensions = 2u;
  using Span = stdex::mdspan<T, stdex::dextents<int, numDimensions>, stdex::layout_stride>;
  using ElementType = Span::element_type;
  using ExtentsType = Span::extents_type;
  using IndexType = Span::index_type;
  using Coord = Vec2<IndexType>;
  using SizeType = Span::size_type;
  using MappingType = Span::mapping_type;

  Grid2dSpan() = default;
  Grid2dSpan(ElementType *p, auto const xSize, auto const ySize)
      : _span(p,
              MappingType{ExtentsType{integerCast<IndexType>(xSize), integerCast<IndexType>(ySize)},
                          std::array<IndexType, 2u>{1, integerCast<IndexType>(xSize)}}) {}

  Grid2dSpan(ElementType *p, auto const xSize, auto const ySize, auto const xStride,
             auto const yStride)
      : _span(p,
              MappingType{ExtentsType{integerCast<IndexType>(xSize), integerCast<IndexType>(ySize)},
                          std::array<IndexType, 2u>{integerCast<IndexType>(xStride),
                                                    integerCast<IndexType>(yStride)}}) {}

  Grid2dSpan(ElementType *p, auto const xSize, auto const ySize, auto const yStride)
      : _span(p,
              MappingType{ExtentsType{integerCast<IndexType>(xSize), integerCast<IndexType>(ySize)},
                          std::array<IndexType, 2u>{1, integerCast<IndexType>(yStride)}}) {}

  Grid2dSpan(ElementType *p, MappingType const &mapping) : _span(p, mapping) {}

  template <typename U> Grid2dSpan(Grid2dSpan<U> const &other) : _span(other.span()) {}

  [[nodiscard]] ElementType &operator[](std::integral auto const x,
                                        std::integral auto const y) const {
    return _span[x, y];
  }
  template <std::integral U> [[nodiscard]] ElementType &operator[](Vec2<U> const &x) const {
    return _span[x.x(), x.y()];
  }

  [[nodiscard]] IndexType xSize() const { return _span.extent(0); }
  [[nodiscard]] IndexType ySize() const { return _span.extent(1); }
  [[nodiscard]] Vec2<IndexType> size() const { return {_span.extent(0), _span.extent(1)}; }

  [[nodiscard]] Grid2dSpan column(IndexType const x) const {
    return {&_span[x, 0], 1, ySize(), _span.mapping().strides()[0], _span.mapping().strides()[1]};
  }
  [[nodiscard]] Grid2dSpan line(IndexType const y) const {
    return {&_span[0, y], xSize(), 1, 1, _span.mapping().strides()[1]};
  }

  template <class F> void forEach(F f) const {
    for (int y = 0; y < ySize(); ++y)
      for (int x = 0; x < xSize(); ++x)
        f(_span[x, y]);
  }

  void fill(ElementType const &e) const;

  [[nodiscard]] Coord find(ElementType const &e) const;
  [[nodiscard]] std::vector<Coord> findAll(ElementType const &e) const;

  [[nodiscard]] auto map(auto &&mapFunc) const -> Grid2d<decltype(mapFunc(' '))>;
  [[nodiscard]] size_t count(ElementType const &e) const;
  [[nodiscard]] size_t count_if(auto predicate) const;

  [[nodiscard]] ElementType max() const;
  [[nodiscard]] ElementType sum() const;

  [[nodiscard]] Span const &span() const { return _span; }
  [[nodiscard]] Grid2dSpan subgrid(Coord const &begin, Coord const &end) const {
    return {&((*this)[begin]), end.x() - begin.x(), end.y() - begin.y(), _span.stride(1u)};
  }

  [[nodiscard]] Grid2dSpan flipX() const {
    return {&(*this)[xSize() - 1, 0], xSize(), ySize(), -_span.stride(0), _span.stride(1)};
  }

  [[nodiscard]] Grid2dSpan flipY() const {
    return {&(*this)[0, ySize() - 1], xSize(), ySize(), _span.stride(0), -_span.stride(1)};
  }

private:
  Span _span;
};

template <typename T> class Grid2d {
public:
  using SpanType = Grid2dSpan<T>;
  using ElementType = SpanType::ElementType;
  using IndexType = SpanType::IndexType;
  using Coord = SpanType::Coord;
  using SizeType = SpanType::SizeType;

  Grid2d() = default;

  Grid2d(std::integral auto const xSize, std::integral auto const ySize)
      : _data(xSize * ySize), _span(_data.data(), xSize, ySize) {}

  Grid2d(std::integral auto const xSize, std::integral auto const ySize, ElementType const &init)
      : _data(integerCast<size_t>(xSize * ySize), init), _span(_data.data(), xSize, ySize) {}

  Grid2d(std::integral auto const xSize, std::integral auto const ySize, ElementType const &init,
         std::integral auto const numGhostLayers)
      : _data(integerCast<size_t>((xSize + 2 * numGhostLayers) * (ySize + 2 * numGhostLayers)),
              init),
        _span(&_data[numGhostLayers * (xSize + 2 * numGhostLayers) + numGhostLayers], xSize, ySize,
              xSize + 2 * numGhostLayers) {}

  template <std::integral U> Grid2d(Vec2<U> const &size) : Grid2d(size.x(), size.y()) {}

  template <std::integral U>
  Grid2d(Vec2<U> const &size, ElementType const &init) : Grid2d(size.x(), size.y(), init) {}

  template <std::integral U>
  Grid2d(Vec2<U> const &size, ElementType const &init, auto const numGhostLayers)
      : Grid2d(size.x(), size.y(), init, numGhostLayers) {}

  Grid2d(Grid2d const &other)
      : _data(other._data),
        _span(_data.data() + other.pointerOffset(), other.span().span().mapping()) {} // NOLINT

  Grid2d(Grid2d &&other) noexcept : _data(std::move(other._data)), _span(std::move(other._span)) {
    other._span = SpanType{};
  }

  Grid2d &operator=(Grid2d const &other) {
    _data = other._data;
    _span = SpanType(_data.data() + other.pointerOffset(), other.span().span().mapping());
    return *this;
  }

  Grid2d &operator=(Grid2d &&other) noexcept {
    _data = std::move(other._data);
    _span = std::move(other._span);
    other._span = SpanType{};
    return *this;
  }

  ~Grid2d() = default;

  operator Grid2dSpan<T const>() const { return _span; }
  operator Grid2dSpan<T>() { return _span; }

  [[nodiscard]] ElementType &operator[](std::integral auto const x, std::integral auto const y) {
    return _span[x, y];
  }
  [[nodiscard]] ElementType const &operator[](std::integral auto const x,
                                              std::integral auto const y) const {
    return _span[x, y];
  }

  template <std::integral U> [[nodiscard]] ElementType &operator[](Vec2<U> const &x) {
    return _span[x.x(), x.y()];
  }
  template <std::integral U> [[nodiscard]] ElementType const &operator[](Vec2<U> const &x) const {
    return _span[x.x(), x.y()];
  }

  [[nodiscard]] IndexType xSize() const { return _span.xSize(); }
  [[nodiscard]] IndexType ySize() const { return _span.ySize(); }
  [[nodiscard]] Vec2<IndexType> size() const { return _span.size(); }

  [[nodiscard]] bool inBounds(Coord const &x) const {
    return x.x() >= 0 && x.y() >= 0 && x.x() < xSize() && x.y() < ySize();
  }

  [[nodiscard]] SpanType column(IndexType const x) const { return _span.column(x); }
  [[nodiscard]] SpanType line(IndexType const y) const { return _span.line(y); }

  void fill(ElementType const &e) { return _span.fill(e); }

  [[nodiscard]] Coord find(ElementType const &e) const { return _span.find(e); }
  [[nodiscard]] std::vector<Coord> findAll(ElementType const &e) const { return _span.findAll(e); }

  [[nodiscard]] SpanType const &span() const { return _span; }

  [[nodiscard]] auto map(auto &&mapFunc) const -> Grid2d<decltype(mapFunc(' '))> {
    return _span.map(std::forward<decltype(mapFunc)>(mapFunc));
  }
  [[nodiscard]] size_t count(ElementType const &e) const { return _span.count(e); }
  [[nodiscard]] size_t count_if(auto predicate) const { return _span.count_if(predicate); }
  [[nodiscard]] ElementType max() const { return _span.max(); }
  [[nodiscard]] ElementType sum() const { return _span.sum(); }

  [[nodiscard]] Grid2dSpan<T> subgrid(Coord const &begin, Coord const &end) const {
    return _span.subgrid(begin, end);
  }

  [[nodiscard]] Grid2dSpan<T> flipY() const { return _span.flipY(); }
  [[nodiscard]] Grid2dSpan<T> flipX() const { return _span.flipX(); }

private:
  [[nodiscard]] size_t pointerOffset() const { return _span.span().data_handle() - _data.data(); }
  std::vector<T> _data;
  SpanType _span;
};

template <typename T> bool operator==(Grid2dSpan<T> const &lhs, Grid2dSpan<T> const &rhs) {
  if (lhs.xSize() != rhs.xSize() || lhs.ySize() != rhs.ySize())
    return false;

  using IndexType = Grid2dSpan<T>::IndexType;
  for (IndexType y = 0; y < lhs.ySize(); ++y)
    for (IndexType x = 0; x < lhs.xSize(); ++x)
      if (lhs[x, y] != rhs[x, y])
        return false;

  return true;
}

template <typename T> bool operator==(Grid2d<T> const &lhs, Grid2d<T> const &rhs) {
  return lhs.span() == rhs.span();
}

template <typename T> bool operator<(Grid2dSpan<T> const &lhs, Grid2dSpan<T> const &rhs) {
  if (lhs.xSize() != rhs.xSize() || lhs.ySize() != rhs.ySize())
    return false;

  using IndexType = Grid2dSpan<T>::IndexType;
  for (IndexType y = 0; y < lhs.ySize(); ++y)
    for (IndexType x = 0; x < lhs.xSize(); ++x) {
      if (lhs[x, y] < rhs[x, y])
        return true;
      if (lhs[x, y] > rhs[x, y])
        return false;
    }
  return false;
}

template <typename T> bool operator<(Grid2d<T> const &lhs, Grid2d<T> const &rhs) {
  return lhs.span() < rhs.span();
}

template <typename H, typename T> H AbslHashValue(H h, Grid2dSpan<T> const &s) {
  H state = H::combine(std::move(h), s.xSize(), s.ySize());
  using IndexType = Grid2dSpan<T>::IndexType;
  for (IndexType y = 0; y < s.ySize(); ++y)
    for (IndexType x = 0; x < s.xSize(); ++x)
      state = H::combine(std::move(state), s[x, y]);
  return state;
}

template <typename H, typename T> H AbslHashValue(H h, Grid2d<T> const &s) {
  return AbslHashValue(std::move(h), s.span());
}

template <typename T> void Grid2dSpan<T>::fill(ElementType const &e) const {
  forEach([&e](ElementType &ee) { ee = e; });
}

template <typename T> auto Grid2dSpan<T>::find(ElementType const &e) const -> Coord {
  for (Coord c{0, 0}; c[1] < ySize(); ++c[1])
    for (c[0] = 0; c[0] < xSize(); ++c[0])
      if ((*this)[c] == e)
        return c;

  return {xSize(), ySize()};
}

template <typename T>
auto Grid2dSpan<T>::findAll(ElementType const &e) const -> std::vector<Coord> {
  std::vector<Coord> result;
  for (Coord c{0, 0}; c[1] < ySize(); ++c[1])
    for (c[0] = 0; c[0] < xSize(); ++c[0])
      if ((*this)[c] == e)
        result.push_back(c);

  return result;
}

template <typename T>
auto Grid2dSpan<T>::map(auto &&mapFunc) const -> Grid2d<decltype(mapFunc(' '))> {
  Grid2d<decltype(mapFunc(' '))> result(xSize(), ySize());
  for (int y = 0; y < ySize(); ++y)
    for (int x = 0; x < xSize(); ++x)
      result[x, y] = mapFunc(_span[x, y]);
  return result;
}

template <typename T> size_t Grid2dSpan<T>::count_if(auto predicate) const {
  size_t ctr = 0;
  forEach([&](ElementType const &e) {
    if (predicate(e))
      ++ctr;
  });
  return ctr;
}

template <typename T> size_t Grid2dSpan<T>::count(ElementType const &e) const {
  return count_if([&e](ElementType const &ee) { return ee == e; });
}

template <typename T> auto Grid2dSpan<T>::max() const -> ElementType {
  ElementType max = _span[0, 0];
  forEach([&max](ElementType const &e) {
    if (e > max)
      max = e;
  });

  return max;
}

template <typename T> auto Grid2dSpan<T>::sum() const -> ElementType {
  ElementType sum = ElementType();
  forEach([&sum](ElementType const &e) { sum += e; });

  return sum;
}

std::ostream &operator<<(std::ostream &oss, Grid2dSpan<char> const &span);
inline std::ostream &operator<<(std::ostream &oss, Grid2d<char> const &grid) {
  return oss << grid.span();
}

template <> struct fmt::formatter<Grid2dSpan<char>> : ostream_formatter {};
template <> struct fmt::formatter<Grid2d<char>> : ostream_formatter {};

template <typename T> class SparseGrid2d {
public:
  using ElementType = T;
  using IndexType = int;
  using Coord = Vec2<IndexType>;
  using SizeType = size_t;

  SparseGrid2d(ElementType const &defaultValue) : _defaultValue(defaultValue) {}

  [[nodiscard]] ElementType operator[](auto const x, auto const y) const { (*this)[{x, y}]; }
  [[nodiscard]] ElementType operator[](Coord const &x) const {
    auto it = _map.find(x);
    return it == _map.end() ? _defaultValue : it->second;
  }

  void set(Coord const &x, ElementType const &value) { _map[x] = value; }
  void set(int const x, int const y, ElementType const &value) { _map[{x, y}] = value; }

  [[nodiscard]] std::pair<Coord, Coord> nonDefaultCoordsInterval() const {
    Coord min(std::numeric_limits<IndexType>::max());
    Coord max(std::numeric_limits<IndexType>::min());
    for (auto const &[coord, val] : _map) {
      min = elementwiseMin(min, coord);
      max = elementwiseMax(max, coord);
    }

    return {min, max};
  }

  [[nodiscard]] std::tuple<Grid2d<T>, Coord> toGrid2d() const {
    auto const [min, max] = nonDefaultCoordsInterval();
    Coord const size = max - min + 1;

    Grid2d<T> grid(size.x(), size.y(), _defaultValue);
    for (auto const &[coord, val] : _map)
      grid[coord - min] = val;
    return {std::move(grid), min};
  }

private:
  ElementType _defaultValue;
  absl::flat_hash_map<Coord, ElementType> _map;
};

std::ostream &operator<<(std::ostream &oss, SparseGrid2d<char> const &span);

template <> struct fmt::formatter<SparseGrid2d<char>> : ostream_formatter {};