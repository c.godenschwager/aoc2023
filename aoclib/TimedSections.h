#pragma once
#include "Timer.h"

#include <fmt/format.h>

#include <map>
#include <string>
#include <string_view>

class TimedSections {
public:
  void start(std::string_view const name) { _timers.emplace(name, Timer{}); }
  void stop(std::string_view const name) {
    auto it = _timers.find(name);
    if (it == _timers.end())
      return;
    auto elapsed = it->second.elapsed();
    fmt::println("Section \"{}\" took {}.", it->first, Timer::formatDuration(elapsed));
  }

private:
  std::map<std::string, Timer, std::less<>> _timers;
};