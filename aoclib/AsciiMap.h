#pragma once

#include <IntegerCast.h>

#include <libassert/assert.hpp>

#include <array>

template <typename map_to> class AsciiMap {
public:
  using MapTo = map_to;

  constexpr AsciiMap(MapTo const &defaultValue) {
    std::fill(_map.begin(), _map.end(), defaultValue);
  }

  [[nodiscard]] constexpr MapTo const &operator[](char const c) const {
    DEBUG_ASSERT(c >= 0);
    DEBUG_ASSERT(c < _map.size());
    return _map[c];
  }

  [[nodiscard]] constexpr MapTo &operator[](char const c) {
    DEBUG_ASSERT(c >= 0);
    DEBUG_ASSERT(c < _map.size());
    return _map[c];
  }

private:
  std::array<MapTo, 128u> _map;
};