#include "LinewiseInput.h"
#include "Grid2d.h"

#include <algorithm>
#include <ranges>

LinewiseInput::LinewiseInput(std::string_view const text) {
  for (auto it = text.begin(); it != text.end();) {
    auto lineStart = it;
    it = std::find(it, text.end(), '\n');
    _lines.emplace_back(lineStart, it);
    if (it != text.end())
      ++it;
  }
}

std::vector<std::span<std::string_view const>> LinewiseInput::splitOnEmptyLines() const {
  std::vector<std::span<std::string_view const>> result;

  for (auto it = _lines.begin(); it != _lines.end();) {
    while (it->empty() && it != _lines.end())
      ++it;
    auto startIt = it;
    it = std::find_if(it, _lines.end(), [](std::string_view const s) { return s.empty(); });
    result.emplace_back(startIt, it);
  }
  return result;
}

bool LinewiseInput::allLinesOfSameSize() const {
  if (_lines.empty())
    return true;
  return std::all_of(std::next(_lines.begin()), _lines.end(), [this](std::string_view const s) {
    return s.size() == _lines.front().size();
  });
}

Grid2d<char> makeCharGrid2d(std::span<std::string_view const> const lines) {
  size_t const xSize = lines.front().size();
  size_t const ySize = lines.size();
  Grid2d<char> result(xSize, ySize);

  Grid2d<char>::IndexType y = 0;
  std::ranges::for_each(std::ranges::reverse_view(lines), [&](std::string_view line) {
    DEBUG_ASSERT(line.size() == xSize);
    for (size_t x = 0; x < xSize; ++x)
      result[x, y] = line[x];
    ++y;
  });

  return result;
}

Grid2d<char> LinewiseInput::makeCharGrid2d() const { return ::makeCharGrid2d(_lines); }

Grid2d<char> LinewiseInput::makeCharGrid2dWithGhostLayers(int const numGhostLayers,
                                                          char const fillElement) const {
  DEBUG_ASSERT(allLinesOfSameSize());
  using IndexType = Grid2d<char>::IndexType;
  size_t const xSize = _lines.front().size();
  size_t const ySize = _lines.size();

  Grid2d<char> result(xSize, ySize, fillElement, numGhostLayers);

  IndexType y = 0;
  std::ranges::for_each(std::ranges::reverse_view(_lines), [&](std::string_view line) {
    for (IndexType x = 0; x < result.xSize(); ++x)
      result[x, y] = line[x];
    ++y;
  });

  return result;
}

std::generator<std::string_view> getLineWise(std::string_view const text) {
  for (auto it = text.begin(); it != text.end();) {
    auto lineStart = it;
    it = std::find(it, text.end(), '\n');
    co_yield std::string_view{lineStart, it};
    if (it != text.end())
      ++it;
  }
}
