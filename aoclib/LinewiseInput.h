#pragma once

#include <libassert/assert.hpp>

#include <algorithm>
#include <generator>
#include <span>
#include <string_view>
#include <vector>

template <typename T> class Grid2d;

class LinewiseInput {
public:
  LinewiseInput(std::string_view const text);

  [[nodiscard]] auto begin() const { return std::cbegin(_lines); }
  [[nodiscard]] auto end() const { return std::cend(_lines); }
  [[nodiscard]] auto rbegin() const { return std::crbegin(_lines); }
  [[nodiscard]] auto rend() const { return std::crend(_lines); }

  [[nodiscard]] size_t size() const { return _lines.size(); }

  [[nodiscard]] std::string_view const &operator[](size_t const i) const {
    DEBUG_ASSERT(i < _lines.size());
    return _lines[i];
  }

  [[nodiscard]] std::vector<std::span<std::string_view const>> splitOnEmptyLines() const;

  [[nodiscard]] bool allLinesOfSameSize() const;

  [[nodiscard]] Grid2d<char> makeCharGrid2d() const;
  [[nodiscard]] Grid2d<char> makeCharGrid2dWithGhostLayers(int const numGhostLayers,
                                                           char const fillElement) const;

private:
  std::vector<std::string_view> _lines;
};

std::generator<std::string_view> getLineWise(std::string_view const text);

Grid2d<char> makeCharGrid2d(std::span<std::string_view const> const lines);
