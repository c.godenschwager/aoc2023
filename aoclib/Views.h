#pragma once

#include <ranges>

namespace views {
using namespace std::views;

auto constexpr toString = transform([](auto const &x) { return std::to_string(x); });

auto constexpr splitLines = std::views::split('\n') | transform([](auto &&r) {
                              return std::string_view(r.begin(), r.end());
                            });

constexpr auto toStringAndJoinWith(std::string_view const j) { return toString | join_with(j); }
constexpr auto toStringAndJoinWith(char const j) { return toString | join_with(j); }

} // namespace views