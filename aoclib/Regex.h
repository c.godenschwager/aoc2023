#pragma once

#include <re2/re2.h>

#include <generator>
#include <string_view>
#include <tuple>

template <class... Args> class FindAndConsume {
public:
  FindAndConsume(std::string_view const regex) : _regex(regex) {}

  using Result = std::tuple<Args...>;

  std::generator<Result> operator()(std::string_view s) const {
    std::tuple<Args...> parsedValues;
    auto f = [&](auto &...args) { return RE2::FindAndConsume(&s, _regex, &args...); };
    while (std::apply(f, parsedValues))
      co_yield parsedValues;
  }

private:
  RE2 _regex;
};
