#pragma once

#include <chrono>
#include <format>

class Timer {
public:
  using Clock = std::chrono::steady_clock;
  using Duration = Clock::duration;
  using TimePoint = Clock::time_point;

  Timer() : _start(Clock::now()) {}

  Duration elapsed() { return Clock::now() - _start; }

  static std::string formatDuration(Timer::Duration const d) {
    if (d >= std::chrono::seconds(1)) {
      std::chrono::duration<float> const secs = d;
      return std::format("{:.3f} s", secs.count());
    }
    if (d >= std::chrono::milliseconds(1)) {
      std::chrono::duration<float, std::milli> const msecs = d;
      return std::format("{:.3f} ms", msecs.count());
    }
    std::chrono::duration<float, std::micro> const musecs = d;
    return std::format("{:.3f} μs", musecs.count());
  }

private:
  TimePoint _start;
};