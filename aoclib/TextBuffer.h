#pragma once

#include <filesystem>
#include <memory>
#include <string_view>

class TextBuffer {
public:
  TextBuffer(std::filesystem::path const &p);
  [[nodiscard]] std::string_view getText() const { return {_buffer.get(), _size}; }

private:
  std::unique_ptr<char[]> _buffer; // NOLINT
  size_t _size = 0u;
};