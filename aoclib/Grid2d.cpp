#include "Grid2d.h"

#include <iterator>

std::ostream &operator<<(std::ostream &oss, Grid2dSpan<char> const &span) {
  if (span.ySize() == 0)
    return oss;

  Grid2dSpan<char>::IndexType y = span.ySize() - 1;
  for (Grid2dSpan<char>::IndexType x = 0; x < span.xSize(); ++x)
    oss << span[x, y];

  --y;

  for (; y >= 0; --y) {
    oss << '\n';
    for (Grid2d<char>::IndexType x = 0; x < span.xSize(); ++x)
      oss << span[x, y];
  }

  return oss;
}

std::ostream &operator<<(std::ostream &oss, SparseGrid2d<char> const &grid) {
  auto [min, max] = grid.nonDefaultCoordsInterval();

  if (max < min)
    return oss;

  SparseGrid2d<char>::Coord pos{min[0], max[1]};

  for (; pos[0] <= max[0]; ++pos[0])
    oss << grid[pos];

  --pos[1];

  for (; pos[1] >= min[1]; --pos[1]) {
    oss << '\n';
    for (pos[0] = min[0]; pos[0] <= max[0]; ++pos[0])
      oss << grid[pos];
  }
  return oss;
}