#include "TextBuffer.h"
#include "IntegerCast.h"

#include <fstream>

TextBuffer::TextBuffer(std::filesystem::path const &p) {
  std::ifstream t(p);
  t.seekg(0, std::ios::end);
  _size = t.tellg();
  _buffer = std::make_unique_for_overwrite<char[]>(_size); // NOLINT
  t.seekg(0);
  t.read(_buffer.get(), integerCast<std::streamsize>(_size));
}