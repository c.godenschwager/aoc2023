#pragma once

#include <libassert/assert.hpp>

#include <cstdint>
#include <functional>
#include <queue>
#include <string_view>
#include <vector>

class Computer {
public:
  using Address = uint64_t;
  using AddressOffset = int64_t;
  using OutputFunction = std::function<void(int64_t)>;
  enum class Status : uint8_t { HALTED, PAUSED };
  using enum Status;

  Computer(std::string_view const programString);

  Status run();
  void queueInput(int64_t value);

  void registerOutput(OutputFunction f) { _output.push_back(std::move(f)); }

  void writeMemory(Address address, int64_t value);
  int64_t readMemory(Address address);

  void reset();
  void reset(std::vector<int64_t> const &memory);

  [[nodiscard]] std::vector<int64_t> memdump() const { return _memory; }
  void setMemory(std::vector<int64_t> const &memory) { _memory = memory; }

private:
  [[nodiscard]] int64_t opcode() { return opcode(_ip); }
  [[nodiscard]] int64_t opcode(Address address);

  [[nodiscard]] int64_t parameterMode(Address address, size_t param);
  [[nodiscard]] int64_t parameterMode(size_t param) { return parameterMode(_ip, param); }

  bool performOp();
  [[nodiscard]] int64_t readParam(size_t param);
  void writeParam(size_t param, int64_t value);

  Address _ip = 0;
  AddressOffset _rb = 0;
  Status _status{PAUSED};
  std::vector<int64_t> _memory;
  std::queue<int64_t> _inputs;
  std::vector<OutputFunction> _output;
};