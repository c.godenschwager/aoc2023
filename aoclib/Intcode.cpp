#include "Intcode.h"

#include "IntegerCast.h"
#include "Parsing.h"

namespace {
static constexpr std::array<int64_t, 10u> paramFactor = {
    -1, 100, 1000, 10000, 1000000, 10000000, 100000000, 1000000000, 10000000000, 100000000000};
}

Computer::Computer(std::string_view const programString)
    : _memory(parseIntegerRange<int64_t>(programString)) {}

Computer::Status Computer::run() {
  while (performOp())
    ;
  return _status;
}

void Computer::reset() {
  _ip = 0;
  _rb = 0;
  while (!_inputs.empty())
    _inputs.pop();
}

void Computer::queueInput(int64_t value) { _inputs.push(value); }

void Computer::reset(std::vector<int64_t> const &memory) {
  reset();
  setMemory(memory);
}

int64_t Computer::opcode(Address address) { return readMemory(address) % 100; }

int64_t Computer::parameterMode(Address address, size_t param) {
  return readMemory(address) / paramFactor[param] % 10;
}

void Computer::writeMemory(Address address, int64_t value) {
  if (address >= _memory.size())
    _memory.resize(address + 1, 0);
  _memory[address] = value;
}

int64_t Computer::readMemory(Address address) {
  if (address >= _memory.size())
    _memory.resize(address + 1, 0);
  return _memory[address];
}

bool Computer::performOp() {
  int64_t value = 0;
  switch (opcode()) {
  case 1: // add
    writeParam(3, readParam(1) + readParam(2));
    _ip += 4u;
    break;
  case 2: // multiply
    writeParam(3, readParam(1) * readParam(2));
    _ip += 4u;
    break;
  case 3: // input
    if (_inputs.empty()) {
      _status = PAUSED;
      return false;
    }
    writeParam(1, _inputs.front());
    _inputs.pop();
    _ip += 2u;
    break;
  case 4: // output
    value = readParam(1);
    for (auto const &f : _output)
      f(value);
    _ip += 2u;
    break;
  case 5: // jump if true
    if (readParam(1) != 0)
      _ip = readParam(2);
    else
      _ip += 3;
    break;
  case 6: // jump if false
    if (readParam(1) == 0)
      _ip = readParam(2);
    else
      _ip += 3;
    break;
  case 7: // less than
    writeParam(3, readParam(1) < readParam(2) ? 1 : 0);
    _ip += 4u;
    break;
  case 8: // equals
    writeParam(3, readParam(1) == readParam(2) ? 1 : 0);
    _ip += 4u;
    break;
  case 9: // adjust relative base
    _rb += readParam(1);
    _ip += 2u;
    break;
  case 99:
    _status = HALTED;
    return false;
  default:
    ASSERT(false, "Invalid OP code", opcode());
  };
  return true;
}

int64_t Computer::readParam(size_t param) {
  DEBUG_ASSERT(_ip + param < _memory.size());

  int64_t const mode = parameterMode(param);
  switch (mode) {
  case 0: // position mode
    return readMemory(readMemory(_ip + param));
  case 1: // immediate mode
    return readMemory(_ip + param);
  case 2: // relative mode
    return readMemory(_rb + readMemory(_ip + param));
  default:
    ASSERT(false, "Invalid parameter mode", mode);
    return 0;
  }
}

void Computer::writeParam(size_t param, int64_t value) {
  int64_t const mode = parameterMode(param);
  switch (mode) {
  case 0: // position mode
    writeMemory(readMemory(_ip + param), value);
    break;
  case 2: // relative mode
    writeMemory(_rb + readMemory(_ip + param), value);
    break;
  default:
    ASSERT(false, "Invalid parameter mode", mode);
  }
}
