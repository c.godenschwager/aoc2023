#pragma once

#include "IntegerCast.h"

#include <absl/hash/hash.h>

#include <algorithm>
#include <array>
#include <functional>
#include <ranges>

template <size_t N, typename T> class Vector {
public:
  using ElementType = T;
  using IndexType = int;

  constexpr Vector() : _data{} {};

  explicit constexpr Vector(ElementType const &init)
    requires(N > 1u)
  {
    std::fill(_data.begin(), _data.end(), init);
  }

  template <typename... Ts>
  constexpr Vector(Ts &&...vals)
    requires(sizeof...(Ts) == N)
      : _data{std::forward<Ts>(vals)...} {}

  template <typename U>
  explicit constexpr Vector(Vector<N, U> const &other)
    requires(!std::is_same_v<T, U>)
  {
    std::ranges::copy(other, begin());
  }

  template <typename U>
  explicit constexpr Vector(Vector<N, U> &&other)
    requires(!std::is_same_v<T, U>)
  {
    std::ranges::move(std::move(other), begin());
  }

  [[nodiscard]] constexpr ElementType &operator[](auto const i) {
    ASSUME(integerCast<IndexType>(i) < size());
    return _data[integerCast<size_t>(i)];
  }

  [[nodiscard]] constexpr ElementType const &operator[](auto const i) const {
    ASSUME(integerCast<IndexType>(i) < size());
    return _data[integerCast<size_t>(i)];
  }

  [[nodiscard]] constexpr ElementType &x() { return (*this)[0]; }
  [[nodiscard]] constexpr ElementType const &x() const { return (*this)[0]; }
  [[nodiscard]] constexpr ElementType &y()
    requires(N >= 2)
  {
    return (*this)[1];
  }

  [[nodiscard]] constexpr ElementType const &y() const
    requires(N >= 2)
  {
    return (*this)[1];
  }

  [[nodiscard]] constexpr ElementType &z()
    requires(N >= 3)
  {
    return (*this)[2];
  }

  [[nodiscard]] constexpr ElementType const &z() const
    requires(N >= 3)
  {
    return (*this)[2];
  }

  [[nodiscard]] constexpr T *data() { return _data.data(); }
  [[nodiscard]] constexpr T const *data() const { return _data.data(); }
  [[nodiscard]] constexpr size_t size() const { return N; }

  [[nodiscard]] constexpr auto begin() { return _data.begin(); }
  [[nodiscard]] constexpr auto end() { return _data.end(); }
  [[nodiscard]] constexpr auto begin() const { return _data.begin(); }
  [[nodiscard]] constexpr auto end() const { return _data.end(); }
  [[nodiscard]] constexpr auto cbegin() const { return _data.cbegin(); }
  [[nodiscard]] constexpr auto cend() const { return _data.cend(); }
  [[nodiscard]] constexpr auto rbegin() { return _data.rbegin(); }
  [[nodiscard]] constexpr auto rend() { return _data.rend(); }
  [[nodiscard]] constexpr auto rbegin() const { return _data.rbegin(); }
  [[nodiscard]] constexpr auto rend() const { return _data.rend(); }
  [[nodiscard]] constexpr auto crbegin() const { return _data.crbegin(); }
  [[nodiscard]] constexpr auto crend() const { return _data.crend(); }

  friend constexpr Vector operator+(Vector const &lhs, Vector const &rhs) {
    return {std::views::zip_transform(std::plus{}, lhs, rhs)};
  }

  friend constexpr Vector operator-(Vector const &lhs, Vector const &rhs) {
    return {std::views::zip_transform(std::minus{}, lhs, rhs)};
  }

  friend constexpr Vector operator*(Vector const &lhs, Vector const &rhs) {
    return {std::views::zip_transform(std::multiplies{}, lhs, rhs)};
  }

  friend constexpr Vector operator/(Vector const &lhs, Vector const &rhs) {
    return {std::views::zip_transform(std::divides{}, lhs, rhs)};
  }

  friend constexpr Vector operator%(Vector const &lhs, Vector const &rhs) {
    return {std::views::zip_transform(std::modulus{}, lhs, rhs)};
  }

  friend constexpr Vector operator-(Vector const &v) {
    return {std::views::transform(v, std::negate{})};
  }

  constexpr Vector &operator+=(Vector const &other) {
    std::ranges::transform(*this, other, this->begin(), std::plus{});
    return *this;
  }

  constexpr Vector &operator-=(Vector const &other) {
    std::ranges::transform(*this, other, this->begin(), std::minus{});
    return *this;
  }

  constexpr Vector &operator*=(Vector const &other) {
    std::ranges::transform(*this, other, this->begin(), std::multiplies{});
    return *this;
  }

  constexpr Vector &operator/=(Vector const &other) {
    std::ranges::transform(*this, other, this->begin(), std::divides{});
    return *this;
  }

  constexpr Vector &operator%=(Vector const &other) {
    std::ranges::transform(*this, other, this->begin(), std::modulus{});
    return *this;
  }

  constexpr friend Vector operator+(Vector const &lhs, ElementType const &rhs) {
    return {std::ranges::transform_view(lhs, [&rhs](ElementType const &e) { return e + rhs; })};
  }

  constexpr friend Vector operator-(Vector const &lhs, ElementType const &rhs) {
    return {std::ranges::transform_view(lhs, [&rhs](ElementType const &e) { return e - rhs; })};
  }

  constexpr friend Vector operator*(Vector const &lhs, ElementType const &rhs) {
    return {std::ranges::transform_view(lhs, [&rhs](ElementType const &e) { return e * rhs; })};
  }

  constexpr friend Vector operator/(Vector const &lhs, ElementType const &rhs) {
    return {std::ranges::transform_view(lhs, [&rhs](ElementType const &e) { return e / rhs; })};
  }

  constexpr friend Vector operator%(Vector const &lhs, ElementType const &rhs) {
    return {std::ranges::transform_view(lhs, [&rhs](ElementType const &e) { return e % rhs; })};
  }

  constexpr friend Vector operator+(ElementType const &lhs, Vector const &rhs) {
    return {std::ranges::transform_view(rhs, [&lhs](ElementType const &e) { return lhs + e; })};
  }

  constexpr friend Vector operator-(ElementType const &lhs, Vector const &rhs) {
    return {std::ranges::transform_view(rhs, [&lhs](ElementType const &e) { return lhs - e; })};
  }

  constexpr friend Vector operator*(ElementType const &lhs, Vector const &rhs) {
    return {std::ranges::transform_view(rhs, [&lhs](ElementType const &e) { return lhs * e; })};
  }

  constexpr friend Vector operator/(ElementType const &lhs, Vector const &rhs) {
    return {std::ranges::transform_view(rhs, [&lhs](ElementType const &e) { return lhs / e; })};
  }

  constexpr friend Vector operator%(ElementType const &lhs, Vector const &rhs) {
    return {std::ranges::transform_view(rhs, [&lhs](ElementType const &e) { return lhs % e; })};
  }

  constexpr auto operator<=>(Vector const &other) const = default;

  template <typename H> friend constexpr H AbslHashValue(H h, Vector const &v) {
    return H::combine(std::move(h), v._data);
  }

  constexpr friend Vector elementwiseMin(Vector const &lhs, Vector const &rhs) {
    return {std::views::zip_transform(
        [](ElementType const &l, ElementType const &r) { return std::min(l, r); }, lhs, rhs)};
  }

  constexpr friend Vector elementwiseMax(Vector const &lhs, Vector const &rhs) {
    return {std::views::zip_transform(
        [](ElementType const &l, ElementType const &r) { return std::max(l, r); }, lhs, rhs)};
  }

  constexpr ElementType l1norm() const {
    return std::ranges::fold_left(
        *this | std::views::transform([](auto const &x) { return std::abs(x); }), ElementType{0},
        std::plus{});
  }

private:
  template <std::ranges::sized_range R>
  constexpr Vector(R const &r)
    requires(!std::is_same_v<std::remove_cvref_t<R>, Vector<N, T>>)
  {
    DEBUG_ASSERT(std::ranges::size(r) == N);
    std::ranges::copy(r, _data.begin());
  };

  template <std::ranges::sized_range R>
  constexpr Vector(R &&r)
    requires(!std::is_same_v<std::remove_cvref_t<R>, Vector<N, T>>)
  {
    DEBUG_ASSERT(std::ranges::size(r) == N);
    std::ranges::move(std::forward<R>(r), _data.begin());
  };

  std::array<ElementType, N> _data;
};

// Custom specialization of std::hash can be injected in namespace std.
template <size_t N, typename T> struct std::hash<Vector<N, T>> : public absl::Hash<Vector<N, T>> {};

// Making Vector "tuple-like" to enable structured bindings
template <size_t N, typename T>
struct std::tuple_size<Vector<N, T>> : public integral_constant<std::size_t, N> {};

template <std::size_t I, size_t N, typename T>
  requires(I < N)
struct std::tuple_element<I, Vector<N, T>> {
  using type = T;
};

template <std::size_t I, size_t N, typename T>
  requires(I < N)
constexpr T &get(Vector<N, T> &v) {
  return v[I];
}

template <std::size_t I, size_t N, typename T>
  requires(I < N)
constexpr T const &get(Vector<N, T> const &v) {
  return v[I];
}

template <std::size_t I, size_t N, typename T>
  requires(I < N)
constexpr T &&get(Vector<N, T> &&v) {
  return std::move(get<I>(v));
}

template <std::size_t I, size_t N, typename T>
  requires(I < N)
constexpr T const &&get(Vector<N, T> const &&v) {
  return std::move(get<I>(v));
}

template <typename T> using Vec2 = Vector<2u, T>;
template <typename T> using Vec3 = Vector<3u, T>;