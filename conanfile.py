from conan import ConanFile
from conan.tools.cmake import CMakeToolchain, cmake_layout

class AoC(ConanFile):
    settings = "os", "compiler", "build_type", "arch"
    generators = "CMakeDeps"

    def generate(self):
        tc = CMakeToolchain(self)
        tc.presets_prefix = ""
        tc.generate()

    def requirements(self):
        self.requires("abseil/20240116.1")
        self.requires("benchmark/1.9.0")
        self.requires("boost/1.86.0")
        self.requires("fmt/11.0.2")
        self.requires("gtest/1.15.0")
        self.requires("hash-library/8.0")
        self.requires("libassert/2.1.2")
        self.requires("mdspan/0.6.0")
        self.requires("onetbb/2021.12.0")
        self.requires("re2/20240702")

    def layout(self):
        cmake_layout(self)