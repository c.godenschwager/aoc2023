#!/bin/bash
set -e

export CI=1

rm -r build

conan install . --build=missing -pr=clang-debug
cmake --preset clang-debug
cmake --build --preset clang-debug
ctest --preset clang-debug -j $(nproc)
run-clang-tidy -p build/clang/Debug -j $(nproc) -warnings-as-errors "*"

conan install . --build=missing -pr=clang-release
cmake --preset clang-release
cmake --build --preset clang-release
ctest --preset clang-release -j $(nproc)
run-clang-tidy -p build/clang/Release -j $(nproc) -warnings-as-errors "*"

conan install . --build=missing -pr=clang-sanitizer-debug
cmake --preset clang-sanitizer-debug
cmake --build --preset clang-sanitizer-debug
ctest --preset clang-sanitizer-debug -j $(nproc)

conan install . --build=missing -pr=gcc-debug
cmake --preset gcc-debug
cmake --build --preset gcc-debug
ctest --preset gcc-debug -j $(nproc)

conan install . --build=missing -pr=gcc-release
cmake --preset gcc-release
cmake --build --preset gcc-release
ctest --preset gcc-release -j $(nproc)