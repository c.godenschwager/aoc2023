#!/bin/bash

set -vex

conan install . --build=missing -pr=$COMPILER-$BUILD_TYPE
cmake --preset $COMPILER-$BUILD_TYPE
ccache -z
cmake --build --preset $COMPILER-$BUILD_TYPE
ccache -s
ctest --preset $COMPILER-$BUILD_TYPE --output-junit $CI_PROJECT_DIR/ctest_report.xml -j $(nproc)

if [ "$COMPILER" == "clang" ]
then
    if [ "$LLVM_PROFILE_FILE" ]
    then
        llvm-profdata merge --sparse=true --output=/tmp/profile/coverage.profdata /tmp/profile/*.profraw
        llvm-cov report --show-branch-summary=false --show-region-summary=false --instr-profile /tmp/profile/coverage.profdata build/clang/Debug/bin/puzzle_tests
        llvm-cov export --instr-profile /tmp/profile/coverage.profdata --format=lcov build/clang/Debug/bin/puzzle_tests > /tmp/profile/coverage.lcov
        lcov_cobertura /tmp/profile/coverage.lcov --output=coverage.xml --demangle
        genhtml -j $(nproc) --demangle-cpp --title "$CI_COMMIT_REF_NAME" --header-title "AoC code coverage" --output-directory ./coverage_report /tmp/profile/coverage.lcov
    fi

    echo "WarningsAsErrors: '*'" >> $CI_PROJECT_DIR/.clang-tidy
    if [ "$CI_PIPELINE_SOURCE" == "merge_request_event" ]
    then
        git fetch origin $CI_MERGE_REQUEST_TARGET_BRANCH_NAME $CI_COMMIT_SHA
        git diff -U0 origin/$CI_MERGE_REQUEST_TARGET_BRANCH_NAME...$CI_COMMIT_SHA | clang-tidy-diff -p1 -path build/clang/* -j $(nproc)
#    else
#        run-clang-tidy -p build/clang/* -j $(nproc)
    fi
fi
