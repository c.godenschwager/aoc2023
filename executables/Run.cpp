#include "Puzzle.h"
#include "TextBuffer.h"
#include "Timer.h"

#include <benchmark/benchmark.h>
#include <fmt/format.h>
#include <re2/re2.h>

#include <algorithm>
#include <cstdlib>
#include <string_view>
#include <vector>

int run(int argc, char *argv[]) { // NOLINT
  using namespace std::chrono_literals;
  std::vector<std::string_view> args(argv, argv + argc);

  std::optional<unsigned> partNumber;
  auto it = std::ranges::find(args, "--part");
  if (it != args.end()) {
    auto partNumberIt = std::next(it);
    unsigned p = 0;
    if (partNumberIt != args.end() && RE2::FullMatch(*partNumberIt, "([12])", &p)) {
      partNumber = p;
    } else {
      fmt::println("Part number given via \"--part\" invalid!");
      return EXIT_FAILURE;
    }
    args.erase(it, std::next(partNumberIt));
  }

  RE2 const yearPattern = R"((\d{4}))";
  RE2 const dayPattern = R"((\d{1,2}))";

  bool yearGiven = false;
  Puzzle::Year year = Puzzle::latestYear();
  if (args.size() > 1u) {
    int y = 0;
    if (RE2::FullMatch(args[1u], yearPattern, &y)) {
      year = Puzzle::Year(y);
      args.erase(std::next(args.begin()));
      yearGiven = true;
    }
  }

  std::vector<std::unique_ptr<Puzzle>> puzzles;
  it = std::next(args.begin());
  for (; it != args.end(); ++it) {
    unsigned d = 0;
    if (RE2::FullMatch(*it, dayPattern, &d)) {
      Puzzle::Day day(d);
      std::unique_ptr<Puzzle> puzzle = Puzzle::create(year, day);
      if (!puzzle) {
        fmt::println("Puzzle from {}, day {} not found!", static_cast<int>(year),
                     static_cast<unsigned>(day));
        return EXIT_FAILURE;
      } else {
        puzzles.push_back(std::move(puzzle));
      }
    } else {
      break;
    }
  }
  args.erase(std::next(args.begin()), it);

  if (puzzles.empty()) {
    if (yearGiven)
      puzzles = Puzzle::createAll(year);
    else
      puzzles.push_back(Puzzle::createLatest());
  }

  bool result = true;
  for (auto const &puzzle : puzzles) {
    TextBuffer text(puzzle->inputFile());

    fmt::println(R"(Running puzzle "{}".)", puzzle->name());
    if (!partNumber || partNumber == 1) {
      Timer t;
      std::string const solution = puzzle->runPart1(text.getText());
      auto elapsed = t.elapsed();
      if (solution == puzzle->solutionPart1()) {
        fmt::println(R"(Solution "{}" for part 1 is correct. Took {}.)", solution,
                     Timer::formatDuration(elapsed));
      } else {
        fmt::println(R"(Solution "{}" for part 1 is wrong. It should be "{}". Took {})", solution,
                     puzzle->solutionPart1(), Timer::formatDuration(elapsed));
        result = false;
      }
    }

    if (!partNumber || partNumber == 2) {
      Timer t;
      std::string const solution = puzzle->runPart2(text.getText());
      auto elapsed = t.elapsed();
      if (solution == puzzle->solutionPart2()) {
        fmt::println(R"(Solution "{}" for part 2 is correct. Took {}.)", solution,
                     Timer::formatDuration(elapsed));
      } else {
        fmt::println(R"(Solution "{}" for part 2 is wrong. It should be "{}". Took {})", solution,
                     puzzle->solutionPart2(), Timer::formatDuration(elapsed));
        result = false;
      }
    }
  }

  return result ? EXIT_SUCCESS : EXIT_FAILURE;
}

int bench(int argc, char *argv[]) { // NOLINT

  std::vector<std::unique_ptr<Puzzle>> puzzles = Puzzle::createAll();

  auto runBenchmarkPt2 = [](benchmark::State &state, Puzzle const *puzzle,
                            std::filesystem::path const &inputPath) {
    TextBuffer input(inputPath);
    for (auto _ : state)
      std::ignore = puzzle->runPart2(input.getText());
  };

  auto runBenchmarkPt1 = [](benchmark::State &state, Puzzle const *puzzle,
                            std::filesystem::path const &inputPath) {
    TextBuffer input(inputPath);
    for (auto _ : state)
      std::ignore = puzzle->runPart1(input.getText());
  };
  for (auto const &puzzle : puzzles) {
    benchmark::RegisterBenchmark(puzzle->name() + "_P1", runBenchmarkPt1, puzzle.get(),
                                 puzzle->inputFile())
        ->Unit(benchmark::kMicrosecond);
    benchmark::RegisterBenchmark(puzzle->name() + "_P2", runBenchmarkPt2, puzzle.get(),
                                 puzzle->inputFile())
        ->Unit(benchmark::kMicrosecond);
  }

  benchmark::Initialize(&argc, argv);
  if (::benchmark::ReportUnrecognizedArguments(argc, argv))
    return EXIT_FAILURE;
  benchmark::RunSpecifiedBenchmarks();
  benchmark::Shutdown();

  return EXIT_SUCCESS;
}

int main(int argc, char *argv[]) {
  std::filesystem::path const executable(argv[0]); // NOLINT
  if (executable.filename() == "run")
    return run(argc, argv);
  if (executable.filename() == "bench")
    return bench(argc, argv);
}