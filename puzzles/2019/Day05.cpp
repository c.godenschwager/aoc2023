#include "Intcode.h"
#include "IntegerCast.h"
#include "Parsing.h"
#include "PuzzleImpl.h"

namespace {} // namespace

template <> size_t part1<2019, 5>(std::string_view const input) {
  Computer c(input);
  std::vector<int64_t> outputs;
  c.registerOutput([&](int64_t v) { outputs.push_back(v); });
  c.queueInput(1);
  ASSERT(c.run() == Computer::HALTED);
  for (size_t i = 0; i < outputs.size() - 1; ++i)
    ASSERT(outputs[i] == 0);
  return outputs.back();
}

template <> size_t part2<2019, 5>(std::string_view const input) {
  Computer c(input);
  int64_t lastOutput = 0;
  c.registerOutput([&](int64_t v) { lastOutput = v; });
  c.queueInput(5);
  ASSERT(c.run() == Computer::HALTED);
  return lastOutput;
}
