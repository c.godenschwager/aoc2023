#include "Intcode.h"
#include "PuzzleImpl.h"

#include <optional>

template <> size_t part1<2019, 9>(std::string_view const input) {
  Computer c(input);

  std::optional<int64_t> onlyOutput;
  c.registerOutput([&](int64_t v) {
    ASSERT(!onlyOutput);
    onlyOutput = v;
  });

  c.queueInput(1);
  ASSERT(c.run() == Computer::HALTED);

  return onlyOutput.value_or(0);
}

template <> size_t part2<2019, 9>(std::string_view const input) {
  Computer c(input);

  std::optional<int64_t> onlyOutput;
  c.registerOutput([&](int64_t v) {
    ASSERT(!onlyOutput);
    onlyOutput = v;
  });

  c.queueInput(2);
  ASSERT(c.run() == Computer::HALTED);

  return onlyOutput.value_or(0);
}
