#include "Grid2d.h"
#include "Intcode.h"
#include "IntegerCast.h"
#include "LinewiseInput.h"
#include "PuzzleImpl.h"

#include <algorithm>
#include <chrono>
#include <iostream>
#include <limits>
#include <queue>
#include <stack>
#include <thread>

namespace {
using Coord = Vec2<int>;
constexpr std::array<Coord, 4u> stencil = {{{0, 1}, {1, 0}, {0, -1}, {-1, 0}}};
} // namespace

template <> size_t part1<2019, 17>(std::string_view const input) {
  Computer c(input);
  std::string output;
  c.registerOutput([&](int64_t v) { output.push_back(static_cast<char>(v)); });
  ASSERT(c.run() == Computer::HALTED);
  output.pop_back(); // remove last newline
  Grid2d<char> grid = LinewiseInput{output}.makeCharGrid2dWithGhostLayers(1, '.');
  int64_t result = 0;
  for (Coord c{0, 0}; c.y() < grid.ySize(); ++c.y())
    for (c.x() = 0; c.x() < grid.xSize(); ++c.x()) {
      if (grid[c] == '#' &&
          std::ranges::count_if(stencil, [&](auto const &s) { return grid[c + s] == '#'; }) >= 3) {
        grid[c] = 'O';
        result += integerCast<int64_t>(c.x() * (grid.ySize() - c.y() - 1));
      }
    }

  return result;
}

template <> size_t part2<2019, 17>(std::string_view const /*input*/) { return 0; }
