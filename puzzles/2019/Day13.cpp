#include "Grid2d.h"
#include "Intcode.h"
#include "PuzzleImpl.h"

#include <absl/container/flat_hash_map.h>

#include <chrono>
#include <iostream>
#include <thread>

namespace {

using Coord = Grid2d<char>::Coord;

static constexpr std::array<char, 5u> tiles = {' ', '#', 'X', '-', 'o'};

Grid2d<char> initialGrid(std::string_view const input) {
  Computer c(input);

  std::vector<int64_t> output;
  c.registerOutput([&](int64_t v) { output.push_back(v); });
  ASSERT(c.run() == Computer::HALTED);

  Coord max;
  for (size_t i = 0; i < output.size(); i += 3) {
    max.x() = std::max(max.x(), integerCast<Coord::ElementType>(output[i]));
    max.y() = std::max(max.y(), integerCast<Coord::ElementType>(output[i + 1]));
  }

  Grid2d<char> screen(max.x() + 1, max.y() + 1, ' ');

  for (size_t i = 0; i < output.size(); i += 3)
    screen[output[i], max.y() - output[i + 1]] = tiles[output[i + 2]];

  return screen;
}

constexpr int64_t NEUTRAL = 0;
constexpr int64_t LEFT = -1;
constexpr int64_t RIGHT = 1;

} // namespace

template <> size_t part1<2019, 13>(std::string_view const input) {
  Grid2d<char> screen = initialGrid(input);

  // std::cout << "\n" << screen << "\n\n";

  return screen.count('X');
}

template <> size_t part2<2019, 13>(std::string_view const input) {
  // Grid2d<char> screen = initialGrid(input);
  // screen.fill(' ');

  Computer c(input);
  c.writeMemory(0, 2);

  int64_t joystickPos = NEUTRAL;
  int64_t score = 0;
  int64_t paddleX = 0;
  Vec2<int64_t> ballPos(0, 0);

  auto outputFunc = [&, buffer = std::vector<int64_t>{}](int64_t v) mutable {
    buffer.push_back(v);
    if (buffer.size() == 3u) {
      if (buffer[0] == -1 && buffer[1] == 0) {
        score = buffer[2];
      } else {
        // screen[buffer[0], screen.ySize() - buffer[1] - 1] = tiles[buffer[2]];
        switch (buffer[2]) {
        case 3:
          paddleX = buffer[0];
          break;
        case 4:
          ballPos[0] = buffer[0];
          ballPos[1] = buffer[1];
          break;
        default:
          break;
        }
      }
      buffer.clear();
    }
  };

  c.registerOutput(outputFunc);

  // constexpr int FPS = 60;
  // constexpr std::chrono::duration<double> frameTime{1.0 / FPS};
  while (c.run() != Computer::HALTED) {
    // auto const start = std::chrono::steady_clock::now();
    if (ballPos.x() < paddleX)
      joystickPos = LEFT;
    else if (ballPos.x() > paddleX)
      joystickPos = RIGHT;
    else
      joystickPos = NEUTRAL;
    c.queueInput(joystickPos);
    // std::cout << "\n\n\n\n\n\nScore: " << score << "\n" << screen << "\n";
    // std::this_thread::sleep_until(start + frameTime);
  }

  return score;
}
