#include "Intcode.h"
#include "Parsing.h"
#include "PuzzleImpl.h"

namespace {} // namespace

template <> size_t part1<2019, 2>(std::string_view const input) {
  Computer c(input);
  c.writeMemory(1, 12);
  c.writeMemory(2, 2);
  ASSERT(c.run() == Computer::HALTED);
  return c.readMemory(0);
}

template <> size_t part2<2019, 2>(std::string_view const input) {
  constexpr int64_t expectedResult = 19690720;
  Computer c(input);
  std::vector<int64_t> const originalMemory = c.memdump();
  int64_t noun = 0;
  int64_t verb = 0;
  for (; noun < 100; ++noun) {
    for (verb = 0; verb < 100; ++verb) {
      c.reset(originalMemory);
      c.writeMemory(1, noun);
      c.writeMemory(2, verb);
      ASSERT(c.run() == Computer::HALTED);
      if (c.readMemory(0) == expectedResult)
        break;
    }
    if (c.readMemory(0) == expectedResult)
      break;
  }
  ASSERT(c.readMemory(0) == expectedResult);
  return 100 * noun + verb;
}
