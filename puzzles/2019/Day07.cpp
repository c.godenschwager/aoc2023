#include "Intcode.h"
#include "IntegerCast.h"
#include "PuzzleImpl.h"

#include <algorithm>

namespace {} // namespace

template <> size_t part1<2019, 7>(std::string_view const input) {

  std::array<Computer, 5u> amplifiers = {{{input}, {input}, {input}, {input}, {input}}};
  std::array<int64_t, 5u> sequence = {0, 1, 2, 3, 4};
  auto const originalMemory = amplifiers.front().memdump();
  int64_t lastOutput = 0;
  amplifiers.back().registerOutput([&](int64_t v) { lastOutput = v; });

  for (size_t i = 1; i < amplifiers.size(); ++i)
    amplifiers[i - 1].registerOutput([&c = amplifiers[i]](int64_t v) { c.queueInput(v); });

  int64_t maxOutput = 0;
  do { // NOLINT
    for (size_t i = 0; i < amplifiers.size(); ++i) {
      amplifiers[i].reset(originalMemory);
      amplifiers[i].queueInput(sequence[i]);
    }

    amplifiers.front().queueInput(0);

    for (auto &amp : amplifiers)
      ASSERT(amp.run() == Computer::HALTED);

    maxOutput = std::max(maxOutput, lastOutput);
  } while (std::ranges::next_permutation(sequence).found);
  return maxOutput;
}

template <> size_t part2<2019, 7>(std::string_view const input) {
  std::array<Computer, 5u> amplifiers = {{{input}, {input}, {input}, {input}, {input}}};
  std::array<int64_t, 5u> sequence = {5, 6, 7, 8, 9};
  auto const originalMemory = amplifiers.front().memdump();
  int64_t lastOutput = 0;

  for (size_t i = 1; i < amplifiers.size(); ++i)
    amplifiers[i - 1].registerOutput([&c = amplifiers[i]](int64_t v) { c.queueInput(v); });

  amplifiers.back().registerOutput([&](int64_t v) { lastOutput = v; });
  amplifiers.back().registerOutput([&c = amplifiers.front()](int64_t v) { c.queueInput(v); });

  int64_t maxOutput = 0;
  do { // NOLINT
    for (size_t i = 0; i < amplifiers.size(); ++i) {
      amplifiers[i].reset(originalMemory);
      amplifiers[i].queueInput(sequence[i]);
    }

    amplifiers.front().queueInput(0);

    bool allHalted = false;
    while (!allHalted) {
      allHalted = true;
      for (auto &amp : amplifiers)
        if (amp.run() != Computer::HALTED)
          allHalted = false;
    }

    maxOutput = std::max(maxOutput, lastOutput);
  } while (std::ranges::next_permutation(sequence).found);
  return maxOutput;
}
