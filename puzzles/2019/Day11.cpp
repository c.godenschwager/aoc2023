#include "Grid2d.h"
#include "Intcode.h"
#include "PuzzleImpl.h"

#include <absl/container/flat_hash_map.h>

#include <iostream>
#include <limits>

namespace {

using Coord = Grid2d<char>::Coord;

enum Dir : uint8_t { N = 0, E = 1, S = 2, W = 3 };
constexpr std::array<Coord, 4u> stencil = {{{0, 1}, {1, 0}, {0, -1}, {-1, 0}}};
constexpr std::array<Dir, 4u> leftTurn = {W, N, E, S};
constexpr std::array<Dir, 4u> rightTurn = {E, S, W, N};

struct Instruction {
  bool color;
  bool turnLeft;
};

absl::flat_hash_map<Coord, bool> paint(std::string_view const input, int64_t startColor) {
  Computer c(input);

  Instruction i{};
  c.registerOutput([&i, ctr = 0](int64_t v) mutable {
    if (ctr == 0) {
      i.color = v == 1;
      ++ctr;
    } else {
      i.turnLeft = v == 0;
      ctr = 0;
    }
  });

  absl::flat_hash_map<Coord, bool> paintedPanels;
  Coord coord{0, 0};
  Dir d = N;
  c.queueInput(startColor);

  while (c.run() != Computer::HALTED) {
    paintedPanels[coord] = i.color;
    if (i.turnLeft)
      d = leftTurn[d];
    else
      d = rightTurn[d];
    coord += stencil[d];

    auto it = paintedPanels.find(coord);
    if (it == paintedPanels.end())
      c.queueInput(0);
    else
      c.queueInput(it->second ? 1 : 0);
  }

  return paintedPanels;
}

[[maybe_unused]] void print(absl::flat_hash_map<Coord, bool> &canvas) {
  Coord min(std::numeric_limits<Coord::ElementType>::max());
  Coord max(std::numeric_limits<Coord::ElementType>::min());
  for (auto [coord, color] : canvas) {
    if (!color)
      continue;
    min = elementwiseMin(min, coord);
    max = elementwiseMax(max, coord);
  }
  Coord extent = max - min + Coord{1, 1};

  Grid2d<char> grid(extent.x(), extent.y(), ' ');

  for (auto [coord, color] : canvas)
    if (color)
      grid[coord - min] = '#';

  std::cout << '\n' << grid << "\n\n";
}

} // namespace

template <> size_t part1<2019, 11>(std::string_view const input) {
  auto paintedPanels = paint(input, 0);

  // print(paintedPanels);

  return paintedPanels.size();
}

template <> size_t part2<2019, 11>(std::string_view const input) {
  auto paintedPanels = paint(input, 1);

  // print(paintedPanels);

  return paintedPanels.size();
}
