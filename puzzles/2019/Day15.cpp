#include "Grid2d.h"
#include "Intcode.h"
#include "PuzzleImpl.h"

#include <libassert/assert.hpp>

#include <algorithm>
#include <chrono>
#include <iostream>
#include <limits>
#include <queue>
#include <stack>
#include <thread>

namespace {
using Coord = Grid2d<char>::Coord;

enum Dir : uint8_t { N = 0, E = 1, S = 2, W = 3 };
constexpr std::array<Coord, 4u> stencil = {{{0, 1}, {1, 0}, {0, -1}, {-1, 0}}};
constexpr std::array<Dir, 4u> leftTurn = {W, N, E, S};
constexpr std::array<Dir, 4u> rightTurn = {E, S, W, N};
constexpr std::array<Dir, 4u> opposite = {S, W, N, E};
constexpr std::array<int64_t, 4u> command = {1, 4, 2, 3};
constexpr std::array<char, 3u> mapSymbol = {'#', '.', 'X'};

std::tuple<Grid2d<char>, Coord, Coord> exploreMap(std::string_view const input) {
  Computer c(input);
  SparseGrid2d<char> map(' ');
  std::stack<Dir> back;
  Dir d = E;
  Coord pos{0, 0};
  map.set(pos, '.');
  int64_t response = 0;
  std::optional<Coord> oxygenSystemPos;
  c.registerOutput([&](int64_t v) { response = v; });

  //   constexpr int FPS = 60;
  //   constexpr std::chrono::duration<double> frameTime{1.0 / FPS};
  while (!oxygenSystemPos || pos != Coord{0, 0}) {
    // auto const start = std::chrono::steady_clock::now();
    Dir newDir = d;
    Coord newPos = pos;

    newDir = rightTurn[newDir];
    bool backtrack = true;
    for (int i = 0; i < 4; ++i) {
      if (map[pos + stencil[newDir]] == ' ') {
        backtrack = false;
        break;
      }
      newDir = leftTurn[newDir];
    }
    if (backtrack)
      newDir = back.top();

    newPos = pos + stencil[newDir];
    c.queueInput(command[newDir]);

    c.run();

    if (backtrack) {
      DEBUG_ASSERT(response == 1);
      d = newDir;
      pos = newPos;
      back.pop();
    } else {
      switch (response) {
      case 2:
        oxygenSystemPos = newPos;
        [[fallthrough]];
      case 1:
        d = newDir;
        pos = newPos;
        back.push(opposite[d]);
        [[fallthrough]];
      case 0:
        map.set(newPos, mapSymbol[response]);
        break;
      default:
        UNREACHABLE(response);
      }
    }

    // char old = map[pos];
    // map.set(pos, 'D');
    // std::cout << "\n" << map << "\n\n";
    // map.set(pos, old);
    // std::this_thread::sleep_until(start + frameTime);
  }
  ASSERT(oxygenSystemPos);

  auto [grid, offset] = map.toGrid2d();

  return {grid, -offset, oxygenSystemPos.value_or(Coord{0, 0}) - offset};
}

int64_t aStar(Grid2d<char> const &map, Coord start, Coord goal) {
  Grid2d<int64_t> gScore(map.xSize(), map.ySize(), std::numeric_limits<int64_t>::max());
  gScore[start] = 0;
  using PrioCoord = std::tuple<int64_t, Coord>;
  auto comp = [&](PrioCoord const &lhs, PrioCoord const &rhs) {
    return std::get<0>(lhs) > std::get<0>(rhs);
  };
  std::priority_queue<PrioCoord, std::vector<PrioCoord>, decltype(comp)> openSet(comp);
  auto heuristic = [](Coord a, Coord b) {
    return std::abs(a.x() - b.x()) + std::abs(a.y() - b.y());
  };
  openSet.emplace(heuristic(start, goal), start);

  while (!openSet.empty()) {
    auto const [oldScore, cur] = openSet.top();
    openSet.pop();

    if (cur == goal)
      return gScore[cur];

    for (auto const &s : stencil) {
      Coord const neighbor = cur + s;
      if (map[neighbor] == '#')
        continue;
      int64_t tentiativeGScore = gScore[cur] + 1;
      if (tentiativeGScore < gScore[neighbor]) {
        gScore[neighbor] = tentiativeGScore;
        openSet.emplace(tentiativeGScore + heuristic(neighbor, goal), neighbor);
      }
    }
  }

  UNREACHABLE();
}

Grid2d<unsigned> dijkstra(Grid2d<char> const &map, Coord start) {
  Grid2d<unsigned> dist(map.xSize(), map.ySize(), std::numeric_limits<unsigned>::max());
  Grid2d<uint8_t> visited(map.xSize(), map.ySize(), 0);
  dist[start] = 0;
  std::queue<Coord> next;
  next.push(start);
  while (!next.empty()) {
    Coord const cur = next.front();
    visited[cur] = 1;
    next.pop();
    for (auto const &s : stencil) {
      Coord const neighbor = cur + s;
      if (visited[neighbor] || map[neighbor] == '#')
        continue;
      dist[neighbor] = std::min(dist[neighbor], dist[cur] + 1u);
      next.push(neighbor);
    }
  }
  return dist;
}

} // namespace

template <> size_t part1<2019, 15>(std::string_view const input) {
  auto [map, start, goal] = exploreMap(input);
  DEBUG_ASSERT(map[start] == '.', start);
  DEBUG_ASSERT(map[goal] == 'X', goal);

  return aStar(map, start, goal);
}

template <> size_t part2<2019, 15>(std::string_view const input) {
  auto [map, start, goal] = exploreMap(input);
  Grid2d<unsigned> dist = dijkstra(map, goal);
  unsigned maxDist = 0u;
  for (int y = 0; y < dist.ySize(); ++y)
    for (int x = 0; x < dist.xSize(); ++x)
      if (map[x, y] == '.')
        maxDist = std::max(maxDist, dist[x, y]);
  return maxDist;
}
