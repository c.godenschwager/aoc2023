#include "LinewiseInput.h"
#include "Parsing.h"
#include "PuzzleImpl.h"

#include <algorithm>
#include <array>
#include <cstdint>
#include <numeric>
#include <string_view>

namespace {

uint64_t parseElveCalories(std::span<std::string_view const> s) {
  return std::transform_reduce(s.begin(), s.end(), uint64_t(0), std::plus<>(), parseInt<uint64_t>);
}
} // namespace

template <> size_t part1<2022, 1>(std::string_view const input) {
  LinewiseInput const lines(input);

  auto linesPerElve = lines.splitOnEmptyLines();

  return std::transform_reduce(
      linesPerElve.begin(), linesPerElve.end(), uint64_t(0),
      [](auto lhs, auto rhs) { return std::max(lhs, rhs); }, parseElveCalories);
}

template <> size_t part2<2022, 1>(std::string_view const input) {
  LinewiseInput const lines(input);

  auto linesPerElve = lines.splitOnEmptyLines();
  std::vector<uint64_t> calories;
  calories.reserve(linesPerElve.size());
  std::ranges::transform(linesPerElve, std::back_inserter(calories), parseElveCalories);

  std::partial_sort(calories.begin(), std::next(calories.begin(), 3), calories.end(),
                    [](auto lhs, auto rhs) { return lhs > rhs; });

  return std::reduce(calories.begin(), std::next(calories.begin(), 3));
}
