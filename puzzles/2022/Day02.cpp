#include "IntegerCast.h"
#include "LinewiseInput.h"
#include "PuzzleImpl.h"

#include <libassert/assert.hpp>

#include <algorithm>
#include <array>
#include <cstdint>
#include <numeric>
#include <string_view>

namespace {

uint64_t gameScorePt1(std::string_view const s) {
  char const opponent = s[0];
  char const own = s[2];

  switch (own) {
  case 'X':
    switch (opponent) {
    case 'A':
      return 1u + 3u;
    case 'B':
      return 1u + 0u;
    case 'C':
      return 1u + 6u;
    default:
      UNREACHABLE(opponent);
    }
  case 'Y':
    switch (opponent) {
    case 'A':
      return 2u + 6u;
    case 'B':
      return 2u + 3u;
    case 'C':
      return 2u + 0u;
    default:
      UNREACHABLE(opponent);
    }
  case 'Z':
    switch (opponent) {
    case 'A':
      return 3u + 0u;
    case 'B':
      return 3u + 6u;
    case 'C':
      return 3u + 3u;
    default:
      UNREACHABLE(opponent);
    }
  default:
    UNREACHABLE(own);
  }
}

uint64_t gameScorePt2(std::string_view const s) {
  char const opponent = s[0];
  char const outcome = s[2];

  switch (outcome) {
  case 'X':
    switch (opponent) {
    case 'A':
      return 0u + 3u;
    case 'B':
      return 0u + 1u;
    case 'C':
      return 0u + 2u;
    default:
      UNREACHABLE(opponent);
    }
  case 'Y':
    switch (opponent) {
    case 'A':
      return 3u + 1u;
    case 'B':
      return 3u + 2u;
    case 'C':
      return 3u + 3u;
    default:
      UNREACHABLE(opponent);
    }
  case 'Z':
    switch (opponent) {
    case 'A':
      return 6u + 2u;
    case 'B':
      return 6u + 3u;
    case 'C':
      return 6u + 1u;
    default:
      UNREACHABLE(opponent);
    }
  default:
    UNREACHABLE(outcome);
  }
}
} // namespace

template <> size_t part1<2022, 2>(std::string_view const input) {
  LinewiseInput const lines(input);

  return std::transform_reduce(lines.begin(), lines.end(), uint64_t(0), std::plus<>(),
                               gameScorePt1);
}

template <> size_t part2<2022, 2>(std::string_view const input) {
  LinewiseInput const lines(input);

  return std::transform_reduce(lines.begin(), lines.end(), uint64_t(0), std::plus<>(),
                               gameScorePt2);
}
