#include "IntegerCast.h"
#include "LinewiseInput.h"
#include "PuzzleImpl.h"

#include <libassert/assert.hpp>
#include <re2/re2.h>

#include <algorithm>
#include <cctype>
#include <numeric>
#include <ranges>
#include <string_view>

namespace {

struct Interval {
  size_t min = 0u;
  size_t max = 0u;
};

bool fullOverlap(Interval a, Interval b) {
  if (a.min < b.min)
    return a.max >= b.max;
  else if (b.min < a.min)
    return b.max >= a.max;
  else
    return true;
}

bool overlap(Interval a, Interval b) {
  if (a.min < b.min)
    return a.max >= b.min;
  else if (b.min < a.min)
    return b.max >= a.min;
  else
    return true;
}

std::array<Interval, 2u> parse(std::string_view const s) {
  Interval a, b;
  static RE2 const boxPattern(R"((\d+)-(\d+),(\d+)-(\d+))");
  ASSUME(RE2::FullMatch(s, boxPattern, &a.min, &a.max, &b.min, &b.max));
  return {a, b};
}

} // namespace

template <> size_t part1<2022, 4>(std::string_view const input) {
  return std::ranges::distance(
      input | std::views::split('\n') |
      std::views::transform([](auto &&r) { return parse(std::string_view(r)); }) |
      std::views::filter(
          [](auto const intervals) { return fullOverlap(intervals[0], intervals[1]); }));
}

template <> size_t part2<2022, 4>(std::string_view const input) {
  return std::ranges::distance(
      input | std::views::split('\n') |
      std::views::transform([](auto &&r) { return parse(std::string_view(r)); }) |
      std::views::filter([](auto const intervals) { return overlap(intervals[0], intervals[1]); }));
}
