#include "IntegerCast.h"
#include "LinewiseInput.h"
#include "PuzzleImpl.h"

#include <libassert/assert.hpp>

#include <algorithm>
#include <cctype>
#include <cstdint>
#include <iostream>
#include <numeric>
#include <string_view>

namespace {

char commonElement(std::string_view const contents) {
  auto middle = std::next(contents.begin(), integerCast<int64_t>(contents.size() / 2u));
  std::string a{contents.begin(), middle};
  std::string b{middle, contents.end()};
  std::ranges::sort(a);
  std::ranges::sort(b);
  auto aIt = a.begin();
  auto bIt = b.begin();
  while (aIt != a.end() && bIt != b.end()) {
    if (*aIt < *bIt)
      ++aIt;
    else if (*bIt < *aIt)
      ++bIt;
    else
      return *aIt;
  }
  PANIC("No common element!");
}

char findBadge(std::span<std::string_view const, 3u> const contents) {
  std::string result{contents.front()};
  std::ranges::sort(result);

  for (size_t i = 1; i < contents.size(); ++i) {
    std::string tmp{contents[i]};
    std::ranges::sort(tmp);
    result.erase(std::ranges::set_intersection(result, tmp, result.begin()).out, result.end());
  }
  ASSUME(!result.empty(), contents, result);
  ASSUME(result.front() == result.back(), contents, result);
  return result.front();
}

size_t priority(char const c) { return (std::islower(c)) ? 1u + c - 'a' : 27u + c - 'A'; }

} // namespace

template <> size_t part1<2022, 3>(std::string_view const input) {
  LinewiseInput const lines(input);
  return std::transform_reduce(
      lines.begin(), lines.end(), size_t(0), std::plus{},
      [](std::string_view const line) { return priority(commonElement(line)); });
}

template <> size_t part2<2022, 3>(std::string_view const input) {
  LinewiseInput const lines(input);

  std::vector<std::span<std::string_view const, 3u>> groups;
  groups.reserve(lines.size() / 3u);
  for (auto it = lines.begin(); it < lines.end(); it += 3) {
    groups.emplace_back(it, std::next(it, 3));
  }

  return std::transform_reduce(
      groups.begin(), groups.end(), size_t(0), std::plus{},
      [](std::span<std::string_view const, 3u> const group) { return priority(findBadge(group)); });
}
