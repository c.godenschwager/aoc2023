#include "Grid2d.h"
#include "IntegerCast.h"
#include "LinewiseInput.h"
#include "PuzzleImpl.h"

#include <algorithm>
#include <array>
#include <cstdint>
#include <numeric>
#include <string_view>

namespace {

Grid2d<unsigned char> computeVisibility(Grid2d<signed char> const &treeHeights) {
  Grid2d<unsigned char> visibility(treeHeights.xSize(), treeHeights.ySize(), 0);

  for (int y = 0; y < treeHeights.ySize(); ++y) {
    signed char visibleHeight = -1;
    for (int x = 0; x < treeHeights.xSize(); ++x) {
      if (treeHeights[x, y] > visibleHeight) {
        visibility[x, y] = 1;
        visibleHeight = treeHeights[x, y];
      }
    }
    visibleHeight = -1;
    for (int x = treeHeights.xSize() - 1; x >= 0; --x) {
      if (treeHeights[x, y] > visibleHeight) {
        visibility[x, y] = 1;
        visibleHeight = treeHeights[x, y];
      }
    }
  }

  for (int x = 0; x < treeHeights.xSize(); ++x) {
    signed char visibleHeight = -1;
    for (int y = 0; y < treeHeights.ySize(); ++y) {
      if (treeHeights[x, y] > visibleHeight) {
        visibility[x, y] = 1;
        visibleHeight = treeHeights[x, y];
      }
    }
    visibleHeight = -1;
    for (int y = treeHeights.ySize() - 1; y >= 0; --y) {
      if (treeHeights[x, y] > visibleHeight) {
        visibility[x, y] = 1;
        visibleHeight = treeHeights[x, y];
      }
    }
  }

  return visibility;
}

int computeScenicScore(Grid2d<signed char> const &treeHeights) {

  Grid2d<int> scenicScore(treeHeights.xSize(), treeHeights.ySize(), 0);

  for (int y = 1; y < treeHeights.ySize() - 1; ++y) {
    for (int x = 1; x < treeHeights.xSize() - 1; ++x) {
      scenicScore[x, y] = 1;
      int c = 0;
      for (c = x - 1; c >= 1 && treeHeights[c, y] < treeHeights[x, y]; --c)
        ;
      scenicScore[x, y] *= x - c;
      for (c = x + 1; c < treeHeights.xSize() - 1 && treeHeights[c, y] < treeHeights[x, y]; ++c)
        ;
      scenicScore[x, y] *= c - x;
      for (c = y - 1; c >= 1 && treeHeights[x, c] < treeHeights[x, y]; --c)
        ;
      scenicScore[x, y] *= y - c;
      for (c = y + 1; c < treeHeights.ySize() - 1 && treeHeights[x, c] < treeHeights[x, y]; ++c)
        ;
      scenicScore[x, y] *= c - y;
    }
  }

  return scenicScore.max();
}

} // namespace

template <> size_t part1<2022, 8>(std::string_view const input) {
  LinewiseInput lines(input);
  Grid2d<char> inputGrid = lines.makeCharGrid2d();

  Grid2d<signed char> treeHeights =
      inputGrid.map([](char const c) { return static_cast<signed char>(c - '0'); });

  Grid2d<unsigned char> visibility = computeVisibility(treeHeights);

  return visibility.count(1);
}

template <> size_t part2<2022, 8>(std::string_view const input) {
  LinewiseInput lines(input);
  Grid2d<char> inputGrid = lines.makeCharGrid2d();

  Grid2d<signed char> treeHeights =
      inputGrid.map([](char const c) { return static_cast<signed char>(c - '0'); });

  return computeScenicScore(treeHeights);
}
