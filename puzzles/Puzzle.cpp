#include "Puzzle.h"
#include "InputDir.h"
#include "PuzzleImpl.h"

#include <libassert/assert.hpp>

#include <format>
#include <map>
#include <string>
#include <string_view>

std::string const Puzzle::name() const { return std::format("{}_{}", year(), day()); }

using FactoryFunction = std::unique_ptr<Puzzle> (*)();

static std::map<Puzzle::Year, std::map<Puzzle::Day, FactoryFunction>> &getFactoryFunctions() {
  static std::map<Puzzle::Year, std::map<Puzzle::Day, FactoryFunction>> factoryFunctions;
  return factoryFunctions;
}

static bool registerConcretePuzzle(Puzzle::Year const y, Puzzle::Day const d,
                                   FactoryFunction const f) {
  auto &factoryFunctions = getFactoryFunctions();

  auto [it, yearInserted] = factoryFunctions.try_emplace(y);
  auto [it2, dayInserted] = it->second.emplace(d, f);

  return dayInserted;
}

template <int YEAR, unsigned DAY, size_t SOLUTION_PART1, size_t SOLUTION_PART2>
class LegacyConcretePuzzle final : public Puzzle {
public:
  LegacyConcretePuzzle()
      : Puzzle(Year(YEAR), Day(DAY),
               std::filesystem::path(inputDir) / std::to_string(YEAR) /
                   std::filesystem::path(std::format("Day{:0>2}.txt", DAY))),
        _solutionPart1(std::to_string(SOLUTION_PART1)),
        _solutionPart2(std::to_string(SOLUTION_PART2)) {}

  [[nodiscard]] std::string runPart1(std::string_view const input) const final {
    return std::to_string(part1<YEAR, DAY>(input));
  }
  [[nodiscard]] std::string runPart2(std::string_view const input) const final {
    return std::to_string(part2<YEAR, DAY>(input));
  }

  [[nodiscard]] std::string_view solutionPart1() const final { return _solutionPart1; };
  [[nodiscard]] std::string_view solutionPart2() const final { return _solutionPart2; };

  static std::unique_ptr<Puzzle> create() { return std::make_unique<LegacyConcretePuzzle>(); }
  static inline bool const registered = registerConcretePuzzle(Year(YEAR), Day(DAY), create);

  std::string _solutionPart1;
  std::string _solutionPart2;
};

template <int YEAR, unsigned DAY> class ConcretePuzzle final : public Puzzle {
public:
  ConcretePuzzle()
      : Puzzle(Year(YEAR), Day(DAY),
               std::filesystem::path(inputDir) / std::to_string(YEAR) /
                   std::filesystem::path(std::format("Day{:0>2}.txt", DAY))) {}

  [[nodiscard]] std::string runPart1(std::string_view const input) const final {
    return solvePart1<YEAR, DAY>(input);
  }
  [[nodiscard]] std::string runPart2(std::string_view const input) const final {
    return solvePart2<YEAR, DAY>(input);
  }

  [[nodiscard]] std::string_view solutionPart1() const final { return solution1<YEAR, DAY>(); };
  [[nodiscard]] std::string_view solutionPart2() const final { return solution2<YEAR, DAY>(); };

  static std::unique_ptr<Puzzle> create() { return std::make_unique<ConcretePuzzle>(); }
  static inline bool const registered = registerConcretePuzzle(Year(YEAR), Day(DAY), create);
};

template class LegacyConcretePuzzle<2015, 1, 138, 1771>;
template class LegacyConcretePuzzle<2015, 2, 1586300, 3737498>;
template class LegacyConcretePuzzle<2015, 3, 2081, 2341>;
template class LegacyConcretePuzzle<2015, 4, 117946, 3938038>;
template class LegacyConcretePuzzle<2015, 5, 238, 69>;
template class LegacyConcretePuzzle<2015, 6, 377891, 14110788>;

template class LegacyConcretePuzzle<2019, 2, 6327510, 4112>;
template class LegacyConcretePuzzle<2019, 5, 11933517, 10428568>;
template class LegacyConcretePuzzle<2019, 7, 212460, 21844737>;
template class LegacyConcretePuzzle<2019, 9, 2870072642, 58534>;
template class LegacyConcretePuzzle<2019, 11, 2172, 248>;
template class LegacyConcretePuzzle<2019, 13, 228, 10776>;
template class LegacyConcretePuzzle<2019, 15, 252, 350>;
template class LegacyConcretePuzzle<2019, 17, 5724, 0>;

template class LegacyConcretePuzzle<2022, 1, 67027, 197291>;
template class LegacyConcretePuzzle<2022, 2, 14531, 11258>;
template class LegacyConcretePuzzle<2022, 3, 7674, 2805>;
template class LegacyConcretePuzzle<2022, 4, 503, 827>;
template class LegacyConcretePuzzle<2022, 8, 1688, 410400>;

template class LegacyConcretePuzzle<2023, 1, 56465, 55902>;
template class LegacyConcretePuzzle<2023, 2, 3035, 66027>;
template class LegacyConcretePuzzle<2023, 3, 529618, 77509019>;
template class LegacyConcretePuzzle<2023, 4, 21485, 11024379>;
template class LegacyConcretePuzzle<2023, 5, 331445006, 6472060>;
template class LegacyConcretePuzzle<2023, 6, 503424, 32607562>;
template class LegacyConcretePuzzle<2023, 7, 252656917, 253499763>;
template class LegacyConcretePuzzle<2023, 8, 12361, 18215611419223>;
template class LegacyConcretePuzzle<2023, 9, 1972648895, 919>;
template class LegacyConcretePuzzle<2023, 10, 6909, 461>;
template class LegacyConcretePuzzle<2023, 11, 9681886, 791134099634>;
template class LegacyConcretePuzzle<2023, 12, 6958, 6555315065024>;
template class LegacyConcretePuzzle<2023, 13, 32035, 24847>;
template class LegacyConcretePuzzle<2023, 14, 103333, 97241>;
template class LegacyConcretePuzzle<2023, 15, 494980, 247933>;
template class LegacyConcretePuzzle<2023, 16, 7951, 8148>;
template class LegacyConcretePuzzle<2023, 17, 684, 822>;
template class LegacyConcretePuzzle<2023, 18, 35401, 48020869073824>;
template class LegacyConcretePuzzle<2023, 19, 397643, 132392981697081>;
template class LegacyConcretePuzzle<2023, 20, 836127690, 240914003753369>;
template class LegacyConcretePuzzle<2023, 21, 3716, 0>;
template class LegacyConcretePuzzle<2023, 22, 505, 71002>;
template class LegacyConcretePuzzle<2023, 23, 2246, 6622>;
template class LegacyConcretePuzzle<2023, 24, 15262, 0>;
template class LegacyConcretePuzzle<2023, 25, 543256, 0>;

template class LegacyConcretePuzzle<2024, 1, 1603498, 25574739>;
template class LegacyConcretePuzzle<2024, 2, 680, 710>;
template class LegacyConcretePuzzle<2024, 3, 161289189, 83595109>;
template class LegacyConcretePuzzle<2024, 4, 2547, 1939>;
template class LegacyConcretePuzzle<2024, 5, 6242, 5169>;
template class LegacyConcretePuzzle<2024, 6, 4722, 1602>;
template class LegacyConcretePuzzle<2024, 7, 4998764814652, 37598910447546>;
template class LegacyConcretePuzzle<2024, 8, 305, 1150>;
template class LegacyConcretePuzzle<2024, 9, 6448989155953, 6476642796832>;
template class LegacyConcretePuzzle<2024, 10, 611, 1380>;
template class LegacyConcretePuzzle<2024, 11, 186175, 220566831337810>;
template class LegacyConcretePuzzle<2024, 12, 1494342, 893676>;
template class LegacyConcretePuzzle<2024, 13, 25629, 107487112929999>;
template class LegacyConcretePuzzle<2024, 14, 216772608, 6888>;
template class LegacyConcretePuzzle<2024, 15, 1448589, 1472235>;
template class LegacyConcretePuzzle<2024, 16, 127520, 565>;
template class ConcretePuzzle<2024, 17>;
template class ConcretePuzzle<2024, 18>;
template class LegacyConcretePuzzle<2024, 19, 369, 761826581538190>;
template class LegacyConcretePuzzle<2024, 20, 1360, 1005476>;
template class LegacyConcretePuzzle<2024, 22, 14869099597, 1717>;
template class ConcretePuzzle<2024, 23>;
template class ConcretePuzzle<2024, 25>;

Puzzle::Year Puzzle::latestYear() {
  auto const &factoryFunctions = getFactoryFunctions();
  ASSUME(!factoryFunctions.empty());
  return std::prev(factoryFunctions.end())->first;
}

Puzzle::Day Puzzle::latestDay(Year const year) {
  auto const &factoryFunctions = getFactoryFunctions();
  auto it = factoryFunctions.find(year);
  ASSUME(it != factoryFunctions.end());
  ASSUME(!it->second.empty());
  return std::prev(it->second.end())->first;
};

std::unique_ptr<Puzzle> Puzzle::create(Year const year, Day const day) {
  auto const &factoryFunctions = getFactoryFunctions();
  auto it = factoryFunctions.find(year);
  if (it == factoryFunctions.end())
    return nullptr;
  auto it2 = it->second.find(day);
  if (it2 == it->second.end())
    return nullptr;
  return (it2->second)();
}

std::vector<std::unique_ptr<Puzzle>> Puzzle::createAll() {
  auto const &factoryFunctions = getFactoryFunctions();
  std::vector<std::unique_ptr<Puzzle>> puzzles;

  for (auto const &[year, dayMap] : factoryFunctions)
    for (auto const &[day, func] : dayMap)
      puzzles.push_back(func());

  return puzzles;
}

std::vector<std::unique_ptr<Puzzle>> Puzzle::createAll(Year const year) {
  auto const &factoryFunctions = getFactoryFunctions();
  std::vector<std::unique_ptr<Puzzle>> puzzles;

  auto it = factoryFunctions.find(year);
  if (it != factoryFunctions.end()) {
    for (auto [day, func] : it->second)
      puzzles.push_back(func());
  }

  return puzzles;
}