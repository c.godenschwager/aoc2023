#include "Grid2d.h"
#include "LinewiseInput.h"
#include "PuzzleImpl.h"

#include <fmt/format.h>
#include <fmt/ranges.h>
#include <re2/re2.h>

#include <algorithm>
#include <string_view>

namespace {
struct Operation {
  Operation(std::string_view const s) {
    static RE2 const pattern(R"((turn on|turn off|toggle) (\d+),(\d+) through (\d+),(\d+))");
    std::string_view operation;
    RE2::FullMatch(s, pattern, &operation, &begin.x(), &begin.y(), &end.x(), &end.y());
    if (operation == "turn on") {
      op = ON;
    } else if (operation == "turn off") {
      op = OFF;
    } else {
      op = TOGGLE;
    }

    ++end.x();
    ++end.y();
  }

  void applyPt1(Grid2d<char> &grid) {
    switch (op) {
    case ON:
      grid.subgrid(begin, end).forEach([](char &e) { e = 1; });
      break;
    case OFF:
      grid.subgrid(begin, end).forEach([](char &e) { e = 0; });
      break;
    case TOGGLE:
      grid.subgrid(begin, end).forEach([](char &e) { e = e == 0 ? 1 : 0; });
    }
  }

  void applyPt2(Grid2d<int> &grid) {
    switch (op) {
    case ON:
      grid.subgrid(begin, end).forEach([](int &e) { e += 1; });
      break;
    case OFF:
      grid.subgrid(begin, end).forEach([](int &e) { e = std::max(e - 1, 0); });
      break;
    case TOGGLE:
      grid.subgrid(begin, end).forEach([](int &e) { e += 2; });
    }
  }

  enum Op : std::uint8_t { ON, OFF, TOGGLE };

  Grid2d<char>::Coord begin;
  Grid2d<char>::Coord end;
  Op op;
};

} // namespace

template <> size_t part1<2015, 6>(std::string_view const input) {
  LinewiseInput lines(input);
  Grid2d<char> grid(1000, 1000, 0);
  static RE2 const pattern(R"((turn on|turn off|toggle) (\d+),(\d+) through (\d+),(\d+))");
  for (auto const &l : getLineWise(input)) {
    Operation(l).applyPt1(grid);
  }
  return grid.count(1);
}

template <> size_t part2<2015, 6>(std::string_view const input) {
  Grid2d<int> grid(1000, 1000, 0);
  static RE2 const pattern(R"((turn on|turn off|toggle) (\d+),(\d+) through (\d+),(\d+))");
  for (auto const &l : getLineWise(input)) {
    Operation(l).applyPt2(grid);
  }
  return grid.sum();
}