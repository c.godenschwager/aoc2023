#include "LinewiseInput.h"
#include "PuzzleImpl.h"

#include <libassert/assert.hpp>

#include <algorithm>
#include <array>
#include <iterator>
#include <string_view>
#include <unordered_set>

namespace {

using namespace std::string_view_literals;

bool hasThreeVowels(std::string_view const word) {
  static std::string_view constexpr vowels{"aeiou"};
  return std::ranges::count_if(word, [&](char const c) { return vowels.contains(c); }) >= 3u;
}

bool containsDoubleLetter(std::string_view const word) {
  return std::ranges::adjacent_find(word) != word.end();
}

bool noBadSubstrings(std::string_view const word) {
  static std::array constexpr badSubstrings = {"ab"sv, "cd"sv, "pq"sv, "xy"sv};
  return std::ranges::none_of(badSubstrings, [&](std::string_view const badSubstring) {
    return word.contains(badSubstring);
  });
}

bool hasDoubleLetterPair(std::string_view const word) {
  for (auto it = word.begin(); it != std::prev(word.end(), 1); ++it) {
    std::string_view const pair(it, 2u);
    std::string_view const remainder(std::next(it, 2), word.end());
    if (remainder.contains(pair))
      return true;
  }
  return false;
}

bool hasMirrorRepeat(std::string_view const word) {
  for (auto it = word.begin(); it != std::prev(word.end(), 2); ++it) {
    std::string_view const letters(it, 3u);
    if (letters.front() == letters.back())
      return true;
  }
  return false;
}

} // namespace

template <> size_t part1<2015, 5>(std::string_view const input) {
  LinewiseInput lines(input);
  return std::ranges::count_if(lines, [](std::string_view const line) {
    return hasThreeVowels(line) && containsDoubleLetter(line) && noBadSubstrings(line);
  });
}

template <> size_t part2<2015, 5>(std::string_view const input) {
  LinewiseInput lines(input);
  return std::ranges::count_if(lines, [](std::string_view const line) {
    return hasDoubleLetterPair(line) && hasMirrorRepeat(line);
  });
}