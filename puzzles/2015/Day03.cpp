#include "PuzzleImpl.h"
#include "Vector.h"

#include <libassert/assert.hpp>

#include <array>
#include <unordered_set>

namespace {
using Coord = Vec2<int>;

constexpr std::array<Coord, 128u> makeDirLut() {
  std::array<Coord, 128u> lut{};
  lut['^'] = {0, 1};
  lut['>'] = {1, 0};
  lut['v'] = {0, -1};
  lut['<'] = {-1, 0};
  return lut;
}

static constexpr std::array<Coord, 128u> dirLut = makeDirLut();

} // namespace

template <> size_t part1<2015, 3>(std::string_view const input) {
  std::unordered_set<Coord> visitedHouses;
  Coord coord{0, 0};
  visitedHouses.insert(coord);
  for (char const c : input) {
    coord += dirLut[c];
    visitedHouses.insert(coord);
  }
  return visitedHouses.size();
}

template <> size_t part2<2015, 3>(std::string_view const input) {
  std::unordered_set<Coord> visitedHouses;
  std::array<Coord, 2u> coords = {{{0, 0}, {0, 0}}};
  visitedHouses.emplace(0, 0);
  unsigned int coordIdx = 0;
  for (char const c : input) {
    coords[coordIdx] += dirLut[c];
    visitedHouses.insert(coords[coordIdx]);
    coordIdx ^= 1u;
  }
  return visitedHouses.size();
}