#include "PuzzleImpl.h"

#include <libassert/assert.hpp>
#include <md5.h>

#include <algorithm>
#include <array>
#include <charconv>
#include <string_view>
#include <system_error>

namespace {

using Hash = std::array<unsigned char, 16>;

size_t findHash(std::string_view const key, auto &&stopCriterion) {
  std::array<char, 64> buffer{};
  Hash hash{};
  std::ranges::copy(key, buffer.begin());

  MD5 md5;

  for (size_t i = 0;; ++i) {
    md5.reset();
    auto const [ptr, ec] = std::to_chars(buffer.begin() + key.size(), buffer.end(), i);
    ASSUME(ec == std::errc{}, "Char conversion failed!", i);
    md5.add(buffer.begin(), std::distance(buffer.begin(), ptr));
    md5.getHash(hash.begin());
    if (stopCriterion(hash))
      return i;
  }
}

} // namespace

template <> size_t part1<2015, 4>(std::string_view const input) {

  return findHash(input, [](Hash const &hash) {
    return hash[0] == 0u && hash[1] == 0u && (hash[2] & 0xF0) == 0u;
  });
}

template <> size_t part2<2015, 4>(std::string_view const input) {
  return findHash(input,
                  [](Hash const &hash) { return hash[0] == 0u && hash[1] == 0u && hash[2] == 0u; });
}