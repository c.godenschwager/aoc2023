#include "LinewiseInput.h"
#include "PuzzleImpl.h"

#include <libassert/assert.hpp>
#include <re2/re2.h>

#include <algorithm>
#include <cstdint>
#include <numeric>

namespace {

struct Present {
  Present(std::string_view const s) {
    static RE2 const boxPattern(R"((\d+)x(\d+)x(\d+))");
    ASSUME(RE2::FullMatch(s, boxPattern, &l, &w, &h));
  }

  [[nodiscard]] uint64_t surfaceArea() const { return uint64_t(2) * (l * w + w * h + h * l); }
  [[nodiscard]] uint64_t volume() const { return l * w * h; }
  [[nodiscard]] uint64_t minFaceArea() const { return std::min({l * w, w * h, h * l}); }
  [[nodiscard]] uint64_t minCircumference() const {
    return uint64_t(2) * std::min({l + w, w + h, h + l});
  }

  [[nodiscard]] uint64_t requiredPaper() const { return surfaceArea() + minFaceArea(); }
  [[nodiscard]] uint64_t requiredRibbon() const { return minCircumference() + volume(); }

  uint64_t l = 0u;
  uint64_t w = 0u;
  uint64_t h = 0u;
};

} // namespace

template <> size_t part1<2015, 2>(std::string_view const input) {
  LinewiseInput const lines(input);
  return std::transform_reduce(lines.begin(), lines.end(), uint64_t(0), std::plus<>{},
                               [](std::string_view const s) { return Present(s).requiredPaper(); });
}

template <> size_t part2<2015, 2>(std::string_view const input) {
  LinewiseInput const lines(input);
  return std::transform_reduce(
      lines.begin(), lines.end(), uint64_t(0), std::plus<>{},
      [](std::string_view const s) { return Present(s).requiredRibbon(); });
}