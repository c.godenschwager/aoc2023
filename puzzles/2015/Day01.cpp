#include "PuzzleImpl.h"

#include <libassert/assert.hpp>

#include <algorithm>
#include <iostream>

template <> size_t part1<2015, 1>(std::string_view const input) {
  return 2 * std::ranges::count(input, '(') - input.size();
}

template <> size_t part2<2015, 1>(std::string_view const input) {
  int floor = 0;
  for (size_t pos = 0; pos < input.size(); ++pos) {
    floor += (input[pos] == '(') ? 1 : -1;
    if (floor == -1)
      return pos + 1;
  }
  UNREACHABLE();
}