#include "LinewiseInput.h"
#include "PuzzleImpl.h"
#include "Vector.h"

#include <libassert/assert.hpp>
#include <re2/re2.h>

#include <string_view>
#include <vector>

namespace {

struct Hailstone2d {
  Hailstone2d(std::string_view const line) {
    static const RE2 pattern = R"((\d+), +(\d+), +\d+ @ +(-?\d+), +(-?\d+), +-?\d+)";
    ASSUME(RE2::FullMatch(line, pattern, &pos[0], &pos[1], &vel[0], &vel[1]));
  }

  Vec2<double> pos;
  Vec2<double> vel;
};

std::optional<Vec2<double>> intersect(Hailstone2d const &a, Hailstone2d const &b) {
  double const dx = b.pos[0] - a.pos[0];
  double const dy = b.pos[1] - a.pos[1];
  double const det = b.vel[0] * a.vel[1] - b.vel[1] * a.vel[0];
  double const u = (dy * b.vel[0] - dx * b.vel[1]) / det;
  double const v = (dy * a.vel[0] - dx * a.vel[1]) / det;
  if (u > 0.0 && v > 0.0)
    return a.pos + a.vel * u;
  else
    return std::nullopt;
}

template <size_t N> bool inTestArea(Vector<N, double> const &v) {
  return std::ranges::all_of(
      v, [](double const d) { return d >= 200000000000000.0 && d <= 400000000000000.0; });
}

} // namespace

template <> size_t part1<2023, 24>(std::string_view const input) {
  LinewiseInput lines(input);

  std::vector<Hailstone2d> hailstones(lines.begin(), lines.end());

  size_t ctr = 0;
  for (auto it = hailstones.begin(); it != hailstones.end(); ++it)
    for (auto it2 = std::next(it); it2 != hailstones.end(); ++it2) {
      auto intersectPoint = intersect(*it, *it2);
      if (intersectPoint && inTestArea(*intersectPoint))
        ++ctr;
    }

  return ctr;
}

template <> size_t part2<2023, 24>(std::string_view const /*input*/) { return 0; }
