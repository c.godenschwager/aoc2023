#include "Grid2d.h"
#include "LinewiseInput.h"
#include "PuzzleImpl.h"

#include <libassert/assert.hpp>
#include <re2/re2.h>

#include <algorithm>
#include <execution>
#include <iostream>
#include <numeric>
#include <stack>
#include <string_view>

namespace {

using GridView = Grid2dSpan<char>;
using VisitedGridView = Grid2dSpan<uint8_t>;
using Coord = GridView::Coord;

enum Dir : uint8_t { N = 0, E = 1, S = 2, W = 3 };
constexpr std::array<Coord, 4u> stencil = {{{0, 1}, {1, 0}, {0, -1}, {-1, 0}}};

Grid2d<uint8_t> makeVisitedGrid(int xSize, int ySize) {
  Grid2d<uint8_t> visited(xSize, ySize, 0b1111, 1);
  for (int y = 0; y < visited.ySize(); ++y)
    for (int x = 0; x < visited.xSize(); ++x)
      visited[x, y] = 0;
  return visited;
}

size_t walkGrid(GridView const g, Coord startPos, Dir startDir) {
  Grid2d<uint8_t> visited = makeVisitedGrid(g.xSize(), g.ySize());
  std::stack<std::pair<Coord, Dir>> startPoints;
  startPoints.emplace(startPos, startDir);

  while (!startPoints.empty()) {
    auto [c, d] = startPoints.top();
    startPoints.pop();

    while ((visited[c] & 1u << d) == 0u) {
      visited[c] |= 1u << d;

      switch (g[c]) {
      case '.':
        break;
      case '|':
        switch (d) {
        case N:
        case S:
          break;
        case E:
        case W:
          d = N;
          startPoints.emplace(c + stencil[S], S);
          break;
        default:
          UNREACHABLE();
        }
        break;
      case '-':
        switch (d) {
        case N:
        case S:
          d = E;
          startPoints.emplace(c + stencil[W], W);
          break;
        case E:
        case W:
          break;
        default:
          UNREACHABLE();
        }
        break;
      case '/':
        switch (d) {
        case N:
          d = E;
          break;
        case S:
          d = W;
          break;
        case E:
          d = N;
          break;
        case W:
          d = S;
          break;
        default:
          UNREACHABLE();
        }
        break;
      case '\\':
        switch (d) {
        case N:
          d = W;
          break;
        case S:
          d = E;
          break;
        case E:
          d = S;
          break;
        case W:
          d = N;
          break;
        default:
          UNREACHABLE();
        }
        break;
      default:
        UNREACHABLE();
      }
      c += stencil[d];
    }
  }
  return visited.count_if([](auto const &bits) { return bits != 0; });
}

} // namespace

template <> size_t part1<2023, 16>(std::string_view const input) {
  LinewiseInput lines(input);
  Grid2d<char> grid = lines.makeCharGrid2d();

  return walkGrid(grid, {0, grid.ySize() - 1}, E);
}

template <> size_t part2<2023, 16>(std::string_view const input) {
  LinewiseInput lines(input);
  Grid2d<char> grid = lines.makeCharGrid2d();

  std::vector<std::pair<Coord, Dir>> startPoints;
  startPoints.reserve(2 * grid.xSize() + 2 * grid.ySize());

  for (int y = 0; y < grid.ySize(); ++y) {
    startPoints.emplace_back(Coord{0, y}, E);
    startPoints.emplace_back(Coord{grid.xSize() - 1, y}, W);
  }
  for (int x = 0; x < grid.xSize(); ++x) {
    startPoints.emplace_back(Coord{x, 0}, N);
    startPoints.emplace_back(Coord{x, grid.ySize() - 1}, S);
  }

  return std::transform_reduce(
      std::execution::par, startPoints.begin(), startPoints.end(), size_t(0),
      [](size_t lhs, size_t rhs) { return std::max(lhs, rhs); },
      [&grid](auto const &startPos) { return walkGrid(grid, startPos.first, startPos.second); });
}
