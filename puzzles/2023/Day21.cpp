#include "Grid2d.h"
#include "LinewiseInput.h"
#include "PuzzleImpl.h"

#include <absl/container/flat_hash_set.h>
#include <libassert/assert.hpp>

#include <algorithm>
#include <string_view>

namespace {
using Coord = Grid2d<char>::Coord;
constexpr std::array<Coord, 4u> stencil{{{0, 1}, {0, -1}, {1, 0}, {-1, 0}}};

template <typename T> class PeriodicGridAdapter {
public:
  using IndexType = Grid2dSpan<T>::IndexType;
  using Coord = Grid2dSpan<T>::Coord;
  PeriodicGridAdapter(Grid2dSpan<T> const &grid) : _grid(grid) {}

  T const &operator[](Coord const &c) const { return (*this)[c.x(), c.y()]; }
  T const &operator[](IndexType x, IndexType y) const {
    x %= _grid.xSize();
    y %= _grid.ySize();
    x = x < 0 ? x + _grid.xSize() : x;
    y = y < 0 ? y + _grid.ySize() : y;
    return _grid[x, y];
  }

private:
  Grid2dSpan<T const> _grid;
};

} // namespace

template <> size_t part1<2023, 21>(std::string_view const input) {
  constexpr size_t numSteps = 64;

  LinewiseInput lines(input);
  Grid2d<char> grid = lines.makeCharGrid2dWithGhostLayers(1, '#');
  Grid2d<char> tmpGrid = grid;
  for (size_t i = 0; i < numSteps; ++i) {
    for (Coord c{0, 0}; c.y() < grid.ySize(); ++c.y())
      for (c.x() = 0; c.x() < grid.xSize(); ++c.x()) {
        if (grid[c] == '#')
          continue;

        tmpGrid[c] = '.';

        for (auto const &s : stencil) {
          Coord n = c + s;
          if (grid[n] == 'S') {
            tmpGrid[c] = 'S';
            break;
          }
        }
      }
    std::swap(grid, tmpGrid);
  }
  return grid.count('S');
}

template <> size_t part2<2023, 21>(std::string_view const) { return 0; }
