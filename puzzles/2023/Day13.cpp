#include "Grid2d.h"
#include "IntegerCast.h"
#include "LinewiseInput.h"
#include "PuzzleImpl.h"

#include <absl/container/flat_hash_map.h>
#include <absl/hash/hash.h>
#include <libassert/assert.hpp>

#include <execution>
#include <numeric>
#include <optional>
#include <string_view>

namespace {
using IndexType = Grid2dSpan<char>::IndexType;

class LineComparator {
public:
  LineComparator(Grid2dSpan<char const> const &grid) : _grid(grid) {
    _lineHashes.reserve(_grid.ySize());
    absl::Hash<Grid2dSpan<char const>> hasher;
    for (int y = 0; y < _grid.ySize(); ++y)
      _lineHashes.push_back(hasher(grid.line(y)));
  }

  bool operator()(IndexType const lhs, IndexType const rhs) const {
    if (_lineHashes[lhs] != _lineHashes[rhs])
      return false;

    return _grid.line(lhs) == _grid.line(rhs);
  }

private:
  Grid2dSpan<char const> _grid;
  std::vector<size_t> _lineHashes;
};

class ColumnComparator {
public:
  ColumnComparator(Grid2dSpan<char const> const &grid) : _grid(grid) {
    _columnHashes.reserve(_grid.xSize());
    absl::Hash<Grid2dSpan<char const>> hasher;
    for (int x = 0; x < _grid.xSize(); ++x)
      _columnHashes.push_back(hasher(grid.column(x)));
  }

  bool operator()(IndexType const lhs, IndexType const rhs) const {
    if (_columnHashes[lhs] != _columnHashes[rhs])
      return false;

    return _grid.column(lhs) == _grid.column(rhs);
  }

private:
  Grid2dSpan<char const> _grid;
  std::vector<size_t> _columnHashes;
};

class FuzzyLineComparator {
public:
  FuzzyLineComparator(Grid2dSpan<char const> const &grid) : _grid(grid) {}

  bool operator()(IndexType const lhs, IndexType const rhs) const {
    bool missmatch = false;
    for (IndexType x = 0; x < _grid.xSize(); ++x) {
      if (_grid[x, lhs] != _grid[x, rhs]) {
        if (missmatch)
          return false;
        missmatch = true;
      }
    }
    return true;
  }

private:
  Grid2dSpan<char const> _grid;
};

class FuzzyColumnComparator {
public:
  FuzzyColumnComparator(Grid2dSpan<char const> const &grid) : _grid(grid) {}

  bool operator()(IndexType const lhs, IndexType const rhs) const {
    bool missmatch = false;
    for (IndexType y = 0; y < _grid.ySize(); ++y) {
      if (_grid[lhs, y] != _grid[rhs, y]) {
        if (missmatch)
          return false;
        missmatch = true;
      }
    }
    return true;
  }

private:
  Grid2dSpan<char const> _grid;
};

std::optional<IndexType> findReflectionPoint(IndexType const size, auto const &comp) {

  std::optional<IndexType> reflectionLocation;
  IndexType const last = size - 1;

  auto checkReflection = [&](IndexType x1, IndexType x2) {
    if (comp(x1, x2)) {
      bool isReflection = true;
      while (isReflection && x1 < x2) {
        isReflection = comp(x1++, x2--);
      }
      if (isReflection) {
        reflectionLocation = x2;
      }
    }
  };

  for (IndexType x = last % 2 == 1 ? 0 : 1; x < size && !reflectionLocation; x += 2)
    checkReflection(x, last);

  for (IndexType x = last % 2 == 1 ? last : last - 1; x >= 0 && !reflectionLocation; x -= 2)
    checkReflection(0, x);

  return reflectionLocation;
}

std::optional<IndexType> findReflectionPointFuzzy(IndexType const size, auto const &comp,
                                                  auto const &fuzzyComp) {

  std::optional<IndexType> reflectionLocation;
  IndexType const last = size - 1;

  auto checkReflection = [&](IndexType x1, IndexType x2) {
    if (fuzzyComp(x1, x2)) {
      bool fuzzy = false;
      bool isReflection = true;
      while (isReflection && x1 < x2) {
        isReflection = comp(x1, x2);
        if (!isReflection && !fuzzy) {
          fuzzy = true;
          isReflection = fuzzyComp(x1, x2);
        }
        ++x1;
        --x2;
      }
      if (isReflection && fuzzy) {
        reflectionLocation = x2;
      }
    }
  };

  for (IndexType x = last % 2 == 1 ? 0 : 1; x < size && !reflectionLocation; x += 2)
    checkReflection(x, last);

  for (IndexType x = last % 2 == 1 ? last : last - 1; x >= 0 && !reflectionLocation; x -= 2)
    checkReflection(0, x);

  return reflectionLocation;
}

} // namespace

template <> size_t part1<2023, 13>(std::string_view const input) {
  LinewiseInput lines(input);
  auto linesPerGrid = lines.splitOnEmptyLines();

  return std::transform_reduce(
      std::execution::par, linesPerGrid.begin(), linesPerGrid.end(), std::size_t(0), std::plus<>(),
      [](auto const &gridLines) {
        Grid2d<char> const grid = makeCharGrid2d(gridLines);
        LineComparator lineComp(grid);
        ColumnComparator columnComp(grid);

        std::optional<IndexType> columnReflection = findReflectionPoint(grid.xSize(), columnComp);
        if (columnReflection) {
          return integerCast<size_t>(*columnReflection + 1);
        }

        std::optional<IndexType> lineReflection = findReflectionPoint(grid.ySize(), lineComp);
        ASSERT(lineReflection);
        return integerCast<size_t>((grid.ySize() - (*lineReflection + 1)) * 100); // NOLINT
      });
}

template <> size_t part2<2023, 13>(std::string_view const input) {
  LinewiseInput lines(input);
  auto linesPerGrid = lines.splitOnEmptyLines();

  return std::transform_reduce(
      std::execution::par, linesPerGrid.begin(), linesPerGrid.end(), std::size_t(0), std::plus<>(),
      [](auto const &gridLines) {
        Grid2d<char> const grid = makeCharGrid2d(gridLines);
        LineComparator lineComp(grid);
        FuzzyLineComparator fuzzyLineComp(grid);
        ColumnComparator columnComp(grid);
        FuzzyColumnComparator fuzzyColumnComp(grid);

        std::optional<IndexType> columnReflection =
            findReflectionPointFuzzy(grid.xSize(), columnComp, fuzzyColumnComp);
        if (columnReflection) {
          return integerCast<size_t>(*columnReflection + 1);
        }

        std::optional<IndexType> lineReflection =
            findReflectionPointFuzzy(grid.ySize(), lineComp, fuzzyLineComp);
        ASSERT(lineReflection, grid);
        return integerCast<size_t>((grid.ySize() - (*lineReflection + 1)) * 100); // NOLINT
      });
}
