#include "AsciiMap.h"
#include "LinewiseInput.h"
#include "PuzzleImpl.h"

#include <libassert/assert.hpp>

#include <algorithm>
#include <charconv>
#include <cstdint>
#include <span>

namespace {

constexpr size_t numCards = 5u;
using Deck = std::span<char const, numCards>;
using Bid = unsigned short;

static constexpr AsciiMap<unsigned> makeCardValueMapPt1() {
  AsciiMap<unsigned> map('\0');
  map['2'] = 0u;
  map['3'] = 1u;
  map['4'] = 2u;
  map['5'] = 3u;
  map['6'] = 4u;
  map['7'] = 5u;
  map['8'] = 6u;
  map['9'] = 7u;
  map['T'] = 8u;
  map['J'] = 9u;
  map['Q'] = 10u;
  map['K'] = 11u;
  map['A'] = 12u;

  return map;
}

static constexpr AsciiMap<unsigned> makeCardValueMapPt2() {
  AsciiMap<unsigned> map('\0');
  map['J'] = 0u;
  map['2'] = 1u;
  map['3'] = 2u;
  map['4'] = 3u;
  map['5'] = 4u;
  map['6'] = 5u;
  map['7'] = 6u;
  map['8'] = 7u;
  map['9'] = 8u;
  map['T'] = 9u;
  map['Q'] = 10u;
  map['K'] = 11u;
  map['A'] = 12u;

  return map;
}

struct Part1 {

  static constexpr auto cardValueMap = makeCardValueMapPt1();

  static unsigned getValue(Deck const &deck) {
    std::array<unsigned, 13u> frequency{};
    for (char card : deck) {
      ++frequency[cardValueMap[card]];
    }

    std::partial_sort(frequency.begin(), std::next(frequency.begin(), 2), frequency.end(),
                      [](unsigned lhs, unsigned rhs) { return lhs > rhs; });

    unsigned const &mostFrequent = frequency[0];
    unsigned const &secondMostFrequent = frequency[1];

    unsigned value = 0;
    if (mostFrequent == 5u) {
      value = 6;
    } else if (mostFrequent == 4u) {
      value = 5;
    } else if (mostFrequent + secondMostFrequent == 5u) {
      value = 4;
    } else if (mostFrequent == 3u) {
      value = 3;
    } else if (mostFrequent + secondMostFrequent == 4u) {
      value = 2;
    } else if (mostFrequent == 2u) {
      value = 1;
    }

    for (char card : deck) {
      value <<= 5u;
      value += cardValueMap[card];
    }

    return value;
  }
};

struct Part2 {

  static constexpr auto cardValueMap = makeCardValueMapPt2();

  static unsigned getValue(Deck const &deck) {
    std::array<unsigned, 13u> frequency{};
    for (char card : deck) {
      ++frequency[cardValueMap[card]];
    }

    unsigned const numJokers = frequency[cardValueMap['J']];
    frequency[cardValueMap['J']] = 0;

    std::partial_sort(frequency.begin(), std::next(frequency.begin(), 2), frequency.end(),
                      [](unsigned lhs, unsigned rhs) { return lhs > rhs; });

    unsigned const &mostFrequent = frequency[0];
    unsigned const &secondMostFrequent = frequency[1];

    unsigned value = 0;
    if (mostFrequent + numJokers == 5u) {
      value = 6;
    } else if (mostFrequent + numJokers == 4u) {
      value = 5;
    } else if (mostFrequent + secondMostFrequent + numJokers == 5u) {
      value = 4;
    } else if (mostFrequent + numJokers == 3u) {
      value = 3;
    } else if (mostFrequent + secondMostFrequent + numJokers == 4u) {
      value = 2;
    } else if (mostFrequent + numJokers == 2u) {
      value = 1;
    }

    for (char card : deck) {
      value <<= 5u;
      value += cardValueMap[card];
    }

    return value;
  }
};

template <typename Part> struct Game {
  Game(std::string_view const s) : value(Part::getValue(Deck{s})) {
    [[maybe_unused]] std::from_chars_result result =
        std::from_chars(std::next(s.begin(), 6), s.end(), bid);
    DEBUG_ASSERT(result.ec == std::errc());
  }
  unsigned value = 0;
  Bid bid = 0;
};

template <typename Part> size_t run(std::string_view const input) {
  LinewiseInput const lines(input);

  std::vector<Game<Part>> games(lines.begin(), lines.end());

  std::sort(games.begin(), games.end(),
            [](auto const &lhs, auto const &rhs) { return lhs.value < rhs.value; });

  uint64_t rank = 1u;
  uint64_t sum = 0u;
  for (auto const &game : games) {
    sum += rank++ * game.bid;
  }

  return sum;
}

} // namespace

template <> size_t part1<2023, 7>(std::string_view const input) { return run<Part1>(input); }

template <> size_t part2<2023, 7>(std::string_view const input) { return run<Part2>(input); }
