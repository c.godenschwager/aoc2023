#include "IntegerCast.h"
#include "LinewiseInput.h"
#include "PuzzleImpl.h"

#include <algorithm>
#include <array>
#include <cctype>
#include <numeric>
#include <string_view>

namespace {

struct NumberWord {
  std::string_view word;
  unsigned number;
};

std::array<NumberWord, 9u> constexpr numberWords = {{{.word = "one", .number = 1u},
                                                     {.word = "two", .number = 2u},
                                                     {.word = "three", .number = 3u},
                                                     {.word = "four", .number = 4u},
                                                     {.word = "five", .number = 5u},
                                                     {.word = "six", .number = 6u},
                                                     {.word = "seven", .number = 7u},
                                                     {.word = "eight", .number = 8u},
                                                     {.word = "nine", .number = 9u}}};

template <bool useNumberWords> size_t calibrationValue(std::string_view const line) {
  unsigned firstDigit = 0;
  auto firstDigitIt =
      std::find_if(line.begin(), line.end(), [](char const c) { return std::isdigit(c); });
  if (firstDigitIt != line.end())
    firstDigit = *firstDigitIt - '0';

  if constexpr (useNumberWords) {
    for (NumberWord const &nw : numberWords) {
      auto it = std::search(line.begin(), firstDigitIt, nw.word.begin(), nw.word.end());
      if (it != firstDigitIt) {
        firstDigitIt = std::next(it, castToSigned(nw.word.size()));
        firstDigit = nw.number;
      }
    }
  }

  unsigned lastDigit = 0;
  auto lastDigitIt =
      std::find_if(line.rbegin(), line.rend(), [](char const c) { return std::isdigit(c); });

  if (lastDigitIt != line.rend())
    lastDigit = *lastDigitIt - '0';

  if constexpr (useNumberWords) {
    for (NumberWord const &nw : numberWords) {
      auto it = std::search(line.rbegin(), lastDigitIt, nw.word.rbegin(), nw.word.rend());
      if (it != lastDigitIt) {
        lastDigitIt = std::next(it, castToSigned(nw.word.size()));
        lastDigit = nw.number;
      }
    }
  }

  constexpr unsigned base = 10;
  return firstDigit * base + lastDigit;
}
} // namespace

template <> size_t part1<2023, 1>(std::string_view const input) {
  LinewiseInput const lines(input);

  return std::transform_reduce(lines.begin(), lines.end(), size_t(0), std::plus<>(),
                               calibrationValue<false>);
}

template <> size_t part2<2023, 1>(std::string_view const input) {
  LinewiseInput const lines(input);

  return std::transform_reduce(lines.begin(), lines.end(), size_t(0), std::plus<>(),
                               calibrationValue<true>);
}
