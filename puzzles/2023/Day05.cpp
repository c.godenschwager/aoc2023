#include "IntegerCast.h"
#include "LinewiseInput.h"
#include "Parsing.h"
#include "PuzzleImpl.h"

#include <libassert/assert.hpp>
#include <re2/re2.h>

#include <algorithm>
#include <cstdint>
#include <vector>

namespace {

// interval [begin, end)
struct ValueRange {
  uint64_t begin = 0;
  uint64_t end = 0;
};

class Map {
public:
  Map(auto begin, auto end) {
    static RE2 const headerPattern = R"((\w+)-to-(\w+) map:)";
    ASSUME(RE2::FullMatch(*begin, headerPattern, &_srcCategory, &_dstCategory));
    _ranges.assign(std::next(begin), end);
    std::ranges::sort(_ranges, std::ranges::less{}, [](auto const &r) { return r.src.begin; });
  }

  uint64_t operator()(uint64_t const i) const {
    auto it = std::ranges::upper_bound(_ranges, i, std::ranges::less{},
                                       [](auto const &r) { return r.src.end; });
    if (it < _ranges.end() && i >= it->src.begin) {
      return i + it->offset;
    } else {
      return i;
    }
  }

  void operator()(ValueRange const r, auto out) const {
    for (uint64_t i = r.begin; i < r.end;) {
      auto it = std::upper_bound(_ranges.begin(), _ranges.end(), i,
                                 [](uint64_t const x, Range const &r) { return x < r.src.end; });
      if (it == _ranges.end()) {
        out++ = ValueRange(i, r.end);
        break;
      } else if (i >= it->src.begin) {
        if (r.end <= it->src.end) {
          out++ = ValueRange(i + it->offset, r.end + it->offset);
          break;
        } else {
          out++ = ValueRange(i + it->offset, it->src.end + it->offset);
          i = it->src.end;
        }
      } else {
        out++ = ValueRange(i, it->src.begin);
        i = it->src.begin;
      }
    }
  }

private:
  struct Range {
    Range(std::string_view const s) {
      static RE2 const rangePattern = R"((\d+) (\d+) (\d+))";
      uint64_t dstStart = 0;
      uint64_t size = 0;
      ASSUME(RE2::FullMatch(s, rangePattern, &dstStart, &src.begin, &size));
      src.end = src.begin + size;
      offset = integerCast<int64_t>(dstStart) - integerCast<int64_t>(src.begin);
    }

    ValueRange src;
    int64_t offset = 0;
  };

  std::string _srcCategory;
  std::string _dstCategory;
  std::vector<Range> _ranges;
};

std::vector<uint64_t> parseSeeds(std::string_view s) {
  ASSUME(RE2::Consume(&s, "seeds: "));
  s.remove_prefix(s.find(':') + 1u);
  return parseRange<uint64_t>(s);
}

std::vector<ValueRange> parseSeedRanges(std::string_view s) {
  static RE2 const numberPattern = R"((\d+) (\d+) ?)";

  ASSUME(RE2::Consume(&s, "seeds: "));
  std::vector<ValueRange> seeds;
  ValueRange seedRange;
  uint64_t size = 0;
  while (RE2::Consume(&s, numberPattern, &seedRange.begin, &size)) {
    seedRange.end = seedRange.begin + size;
    seeds.push_back(seedRange);
  }
  return seeds;
}

std::vector<Map> parseMaps(auto begin, auto end) {
  std::vector<Map> maps;

  for (auto it = begin; it != end;) {
    if (it->empty()) {
      ++it;
      continue;
    }
    auto rangeBegin = it;
    it = std::find_if(std::next(it), end, [](std::string_view s) { return s.empty(); });
    maps.emplace_back(rangeBegin, it);
  }

  return maps;
}
} // namespace

template <> size_t part1<2023, 5>(std::string_view const input) {
  LinewiseInput const lines(input);

  auto it = lines.begin();
  std::vector<uint64_t> seeds = parseSeeds(*it++);
  std::vector<Map> maps = parseMaps(it, lines.end());

  std::vector<ValueRange> tmp;
  for (auto const &map : maps) {
    std::ranges::transform(seeds, seeds.begin(), map);
  }

  return *std::ranges::min_element(seeds);
}

template <> size_t part2<2023, 5>(std::string_view const input) {
  LinewiseInput const lines(input);

  auto it = lines.begin();
  std::vector<ValueRange> seedRanges = parseSeedRanges(*it++);
  std::vector<Map> maps = parseMaps(it, lines.end());

  std::vector<ValueRange> tmp;
  for (auto const &map : maps) {
    tmp.reserve(seedRanges.size() * 2u);
    for (auto const &seedRange : seedRanges) {
      map(seedRange, std::back_inserter(tmp));
    }
    seedRanges.swap(tmp);
    tmp.clear();
  }

  auto minIt = std::ranges::min_element(seedRanges, std::ranges::less{}, &ValueRange::begin);

  return minIt->begin;
}
