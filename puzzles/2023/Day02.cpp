#include "LinewiseInput.h"
#include "PuzzleImpl.h"

#include <re2/re2.h>

#include <algorithm>
#include <array>
#include <numeric>
#include <string_view>
#include <tuple>

namespace {

std::array<std::string_view, 3u> constexpr colors = {"red", "green", "blue"};

auto parseMaxNumCubes(std::string_view const gameString) {
  static RE2 const gameIdPattern(R"(Game (\d+): )");
  static RE2 const cubePattern(R"((\d+) (\w+),? ?)");
  static RE2 const semicolonPattern("; ");

  std::string_view str(gameString);
  unsigned gameId = 0;
  RE2::Consume(&str, gameIdPattern, &gameId);
  std::array<unsigned, colors.size()> maxNumCubes = {0u, 0u, 0u};
  while (!str.empty()) {
    unsigned numCubes = 0;
    std::string_view color;
    while (RE2::Consume(&str, cubePattern, &numCubes, &color)) {
      auto it = std::ranges::find(colors, color);
      if (it != colors.end()) {
        auto const idx = std::distance(colors.begin(), it);
        maxNumCubes[idx] = std::max(maxNumCubes[idx], numCubes);
      }
    }
    RE2::Consume(&str, semicolonPattern);
  }

  return std::tuple(gameId, maxNumCubes);
}

size_t gamePossible(std::string_view const gameString) {
  static std::array<unsigned, colors.size()> constexpr numCubesInBag = {12u, 13u, 14u};
  auto [gameId, maxNumCubesInGame] = parseMaxNumCubes(gameString);

  if (numCubesInBag[0] >= maxNumCubesInGame[0] && numCubesInBag[1] >= maxNumCubesInGame[1] &&
      numCubesInBag[2] >= maxNumCubesInGame[2])
    return gameId;
  else
    return 0u;
}

size_t cubePower(std::string_view const gameString) {
  auto [gameId, maxNumCubes] = parseMaxNumCubes(gameString);
  return std::reduce(maxNumCubes.begin(), maxNumCubes.end(), size_t(1), std::multiplies<>());
}

} // namespace

template <> size_t part1<2023, 2>(std::string_view const input) {
  LinewiseInput const lines(input);
  return std::transform_reduce(lines.begin(), lines.end(), size_t(0), std::plus<>(), gamePossible);
}

template <> size_t part2<2023, 2>(std::string_view const input) {
  LinewiseInput const lines(input);

  return std::transform_reduce(lines.begin(), lines.end(), size_t(0), std::plus<>(), cubePower);
}
