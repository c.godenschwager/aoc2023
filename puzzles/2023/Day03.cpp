#include "LinewiseInput.h"
#include "PuzzleImpl.h"

#include <libassert/assert.hpp>

#include <algorithm>
#include <array>
#include <charconv>
#include <string_view>

namespace {

unsigned toUnsigned(std::string_view::const_iterator const begin,
                    std::string_view::const_iterator const end) {
  unsigned i = 0u;
  [[maybe_unused]] std::from_chars_result result = std::from_chars(begin, end, i);
  DEBUG_ASSERT(result.ec == std::errc());
  return i;
}

size_t getPartNumbers(std::string_view const line, std::string_view const topNeighborLine,
                      std::string_view const bottomNeighborLine) {

  auto isDigit = [](char const c) { return std::isdigit(c); };
  auto isSymbol = [](char const c) { return c != '.' && !std::isdigit(c); };

  size_t sum = 0u;

  for (auto it = std::ranges::find_if(line, isDigit); it != line.end();
       it = std::find_if(it, line.end(), isDigit)) {
    auto numberStart = it;
    it = std::find_if_not(it, line.end(), isDigit);
    auto numberEnd = it;

    size_t const startPos = std::distance(line.begin(), numberStart);
    size_t leftNeighborPos = startPos;
    if (leftNeighborPos != 0) {
      --leftNeighborPos;
      if (isSymbol(line[leftNeighborPos])) {
        sum += toUnsigned(numberStart, numberEnd);
        continue;
      }
    }

    size_t const endPos = std::distance(line.begin(), numberEnd);
    size_t rightNeighborPos = endPos;
    if (rightNeighborPos != line.size()) {
      ++rightNeighborPos;
      if (isSymbol(line[rightNeighborPos - 1u])) {
        sum += toUnsigned(numberStart, numberEnd);
        continue;
      }
    }

    auto checkLine = [=](std::string_view const l) {
      return !l.empty() &&
             std::any_of(l.begin() + leftNeighborPos, l.begin() + rightNeighborPos, isSymbol);
    };

    if (checkLine(topNeighborLine) || checkLine(bottomNeighborLine)) {
      sum += toUnsigned(numberStart, numberEnd);
    }
  }
  return sum;
}

class Gears {
public:
  bool addGear(size_t const gear) {
    if (_numGears == 2u)
      return false;
    _gears[_numGears++] = gear;
    return true;
  }

  [[nodiscard]] size_t ratio() const { return _gears[0] * _gears[1]; }

private:
  std::array<size_t, 2u> _gears = {0u, 0u};
  size_t _numGears = 0u;
};

size_t getGearRatios(std::string_view const line, std::string_view const topNeighborLine,
                     std::string_view const bottomNeighborLine) {

  auto isDigit = [](char const c) { return std::isdigit(c); };

  size_t sum = 0u;

  for (auto it = std::ranges::find(line, '*'); it != line.end();
       it = std::find(std::next(it), line.end(), '*')) {

    Gears gears;

    auto addGearsLeftAndRight = [&gears, isDigit](std::string_view line,
                                                  std::string_view::const_iterator starIt) {
      auto leftNeighbor = starIt;
      while (leftNeighbor != line.begin() && isDigit(*std::prev(leftNeighbor))) {
        --leftNeighbor;
      }
      if (leftNeighbor != starIt) {
        if (!gears.addGear(toUnsigned(leftNeighbor, starIt)))
          return false;
      }

      auto rightNeighbor = std::next(starIt);
      while (rightNeighbor != line.end() && isDigit(*rightNeighbor)) {
        ++rightNeighbor;
      }
      if (std::prev(rightNeighbor) != starIt) {
        if (!gears.addGear(toUnsigned(std::next(starIt), rightNeighbor)))
          return false;
      }
      return true;
    };

    ASSUME(addGearsLeftAndRight(line, it));

    size_t const starPos = std::distance(line.begin(), it);
    auto addGears = [&gears, starPos, isDigit, addGearsLeftAndRight](std::string_view const l) {
      if (starPos != 0 && starPos + 1u != l.size() && isDigit(l[starPos - 1u]) &&
          !isDigit(l[starPos]) && isDigit(l[starPos + 1])) {
        // special case: two numbers
        auto const starIt = l.begin() + starPos;
        return addGearsLeftAndRight(l, starIt);
      }

      auto findGear = [&](size_t const pos) {
        auto leftIt = l.begin() + pos;
        while (leftIt != l.begin() && isDigit(*std::prev(leftIt))) {
          --leftIt;
        }
        auto rightIt = l.begin() + pos + 1u;
        while (rightIt != l.end() && isDigit(*rightIt)) {
          ++rightIt;
        }
        return toUnsigned(leftIt, rightIt);
      };

      std::optional<unsigned> gear;
      if (isDigit(l[starPos])) {
        gear = findGear(starPos);
      } else if (starPos != 0 && isDigit(l[starPos - 1u])) {
        gear = findGear(starPos - 1u);
      } else if (starPos != l.size() - 1U && isDigit(l[starPos + 1u])) {
        gear = findGear(starPos + 1u);
      }
      if (gear) {
        if (!gears.addGear(*gear))
          return false;
      }

      return true;
    };

    if (!addGears(topNeighborLine))
      continue;

    if (!addGears(bottomNeighborLine))
      continue;

    sum += gears.ratio();
  }
  return sum;
}

} // namespace

template <> size_t part1<2023, 3>(std::string_view const input) {
  LinewiseInput const lines(input);

  size_t sum = 0u;
  for (auto it = lines.begin(); it != lines.end(); ++it) {
    std::string_view top = it == lines.begin() ? std::string_view{} : *std::prev(it);
    std::string_view bottom = std::next(it) == lines.end() ? std::string_view{} : *std::next(it);
    sum += getPartNumbers(*it, top, bottom);
  }

  return sum;
}

template <> size_t part2<2023, 3>(std::string_view const input) {
  LinewiseInput const lines(input);

  size_t sum = 0u;
  for (auto it = lines.begin(); it != lines.end(); ++it) {
    std::string_view top = it == lines.begin() ? std::string_view{} : *std::prev(it);
    std::string_view bottom = std::next(it) == lines.end() ? std::string_view{} : *std::next(it);
    sum += getGearRatios(*it, top, bottom);
  }

  return sum;
}
