#include "Grid2d.h"
#include "LinewiseInput.h"
#include "PuzzleImpl.h"
#include "Vector.h"

#include <libassert/assert.hpp>
#include <ranges>
#include <re2/re2.h>

#include <algorithm>
#include <string_view>
#include <vector>

namespace rng = std::ranges;

namespace {

class Brick {
public:
  Brick(std::string_view const str) {
    static RE2 const pattern = R"((\d+),(\d+),(\d+)~(\d+),(\d+),(\d+))";
    ASSUME(RE2::FullMatch(str, pattern, &_start.x(), &_start.y(), &_start.z(), &_end.x(), &_end.y(),
                          &_end.z()));

    ASSUME(_start.x() <= _end.x());
    ASSUME(_start.y() <= _end.y());
    ASSUME(_start.z() <= _end.z());
  }

  [[nodiscard]] Vec3<int> const &start() const { return _start; }
  [[nodiscard]] Vec3<int> const &end() const { return _end; }

  void dropToLvl(int const lvl) {
    int d = _start.z() - lvl;
    _start.z() -= d;
    _end.z() -= d;
  }

private:
  Vec3<int> _start;
  Vec3<int> _end;
};

void settleBricks(std::vector<Brick> &bricks) {
  Vec3<int> min = bricks.front().start();
  Vec3<int> max = bricks.front().end();
  for (Brick const &b : bricks) {
    min.x() = std::min(min.x(), b.start().x());
    min.y() = std::min(min.y(), b.start().y());
    min.z() = std::min(min.z(), b.start().z());
    max.x() = std::max(max.x(), b.end().x());
    max.y() = std::max(max.y(), b.end().y());
    max.z() = std::max(max.z(), b.end().z());
  }

  ASSUME(min.x() >= 0);
  ASSUME(min.y() >= 0);

  Grid2d<int> heightMap(max.x() + 1, max.y() + 1, 1);

  std::ranges::sort(bricks, std::ranges::less{}, [](Brick const &b) { return b.start().z(); });

  for (Brick &b : bricks) {
    int maxHeight = 0;
    for (int y = b.start().y(); y <= b.end().y(); ++y)
      for (int x = b.start().x(); x <= b.end().x(); ++x)
        maxHeight = std::max(heightMap[x, y], maxHeight);
    b.dropToLvl(maxHeight + 1);
    for (int y = b.start().y(); y <= b.end().y(); ++y)
      for (int x = b.start().x(); x <= b.end().x(); ++x)
        heightMap[x, y] = b.end().z();
  }
}

class SupportInfo {
public:
  SupportInfo(std::vector<Brick> const &bricks)
      : _supports(bricks.size()), _isSupportedBy(bricks.size()) {
    for (size_t i = 0; i < bricks.size(); ++i)
      for (size_t j = 0; j < bricks.size(); ++j) {
        if (bricks[i].end().z() + 1 != bricks[j].start().z())
          continue;
        if (bricks[i].end().x() < bricks[j].start().x() ||
            bricks[i].start().x() > bricks[j].end().x())
          continue;
        if (bricks[i].end().y() < bricks[j].start().y() ||
            bricks[i].start().y() > bricks[j].end().y())
          continue;
        _supports[i].insert(j);
        _isSupportedBy[j].insert(i);
      }
  }

  bool safeToDisentegrate(size_t const idx) {
    return rng::all_of(_supports[idx],
                       [this](size_t const i) { return _isSupportedBy[i].size() > 1u; });
  }

  size_t numFallingBricks(size_t const brickIdx) {
    std::set<size_t> fallingBricks;
    fallingBricks.insert(brickIdx);
    for (size_t const i : _supports[brickIdx])
      addFallingBricks(i, fallingBricks);

    return fallingBricks.size() - 1u;
  }

private:
  void addFallingBricks(size_t brickIdx, std::set<size_t> &fallingBricks) {
    if (fallingBricks.contains(brickIdx) || !rng::includes(fallingBricks, _isSupportedBy[brickIdx]))
      return;
    fallingBricks.insert(brickIdx);
    for (size_t const i : _supports[brickIdx])
      addFallingBricks(i, fallingBricks);
  }

  std::vector<std::set<size_t>> _supports;
  std::vector<std::set<size_t>> _isSupportedBy;
};

} // namespace

template <> size_t part1<2023, 22>(std::string_view const input) {
  LinewiseInput lines(input);
  std::vector<Brick> bricks(lines.begin(), lines.end());
  settleBricks(bricks);
  SupportInfo si(bricks);

  return rng::count_if(rng::views::iota(0u, bricks.size()),
                       [&](size_t const i) { return si.safeToDisentegrate(i); });
}

template <> size_t part2<2023, 22>(std::string_view const input) {
  LinewiseInput lines(input);
  std::vector<Brick> bricks(lines.begin(), lines.end());
  settleBricks(bricks);
  SupportInfo si(bricks);

  return rng::fold_left(
      rng::views::iota(0u, bricks.size()) |
          rng::views::transform([&](size_t const i) { return si.numFallingBricks(i); }),
      0u, std::plus<>());
}
