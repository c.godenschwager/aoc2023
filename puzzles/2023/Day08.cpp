#include "LinewiseInput.h"
#include "PuzzleImpl.h"

#include <libassert/assert.hpp>
#include <limits>
#include <re2/re2.h>

#include <algorithm>
#include <charconv>
#include <cstdint>
#include <numeric>
#include <string_view>
#include <tuple>

namespace {

struct Node {
  unsigned l = std::numeric_limits<unsigned>::max();
  unsigned r = std::numeric_limits<unsigned>::max();
};

constexpr unsigned parseLocation(std::string_view const location) {
  DEBUG_ASSERT(location.size() == 3u);
  return ((location[0] - 'A') << 10u) + ((location[1] - 'A') << 5u) + (location[2] - 'A');
}

bool isStartingLocation(unsigned loc) {
  constexpr unsigned mask = (1u << 5u) - 1u;
  return (loc & mask) == 0;
}

bool isGoalLocation(unsigned loc) {
  constexpr unsigned mask = (1u << 5u) - 1u;
  return (loc & mask) == 'Z' - 'A';
}

[[maybe_unused]] std::string printLocation(unsigned loc) {
  std::string result;
  result.resize(3u);

  constexpr unsigned mask = (1u << 5u) - 1u;
  result[0] = static_cast<char>('A' + (loc >> 10u));
  result[1] = static_cast<char>('A' + ((loc >> 5u) & mask));
  result[2] = static_cast<char>('A' + (loc & mask));
  return result;
}

std::tuple<std::vector<Node>, std::vector<unsigned>> parseNodes(auto begin, auto end) {
  static RE2 const nodePattern(R"(([A-Z]{3}) = \(([A-Z]{3}), ([A-Z]{3})\))");

  std::vector<Node> map(1u << 15);
  std::vector<unsigned> startingLocations;
  std::string_view cur, l, r;
  while (begin != end) {
    ASSUME(RE2::FullMatch(*begin++, nodePattern, &cur, &l, &r));
    unsigned loc = parseLocation(cur);
    DEBUG_ASSERT(loc < map.size());
    map[loc] = Node{.l = parseLocation(l), .r = parseLocation(r)};
    if (isStartingLocation(loc))
      startingLocations.push_back(loc);
  }

  return {map, startingLocations};
}

} // namespace

template <> size_t part1<2023, 8>(std::string_view const input) {
  LinewiseInput const lines(input);
  std::string_view const path = lines[0];
  auto [nodes, locations] = parseNodes(std::next(lines.begin(), 2), lines.end());

  unsigned constexpr goal = parseLocation("ZZZ");
  unsigned location = parseLocation("AAA");

  uint64_t ctr = 0;
  while (location != goal) {
    for (auto it = path.begin(); it != path.end() && !isGoalLocation(location); ++it) {
      location = *it == 'L' ? nodes[location].l : nodes[location].r;
      ++ctr;
    }
  }
  return ctr;
}

template <> size_t part2<2023, 8>(std::string_view const input) {
  LinewiseInput const lines(input);

  std::string_view const path = lines[0];
  auto [nodes, locations] = parseNodes(std::next(lines.begin(), 2), lines.end());

  std::vector<uint64_t> counters(locations.size());

  for (size_t i = 0; i < locations.size(); ++i) {
    unsigned location = locations[i];
    uint64_t &ctr = counters[i];
    while (!isGoalLocation(location))
      for (auto it = path.begin(); it != path.end() && !isGoalLocation(location); ++it) {
        location = *it == 'L' ? nodes[location].l : nodes[location].r;
        ++ctr;
      }
  }

  auto countSteps = [&nodes, &path](unsigned location) {
    uint64_t ctr = 0u;
    while (!isGoalLocation(location))
      for (auto it = path.begin(); it != path.end() && !isGoalLocation(location); ++it) {
        location = *it == 'L' ? nodes[location].l : nodes[location].r;
        ++ctr;
      }
    return ctr;
  };

  return std::transform_reduce(
      locations.begin(), locations.end(), size_t(1),
      [](uint64_t lhs, uint64_t rhs) { return std::lcm(lhs, rhs); }, countSteps);
}
