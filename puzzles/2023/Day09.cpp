#include "IntegerCast.h"
#include "LinewiseInput.h"
#include "Parsing.h"
#include "PuzzleImpl.h"

#include <libassert/assert.hpp>

#include <cstdint>
#include <numeric>
#include <string_view>

namespace {

int64_t extrapolateMeasurementBack(std::string_view const measurement) {
  std::vector<int64_t> derivativesBuffer = parseRange<int64_t>(measurement);
  std::span<int64_t> derivatives(derivativesBuffer);

  int64_t result = derivatives.back();
  bool allZero = false;
  while (!allZero) {
    DEBUG_ASSERT(derivatives.size() > 1U);
    allZero = true;
    for (size_t i = 1; i < derivatives.size(); ++i) {
      derivatives[i - 1] = derivatives[i] - derivatives[i - 1];
      allZero = allZero && derivatives[i - 1] == 0;
    }
    derivatives = std::span(derivatives.data(), derivatives.size() - 1u);
    result += derivatives.back();
  }
  return result;
}

int64_t extrapolateMeasurementFront(std::string_view const measurement) {
  std::vector<int64_t> derivativesBuffer = parseRange<int64_t>(measurement);
  std::span<int64_t> derivatives(derivativesBuffer);

  int64_t result = derivatives.front();
  bool allZero = false;
  int64_t sign = -1;
  while (!allZero) {
    DEBUG_ASSERT(derivatives.size() > 1U);
    allZero = true;
    for (size_t i = 1; i < derivatives.size(); ++i) {
      derivatives[i - 1] = derivatives[i] - derivatives[i - 1];
      allZero = allZero && derivatives[i - 1] == 0;
    }
    derivatives = std::span(derivatives.data(), derivatives.size() - 1u);
    result += sign * derivatives.front();
    sign *= -1;
  }
  return result;
}

} // namespace

template <> size_t part1<2023, 9>(std::string_view const input) {
  LinewiseInput const lines(input);

  return integerCast<size_t>(std::transform_reduce(lines.begin(), lines.end(), int64_t(0),
                                                   std::plus<>(), extrapolateMeasurementBack));
}

template <> size_t part2<2023, 9>(std::string_view const input) {
  LinewiseInput const lines(input);

  return integerCast<size_t>(std::transform_reduce(lines.begin(), lines.end(), int64_t(0),
                                                   std::plus<>(), extrapolateMeasurementFront));
}
