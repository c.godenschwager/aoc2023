#include "Grid2d.h"
#include "LinewiseInput.h"
#include "PuzzleImpl.h"

#include <libassert/assert.hpp>

#include <limits>
#include <queue>

namespace {

using Coord = Grid2dSpan<uint8_t const>::Coord;

enum Dir : uint8_t { N = 0, E = 1, S = 2, W = 3 };
constexpr std::array<Dir, 4u> directions = {N, E, S, W};
constexpr std::array<Coord, 4u> stencil = {{{0, 1}, {1, 0}, {0, -1}, {-1, 0}}};
constexpr std::array<Dir, 4u> leftTurn = {W, N, E, S};
constexpr std::array<Dir, 4u> rightTurn = {E, S, W, N};

using NodePriority = std::tuple<unsigned, Dir, Coord>;
bool operator<(NodePriority const &lhs, NodePriority const &rhs) {
  return std::get<0>(lhs) > std::get<0>(rhs);
};

unsigned costHeuristic(Coord start, Coord goal) {
  return std::abs(start.x() - goal.x()) + std::abs(start.y() - goal.y());
}

unsigned findShortestPath(Grid2dSpan<uint8_t const> heatLoss, Coord const &startPos,
                          Coord const &goalPos, int const minNumStepsSameDir,
                          int const maxNumStepsSameDir) {

  std::priority_queue<NodePriority> openSet;
  std::array<Grid2d<unsigned>, directions.size()> cost;
  for (auto &c : cost)
    c = Grid2d<unsigned>{heatLoss.xSize(), heatLoss.ySize(), std::numeric_limits<unsigned>::max()};

  for (Dir d : directions) {
    openSet.emplace(0, d, startPos);
    cost[d][startPos] = 0u;
  }

  while (!openSet.empty()) {
    auto [costEstimate, curDir, curPos] = openSet.top();
    openSet.pop();

    unsigned const curCost = cost[curDir][curPos];

    if (curPos == goalPos)
      return curCost;

    unsigned neighborCost = curCost;
    Coord neighborPos = curPos;
    for (int steps = 1; steps <= maxNumStepsSameDir; ++steps) {
      neighborPos += stencil[curDir];
      if (neighborPos.x() < 0 || neighborPos.y() < 0 || neighborPos.x() >= heatLoss.xSize() ||
          neighborPos.y() >= heatLoss.ySize())
        break;

      neighborCost += heatLoss[neighborPos];

      if (steps < minNumStepsSameDir)
        continue;

      if (neighborCost < cost[leftTurn[curDir]][neighborPos]) {
        cost[leftTurn[curDir]][neighborPos] = neighborCost;
        openSet.emplace(neighborCost + costHeuristic(neighborPos, goalPos), leftTurn[curDir],
                        neighborPos);
      }

      if (neighborCost < cost[rightTurn[curDir]][neighborPos]) {
        cost[rightTurn[curDir]][neighborPos] = neighborCost;
        openSet.emplace(neighborCost + costHeuristic(neighborPos, goalPos), rightTurn[curDir],
                        neighborPos);
      }
    }
  }
  PANIC();
}

} // namespace

template <> size_t part1<2023, 17>(std::string_view const input) {
  LinewiseInput lines(input);
  Grid2d<char> grid = lines.makeCharGrid2d();
  Grid2d<uint8_t> heatLoss = grid.map([](char const c) { return static_cast<uint8_t>(c - '0'); });

  return findShortestPath(heatLoss, {0, heatLoss.ySize() - 1}, {heatLoss.xSize() - 1, 0}, 1, 3);
}

template <> size_t part2<2023, 17>(std::string_view const input) {
  LinewiseInput lines(input);
  Grid2d<char> grid = lines.makeCharGrid2d();
  Grid2d<uint8_t> heatLoss = grid.map([](char const c) { return static_cast<uint8_t>(c - '0'); });

  return findShortestPath(heatLoss, {0, heatLoss.ySize() - 1}, {heatLoss.xSize() - 1, 0}, 4, 10);
}
