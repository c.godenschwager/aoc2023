#include "Grid2d.h"
#include "IntegerCast.h"
#include "LinewiseInput.h"
#include "PuzzleImpl.h"

#include <libassert/assert.hpp>

#include <numeric>
#include <string_view>

namespace {

using Grid = Grid2dSpan<char>;
using Coord = Grid::Coord;

constexpr char ROUND_ROCK = 'O';
constexpr char CUBE_ROCK = '#';
constexpr char EMPTY_SPACE = '.';

void tiltNorth(Grid const &g) {
  for (Coord x{0, g.ySize() - 1}; x.x() < g.xSize(); ++x.x()) {
    for (x.y() = g.ySize() - 1; x.y() >= 0; --x.y()) {
      if (g[x] != EMPTY_SPACE)
        continue;
      Coord xx = x;
      while (g[xx] == EMPTY_SPACE)
        --xx.y();
      if (g[xx] == ROUND_ROCK) {
        g[x] = ROUND_ROCK;
        g[xx] = EMPTY_SPACE;
      } else if (g[xx] == CUBE_ROCK) {
        x.y() = xx.y();
      }
    }
  }
}

void tiltSouth(Grid const &g) {
  for (Coord x{0, 0}; x.x() < g.xSize(); ++x.x()) {
    for (x.y() = 0; x.y() < g.ySize(); ++x.y()) {
      if (g[x] != EMPTY_SPACE)
        continue;
      Coord xx = x;
      while (g[xx] == EMPTY_SPACE)
        ++xx.y();
      if (g[xx] == ROUND_ROCK) {
        g[x] = ROUND_ROCK;
        g[xx] = EMPTY_SPACE;
      } else if (g[xx] == CUBE_ROCK) {
        x.y() = xx.y();
      }
    }
  }
}

void tiltEast(Grid const &g) {
  for (Coord x{g.xSize() - 1, 0}; x.y() < g.ySize(); ++x.y()) {
    for (x.x() = g.xSize() - 1; x.x() >= 0; --x.x()) {
      if (g[x] != EMPTY_SPACE)
        continue;
      Coord xx = x;
      while (g[xx] == EMPTY_SPACE)
        --xx.x();
      if (g[xx] == ROUND_ROCK) {
        g[x] = ROUND_ROCK;
        g[xx] = EMPTY_SPACE;
      } else if (g[xx] == CUBE_ROCK) {
        x.x() = xx.x();
      }
    }
  }
}

void tiltWest(Grid const &g) {
  for (Coord x{0, 0}; x.y() < g.ySize(); ++x.y()) {
    for (x.x() = 0; x.x() < g.xSize(); ++x.x()) {
      if (g[x] != EMPTY_SPACE)
        continue;
      Coord xx = x;
      while (g[xx] == EMPTY_SPACE)
        ++xx.x();
      if (g[xx] == ROUND_ROCK) {
        g[x] = ROUND_ROCK;
        g[xx] = EMPTY_SPACE;
      } else if (g[xx] == CUBE_ROCK) {
        x.x() = xx.x();
      }
    }
  }
}

void cycle(Grid const &g) {
  tiltNorth(g);
  tiltWest(g);
  tiltSouth(g);
  tiltEast(g);
}

size_t calculateTotalLoad(Grid const &g) {
  std::vector<Coord> roundRocks = g.findAll(ROUND_ROCK);

  return std::transform_reduce(roundRocks.begin(), roundRocks.end(), size_t(0), std::plus<>(),
                               [](Coord const c) { return integerCast<size_t>(c.y() + 1); });
}

} // namespace

template <> size_t part1<2023, 14>(std::string_view const input) {
  LinewiseInput lines(input);
  Grid2d<char> grid = lines.makeCharGrid2dWithGhostLayers(1, CUBE_ROCK);

  tiltNorth(grid);

  return calculateTotalLoad(grid);
}

template <> size_t part2<2023, 14>(std::string_view const input) {
  LinewiseInput lines(input);
  Grid2d<char> grid = lines.makeCharGrid2dWithGhostLayers(1, CUBE_ROCK);

  std::map<Grid2d<char>, size_t> previousCycles;

  constexpr size_t numCycles = 1000000000;

  for (size_t i = 0; i < numCycles; ++i) {
    if (auto it = previousCycles.find(grid); it != previousCycles.end()) {
      size_t const firstAppearance = it->second;
      size_t const cycleLength = i - firstAppearance;
      size_t const remainingCyclesToRun = (numCycles - firstAppearance) % cycleLength;
      for (size_t j = 0; j < remainingCyclesToRun; ++j) {
        cycle(grid);
      }
      break;
    } else {
      previousCycles.try_emplace(grid, i);
      cycle(grid);
    }
  }
  return calculateTotalLoad(grid);
}
