#include "Grid2d.h"
#include "LinewiseInput.h"
#include "PuzzleImpl.h"
#include "Vector.h"

#include <absl/container/flat_hash_map.h>
#include <absl/container/flat_hash_set.h>
#include <libassert/assert.hpp>

#include <algorithm>
#include <cstdint>
#include <iterator>
#include <limits>
#include <queue>
#include <stack>
#include <string_view>
#include <utility>
#include <vector>

namespace {
using Coord = Grid2d<char>::Coord;
enum Dir : uint8_t { N = 0, E = 1, S = 2, W = 3 };
constexpr std::array<Dir, 4u> directions = {N, E, S, W};
constexpr std::array<Dir, 4u> op = {S, W, N, E};
constexpr std::array<Coord, 4u> stencil = {{{0, 1}, {1, 0}, {0, -1}, {-1, 0}}};

class DAG {
public:
  using Vertex = Coord;

  DAG(Grid2dSpan<char const> const map) {

    std::queue<std::tuple<DAG::Vertex, Dir>> queue;

    absl::flat_hash_set<Vertex> visited;

    _start = Coord{0, map.ySize() - 1};
    while (map[_start] != '.' && _start.x() < map.xSize())
      ++_start.x();
    DEBUG_ASSERT(_start.x() < map.xSize());
    visited.insert(_start);
    queue.emplace(_start, S);

    while (!queue.empty()) {
      auto [vertex, dir] = queue.front();
      queue.pop();

      Coord coord = vertex;
      int edgeWeight = 0;
      bool vertexFound = false;
      Dir newDir = N;
      while (!vertexFound) {
        coord += stencil[dir];
        ++edgeWeight;
        DEBUG_ASSERT(map[coord] != '#', coord, dir);

        if (coord.y() == 0) {
          DEBUG_ASSERT(map[coord] == '.');
          _end = coord;
          visited.insert(coord);
          _edges[vertex].push_back(coord);
          _edgeWeights.emplace(std::pair{vertex, coord}, edgeWeight);
          vertexFound = true;
          continue;
        }

        int pathCtr = 0;
        switch (map[coord]) {
        case '^':
          if (dir == S)
            vertexFound = true;
          dir = N;
          break;
        case '>':
          if (dir == W)
            vertexFound = true;
          dir = E;
          break;
        case 'v':
          if (dir == N)
            vertexFound = true;
          dir = S;
          break;
        case '<':
          if (dir == E)
            vertexFound = true;
          dir = W;
          break;
        case '.':
          pathCtr = 0;
          for (auto d : directions) {
            if (d == op[dir])
              continue;
            if (map[coord + stencil[d]] != '#') {
              ++pathCtr;
              newDir = d;
            }
          }
          DEBUG_ASSERT(pathCtr > 0, coord);
          if (pathCtr == 1) {
            dir = newDir;
          } else {
            vertexFound = true;
            DEBUG_ASSERT(std::ranges::find(_edges[vertex], coord) == _edges[vertex].end());
            _edges[vertex].push_back(coord);
            _edgeWeights.emplace(std::pair{vertex, coord}, edgeWeight);
            if (!visited.insert(coord).second)
              continue;
            for (auto d : directions) {
              if (d == op[dir])
                continue;
              if (map[coord + stencil[d]] != '#') {
                queue.emplace(coord, d);
              }
            }
          }
          break;
        case '#':
        default:
          UNREACHABLE(map[coord], coord);
        }
      }
    }

    absl::flat_hash_map<Vertex, absl::flat_hash_set<Vertex>> inEdges;
    for (auto const &[v, us] : _edges)
      for (auto const &u : us)
        inEdges[u].insert(v);

    absl::flat_hash_set<Vertex> vertices;
    vertices.insert(_start);
    while (!vertices.empty()) {
      Vertex v = *vertices.begin();
      vertices.erase(v);
      _vertices.push_back(v);
      for (auto &u : _edges[v]) {
        inEdges[u].erase(v);
        if (inEdges[u].empty())
          vertices.insert(u);
      }
    }
    ASSERT(std::ranges::all_of(inEdges,
                               [](auto const &us) { return us.second.empty(); })); // Check if DAG
  }

  [[nodiscard]] int longestPath() const {
    absl::flat_hash_map<Vertex, int> distances;
    absl::flat_hash_map<Vertex, Vertex> pred;
    for (auto const &v : _vertices)
      distances[v] = std::numeric_limits<int>::max();
    distances[_start] = 0;
    for (auto const &u : _vertices) {
      auto vIt = _edges.find(u);
      if (vIt == _edges.end())
        continue;
      for (auto const &v : vIt->second) {
        auto eIt = _edgeWeights.find(std::pair{u, v});
        DEBUG_ASSERT(eIt != _edgeWeights.end());
        if (distances[v] > distances[u] - eIt->second) {
          distances[v] = distances[u] - eIt->second;
          pred[v] = u;
        }
      }
    }

    return -distances[_end];
  }

private:
  std::vector<Vertex> _vertices;
  Vertex _start;
  Vertex _end;
  absl::flat_hash_map<Vertex, std::vector<Vertex>> _edges;
  absl::flat_hash_map<std::pair<Vertex, Vertex>, int> _edgeWeights;
};

class Graph {
public:
  Graph(Grid2dSpan<char const> const map) {

    std::queue<std::tuple<DAG::Vertex, Dir>> queue;

    absl::flat_hash_map<Coord, size_t> foundVertices;

    auto addVertex = [&](Coord const &c) {
      DEBUG_ASSERT(_outEdges.size() == _numVertices);
      DEBUG_ASSERT(_edgeWeights.size() == _numVertices);
      foundVertices[c] = _numVertices;
      _outEdges.emplace_back();
      _edgeWeights.emplace_back();
      return _numVertices++;
    };

    auto addEdge = [&](size_t const v1, size_t v2, int const edgeWeight) {
      DEBUG_ASSERT(v1 < _edgeWeights.size());
      DEBUG_ASSERT(v2 < _edgeWeights.size());
      DEBUG_ASSERT(_edgeWeights.size() == _outEdges.size());

      auto it = std::ranges::find(_outEdges[v1], v2);
      if (it == _outEdges[v1].end()) {
        DEBUG_ASSERT(!std::ranges::contains(_outEdges[v2], v1));
        _outEdges[v1].push_back(v2);
        _outEdges[v2].push_back(v1);
        _edgeWeights[v1].push_back(edgeWeight);
        _edgeWeights[v2].push_back(edgeWeight);
      } else {
        auto idx = std::distance(_outEdges[v1].begin(), it);
        if (_edgeWeights[v1][idx] >= edgeWeight)
          return;
        _edgeWeights[v1][idx] = edgeWeight;
        it = std::ranges::find(_outEdges[v2], v1);
        DEBUG_ASSERT(it != _outEdges[v2].end());
        idx = std::distance(_outEdges[v2].begin(), it);
        DEBUG_ASSERT(_edgeWeights[v2][idx] < edgeWeight);
        _edgeWeights[v2][idx] = edgeWeight;
      }
    };

    Coord start{0, map.ySize() - 1};
    while (map[start] != '.' && start.x() < map.xSize())
      ++start.x();
    DEBUG_ASSERT(start.x() < map.xSize());
    _start = addVertex(start);
    queue.emplace(start, S);

    while (!queue.empty()) {
      auto [vertexCoord, dir] = queue.front();
      size_t vertex = foundVertices[vertexCoord];
      queue.pop();

      Coord coord = vertexCoord;
      int edgeWeight = 0;
      bool vertexFound = false;
      Dir newDir = N;
      while (!vertexFound) {
        coord += stencil[dir];
        ++edgeWeight;
        DEBUG_ASSERT(map[coord] != '#', coord, dir);

        if (coord.y() == 0) {
          DEBUG_ASSERT(map[coord] == '.');
          _end = addVertex(coord);
          addEdge(vertex, _end, edgeWeight);
          break;
        }

        int pathCtr = 0;
        if (map[coord] != '#') {
          pathCtr = 0;
          for (auto d : directions) {
            if (d == op[dir])
              continue;
            if (map[coord + stencil[d]] != '#') {
              ++pathCtr;
              newDir = d;
            }
          }
          DEBUG_ASSERT(pathCtr > 0, coord);
          if (pathCtr == 1) {
            dir = newDir;
          } else {
            auto it = foundVertices.find(coord);
            bool newVertex = it == foundVertices.end();
            size_t foundVertex = newVertex ? addVertex(coord) : it->second;
            addEdge(vertex, foundVertex, edgeWeight);
            if (newVertex)
              for (auto d : directions) {
                if (d == op[dir])
                  continue;
                if (map[coord + stencil[d]] != '#') {
                  queue.emplace(coord, d);
                }
              }
            break;
          }
        }
      }
    }
  }

  int longestPath() {
    ASSERT(_numVertices <= 64);
    int maxWeight = 0;
    longestPathImpl(_start, 0, 0, maxWeight);
    return maxWeight;
  }

private:
  void longestPathImpl(size_t const node, uint64_t const visited, int const weight,
                       int &maxWeight) {
    uint64_t const nodeMask = uint64_t(1) << node;

    if ((visited & nodeMask) != 0u)
      return;

    if (node == _end) {
      maxWeight = std::max(weight, maxWeight);
      return;
    }

    for (size_t i = 0; i < _outEdges[node].size(); ++i)
      longestPathImpl(_outEdges[node][i], visited | nodeMask, weight + _edgeWeights[node][i],
                      maxWeight);
  }

  size_t _numVertices = 0u;
  std::vector<std::vector<size_t>> _outEdges;
  std::vector<std::vector<int>> _edgeWeights;
  size_t _start;
  size_t _end;
};

} // namespace

template <> size_t part1<2023, 23>(std::string_view const input) {
  LinewiseInput lines(input);
  Grid2d<char> map = lines.makeCharGrid2dWithGhostLayers(1, '#');
  DAG dag(map);
  return dag.longestPath();
}

template <> size_t part2<2023, 23>(std::string_view const input) {
  LinewiseInput lines(input);
  Grid2d<char> map = lines.makeCharGrid2dWithGhostLayers(1, '#');

  Graph g(map);

  return g.longestPath();
}
