#include "LinewiseInput.h"
#include "PuzzleImpl.h"

#include <libassert/assert.hpp>
#include <re2/re2.h>

#include <algorithm>
#include <cstdlib>
#include <map>
#include <memory>
#include <numeric>
#include <queue>
#include <span>
#include <string_view>
#include <tuple>

namespace {

constexpr bool HI = true;
constexpr bool LO = false;

constexpr bool ON = true;
constexpr bool OFF = false;

class Module;

using Call = std::tuple<Module *, Module *, bool>;
using CallQueue = std::queue<Call>;

class Module {
public:
  Module(std::string_view const name, std::shared_ptr<CallQueue> const &calls)
      : _name(name), _calls(calls) {}

  virtual ~Module() = default;

  virtual void receiveSignal(Module const *const sender, bool signal) = 0;

  virtual void registerInput(Module const &sender) = 0;
  void registerOutput(Module &receiver) { _outputs.push_back(&receiver); };

  static void connect(Module *const sender, Module *const receiver) {
    sender->registerOutput(*receiver);
    receiver->registerInput(*sender);
  }

  [[nodiscard]] std::string_view name() const { return _name; }

protected:
  void send(bool const signal) {
    for (Module *m : _outputs)
      _calls->emplace(this, m, signal);
  }

private:
  std::string_view _name;
  std::shared_ptr<CallQueue> _calls;
  std::vector<Module *> _outputs;
};

class FlipFlopModule final : public Module {
public:
  using Module::Module;

  void registerInput(Module const &) final {}

  void receiveSignal(Module const *const, bool signal) final {
    if (signal == LO) {
      _state = !_state;
      send(_state == ON ? HI : LO);
    }
  }

private:
  bool _state = OFF;
};

class ConjunctionModule final : public Module {
public:
  using Module::Module;

  void registerInput(Module const &sender) final { _inputStates.emplace(sender.name(), LO); }

  void receiveSignal(Module const *const sender, bool signal) final {
    auto it = _inputStates.find(sender->name());
    ASSUME(it != _inputStates.end());
    it->second = signal;

    if (signal == HI && _hiCallback)
      _hiCallback(sender->name());

    if (std::ranges::all_of(_inputStates, [](std::pair<std::string_view, bool> const &p) {
          return p.second == HI;
        }))
      send(LO);
    else
      send(HI);
  }

  void registerHiCallback(std::function<void(std::string_view)> const &f) { _hiCallback = f; }

  [[nodiscard]] size_t numInputs() const { return _inputStates.size(); }

private:
  std::map<std::string_view, bool> _inputStates;
  std::function<void(std::string_view)> _hiCallback;
};

class OutputModule final : public Module {
public:
  using Module::Module;
  void registerInput(Module const &) final {}
  void receiveSignal(Module const *const, bool) final {}
};

class BroadcastModule final : public Module {
public:
  using Module::Module;

  void registerInput(Module const &) final { UNREACHABLE(); }

  void receiveSignal(Module const *const, bool signal) final { send(signal); }
};

class Network {
public:
  Network(std::span<std::string_view const> const flipFlopModules,
          std::span<std::string_view const> const conjunctionModules,
          std::span<std::pair<std::string_view, std::string_view> const> const connections)
      : _calls(std::make_shared<CallQueue>()) {

    auto [it, inserted] =
        _modules.emplace("broadcaster", std::make_unique<BroadcastModule>("broadcaster", _calls));
    ASSUME(inserted);
    _broadcaster = dynamic_cast<BroadcastModule *>(it->second.get());
    ASSUME(_broadcaster);

    for (auto const m : flipFlopModules)
      _modules.emplace(m, std::make_unique<FlipFlopModule>(m, _calls));

    for (auto const m : conjunctionModules)
      _modules.emplace(m, std::make_unique<ConjunctionModule>(m, _calls));

    for (auto const &c : connections) {
      auto senderIt = _modules.find(c.first);
      ASSUME(senderIt != _modules.end());
      auto receiverIt = _modules.find(c.second);
      if (receiverIt == _modules.end()) {
        receiverIt =
            _modules.emplace(c.second, std::make_unique<OutputModule>(c.second, _calls)).first;
      }
      Module::connect(senderIt->second.get(), receiverIt->second.get());
    }
  }

  void pushButton() {
    ++_loCtr;
    _broadcaster->receiveSignal(nullptr, LO);
    while (!_calls->empty()) {
      auto [sender, receiver, signal] = _calls->front();
      if (signal == HI)
        ++_hiCtr;
      else
        ++_loCtr;
      receiver->receiveSignal(sender, signal);
      _calls->pop();
    }
  }

  [[nodiscard]] auto ctrs() const { return std::make_tuple(_hiCtr, _loCtr); }

  Module *findModule(std::string_view const name) {
    auto it = _modules.find(name);
    if (it == _modules.end())
      return nullptr;
    return it->second.get();
  }

private:
  std::shared_ptr<CallQueue> _calls;
  std::map<std::string_view, std::unique_ptr<Module>> _modules;
  BroadcastModule *_broadcaster;
  size_t _hiCtr = 0;
  size_t _loCtr = 0;
};

Network parse(std::string_view const input) {
  static const RE2 frontPattern("([%&]?)([a-z]+) -> ");
  static const RE2 listPattern(" ?([a-z]+),?");
  LinewiseInput lines(input);
  std::vector<std::string_view> conjunctionModules;
  std::vector<std::string_view> flipFlopModules;
  std::vector<std::pair<std::string_view, std::string_view>> connections;

  for (auto line : lines) {
    std::string_view c;
    std::string_view name;
    ASSUME(RE2::Consume(&line, frontPattern, &c, &name));
    std::string_view outputName;
    std::vector<std::string_view> outputs;
    while (RE2::Consume(&line, listPattern, &outputName))
      outputs.push_back(outputName);

    if (c == "%") {
      flipFlopModules.push_back(name);
    } else if (c == "&") {
      conjunctionModules.push_back(name);
    }
    for (auto const &output : outputs)
      connections.emplace_back(name, output);
  }

  return {flipFlopModules, conjunctionModules, connections};
}

} // namespace

template <> size_t part1<2023, 20>(std::string_view const input) {
  Network network = parse(input);

  for (int i = 0; i < 1000; ++i)
    network.pushButton();

  auto [hiCtr, loCtr] = network.ctrs();

  return hiCtr * loCtr;
}

template <> size_t part2<2023, 20>(std::string_view const input) {
  Network network = parse(input);

  auto *const hj = dynamic_cast<ConjunctionModule *>(network.findModule("hj"));
  ASSUME(hj);
  std::map<std::string_view, size_t> cycleLengths;
  size_t loopCtr = 1;
  hj->registerHiCallback([&cycleLengths, &loopCtr](std::string_view name) {
    auto it = cycleLengths.find(name);
    if (it == cycleLengths.end())
      cycleLengths.emplace(name, loopCtr);
    else
      ASSUME(loopCtr % it->second == 0);
  });

  size_t numInputs = hj->numInputs();

  while (cycleLengths.size() != numInputs) {
    network.pushButton();
    ++loopCtr;
  }

  return std::transform_reduce(
      cycleLengths.begin(), cycleLengths.end(), size_t(1),
      [](size_t lhs, size_t rhs) { return std::lcm(lhs, rhs); },
      [](auto const &p) { return p.second; });
}
