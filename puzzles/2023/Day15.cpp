#include "Parsing.h"
#include "PuzzleImpl.h"

#include <libassert/assert.hpp>
#include <re2/re2.h>

#include <algorithm>
#include <iostream>
#include <numeric>
#include <string_view>

namespace {

size_t hash(size_t h, char const c) {
  h += static_cast<size_t>(c);
  h *= size_t(17);
  h %= size_t(256);

  return h;
}

size_t hash(std::string_view const s) {
  size_t h = 0;
  for (char const c : s)
    h = hash(h, c);
  return h;
}

class HashMap {
  using Bucket = std::vector<std::pair<std::string_view, unsigned>>;

public:
  void emplace(std::string_view const label, unsigned value) {
    Bucket &bucket = _buckets[hash(label)];

    auto it = std::ranges::find_if(bucket, [label](auto const &p) { return label == p.first; });

    if (it == bucket.end())
      bucket.emplace_back(label, value);
    else
      it->second = value;
  }

  void erase(std::string_view const label) {
    Bucket &bucket = _buckets[hash(label)];

    auto it = std::ranges::find_if(bucket, [label](auto const &p) { return label == p.first; });

    if (it != bucket.end())
      bucket.erase(it);
  }

  [[maybe_unused]] friend std::ostream &operator<<(std::ostream &oss, HashMap const &map) {
    for (size_t i = 0; i < map._buckets.size(); ++i) {
      if (map._buckets[i].empty())
        continue;
      oss << "Box " << i << ": ";
      for (auto const &[k, v] : map._buckets[i]) {
        oss << "[" << k << " " << size_t(v) << "] ";
      }
      oss << "\n";
    }
    return oss;
  }

  [[nodiscard]] size_t focussingPower() const {
    size_t r = 0;
    size_t i = 1;
    for (Bucket const &bucket : _buckets) {
      size_t j = 1;
      for (auto const &[k, v] : bucket) {
        r += i * j * static_cast<size_t>(v);
        ++j;
      }
      ++i;
    }
    return r;
  }

private:
  std::array<Bucket, 256u> _buckets;
};

void performOperation(std::string_view const input, HashMap &map) {
  static RE2 const pattern = R"(([a-z]+)([-=])(\d?))";
  std::string_view label;
  char op = 0;
  std::optional<unsigned> value;
  RE2::FullMatch(input, pattern, &label, &op, &value);

  switch (op) {
  case '=':
    if (!value)
      PANIC(input);
    else
      map.emplace(label, *value);
    break;
  case '-':
    map.erase(label);
    break;
  default:
    PANIC(input);
  };
}

} // namespace

template <> size_t part1<2023, 15>(std::string_view const input) {
  std::vector<std::string_view> inputs = split(input, ',');

  return std::transform_reduce(inputs.begin(), inputs.end(), size_t(0), std::plus<>(),
                               [](std::string_view const s) { return hash(s); });
}

template <> size_t part2<2023, 15>(std::string_view const input) {
  std::vector<std::string_view> inputs = split(input, ',');

  HashMap map;
  for (auto const &in : inputs) {
    performOperation(in, map);
  }

  // std::cout << map;

  return map.focussingPower();
}
