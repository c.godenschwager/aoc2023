#include "LinewiseInput.h"
#include "Parsing.h"
#include "PuzzleImpl.h"

#include <libassert/assert.hpp>

#include <algorithm>
#include <numeric>
#include <vector>

namespace {

template <class InputIt1, class InputIt2>
size_t countingSetIntersection(InputIt1 first1, InputIt1 last1, InputIt2 first2, InputIt2 last2) {
  size_t ctr = 0;
  while (first1 != last1 && first2 != last2) {
    if (*first1 < *first2)
      ++first1;
    else {
      if (!(*first2 < *first1)) {
        ++ctr;
        ++first1;
      }
      ++first2;
    }
  }
  return ctr;
}

size_t checkCard(std::string_view cardString) {
  auto colonIt = std::ranges::find(cardString, ':');
  ASSUME(colonIt != cardString.end());
  auto pipeIt = std::find(colonIt, cardString.end(), '|');
  ASSUME(pipeIt != cardString.end());

  std::string_view const winningNumbersStr(std::next(colonIt), pipeIt);
  std::vector<unsigned> winningNumbers = parseRange<unsigned>(winningNumbersStr);

  std::string_view const ownNumbersStr(std::next(pipeIt), cardString.end());
  std::vector<unsigned> ownNumbers = parseRange<unsigned>(ownNumbersStr);

  std::ranges::sort(winningNumbers);
  std::ranges::sort(ownNumbers);

  return countingSetIntersection(winningNumbers.begin(), winningNumbers.end(), ownNumbers.begin(),
                                 ownNumbers.end());
}

} // namespace

template <> size_t part1<2023, 4>(std::string_view const input) {
  LinewiseInput const lines(input);

  return std::transform_reduce(lines.begin(), lines.end(), size_t(0), std::plus<>(),
                               [](std::string_view const s) {
                                 size_t const matches = checkCard(s);
                                 return matches == 0u ? 0u : (1 << (matches - 1u));
                               });
}

template <> size_t part2<2023, 4>(std::string_view const input) {
  LinewiseInput const lines(input);

  std::vector<size_t> matches(lines.size());

  std::ranges::transform(lines, matches.begin(), checkCard);

  std::vector<size_t> instances(lines.size(), 1u);
  for (size_t i = 0u; i < matches.size(); ++i) {
    for (size_t j = 1u; j <= matches[i] && i + j < instances.size(); ++j) {
      instances[i + j] += instances[i];
    }
  }

  return std::reduce(instances.begin(), instances.end());
}
