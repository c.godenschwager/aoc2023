#include "IntegerCast.h"
#include "LinewiseInput.h"
#include "Parsing.h"
#include "PuzzleImpl.h"

#include <absl/container/flat_hash_map.h>
#include <absl/hash/hash.h>
#include <algorithm>
#include <libassert/assert.hpp>
#include <re2/re2.h>

#include <execution>
#include <ranges>
#include <string_view>
#include <tuple>

namespace {

using ConditionRecord = std::tuple<std::string, std::vector<unsigned>>;

ConditionRecord parseLine(std::string_view const line) {
  auto it = std::ranges::find(line, ' ');
  DEBUG_ASSERT(it != line.end());
  std::string springs(line.begin(), it);
  std::string_view groupsStr(std::next(it), line.end());
  std::vector<unsigned> groups = parseIntegerRange<unsigned>(groupsStr);
  return {std::move(springs), std::move(groups)};
}

std::vector<ConditionRecord> parse(std::string_view const input) {
  LinewiseInput lines(input);
  return std::ranges::to<std::vector<ConditionRecord>>(lines |
                                                       std::ranges::views::transform(parseLine));
}

void unfoldRecord(std::tuple<std::string, std::vector<unsigned>> &r) {
  std::string &springs = std::get<0>(r);
  std::vector<unsigned> &groups = std::get<1>(r);

  auto numSprings = std::ssize(springs);
  auto numGroups = std::ssize(groups);

  springs.reserve(springs.size() * 5u + 4u);
  groups.reserve(groups.size() * 5u);

  for (int i = 0; i < 4; ++i) {
    springs.push_back('?');
    std::copy(springs.begin(), std::next(springs.begin(), numSprings), std::back_inserter(springs));
    std::copy(groups.begin(), std::next(groups.begin(), numGroups), std::back_inserter(groups));
  }
}

struct ConditionRecordPart {
  ConditionRecordPart(ConditionRecord const &r) : springs(std::get<0>(r)), groups(std::get<1>(r)) {}
  ConditionRecordPart(std::string_view const s, std::span<unsigned const> const g)
      : springs(s), groups(g) {}

  std::string_view springs;
  std::span<unsigned const> groups;

  friend bool operator==(ConditionRecordPart const &lhs, ConditionRecordPart const &rhs) {
    return lhs.springs == rhs.springs && std::ranges::equal(lhs.groups, rhs.groups);
  }

  template <typename H> friend H AbslHashValue(H h, ConditionRecordPart const &r) {
    H state = H::combine(std::move(h), r.springs);
    return H::combine_contiguous(std::move(state), r.groups.data(), r.groups.size());
  }
};

class ArrangementCounter {
public:
  size_t count(ConditionRecordPart const &r) const {

    if (auto it = _cache.find(r); it != _cache.end())
      return it->second;

    if (r.springs.empty()) {
      size_t const cnt = r.groups.empty() ? 1u : 0u;
      _cache.emplace(r, cnt);
      return cnt;
    }

    if (r.groups.empty()) {
      size_t const cnt = r.springs.contains('#') ? 0u : 1u;
      _cache.emplace(r, cnt);
      return cnt;
    }

    size_t cnt = 0;
    char const spring = r.springs.front();
    if (spring == '.' || spring == '?') {
      cnt += count({r.springs.substr(1), r.groups});
    }
    if ((spring == '#' || spring == '?') && !r.groups.empty() &&
        canConsumeGroup(r.springs, r.groups.front())) {
      unsigned const consume =
          std::min(r.groups.front() + 1u, integerCast<unsigned>(r.springs.size()));
      cnt += count({r.springs.substr(consume), r.groups.subspan(1)});
    }

    _cache.emplace(r, cnt);
    return cnt;
  }

private:
  static bool canConsumeGroup(std::string_view const s, unsigned groupSize) {
    return s.size() >= groupSize && !s.substr(0, groupSize).contains('.') &&
           (s.size() == groupSize || s[groupSize] != '#');
  }

  using Cache = absl::flat_hash_map<ConditionRecordPart, size_t>;
  mutable Cache _cache;
};

} // namespace

template <> size_t part1<2023, 12>(std::string_view const input) {
  std::vector<ConditionRecord> records = parse(input);

  return std::transform_reduce(std::execution::par, records.begin(), records.end(), size_t(0),
                               std::plus<>(), [](auto const &r) {
                                 ArrangementCounter ctr;
                                 return ctr.count(r);
                               });
}

template <> size_t part2<2023, 12>(std::string_view const input) {
  std::vector<ConditionRecord> records = parse(input);
  std::for_each(std::execution::par, records.begin(), records.end(),
                [](ConditionRecord &r) { unfoldRecord(r); });

  return std::transform_reduce(std::execution::par, records.begin(), records.end(), size_t(0),
                               std::plus<>(), [](auto const &r) {
                                 ArrangementCounter ctr;
                                 return ctr.count(r);
                               });
}
