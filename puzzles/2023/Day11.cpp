#include "Grid2d.h"
#include "LinewiseInput.h"
#include "PuzzleImpl.h"
#include "Vector.h"

#include <libassert/assert.hpp>

#include <algorithm>
#include <string_view>

namespace {

using Grid = Grid2d<char>;
using IndexType = Grid::IndexType;
using Coord = Grid::Coord;

class CoordMapping {
public:
  CoordMapping(Grid const &grid, IndexType const spacing) {
    std::vector<bool> emptyLine(grid.ySize(), true);
    std::vector<bool> emptyColumn(grid.xSize(), true);
    for (IndexType y = 0; y < grid.ySize(); ++y)
      for (IndexType x = 0; x < grid.xSize(); ++x) {
        if (grid[x, y] == '#') {
          emptyLine[y] = false;
          emptyColumn[x] = false;
        }
      }

    for (int x = 0, mappedX = 0; x < grid.xSize(); ++x) {
      _xMapping.push_back(mappedX);
      if (emptyColumn[x])
        mappedX += spacing;
      else
        ++mappedX;
    }
    for (int y = 0, mappedY = 0; y < grid.ySize(); ++y) {
      _yMapping.push_back(mappedY);
      if (emptyLine[y])
        mappedY += spacing;
      else
        ++mappedY;
    }
  }
  [[nodiscard]] Coord operator()(Coord const &c) const {
    return {_xMapping[c.x()], _yMapping[c.y()]};
  }

private:
  std::vector<IndexType> _xMapping;
  std::vector<IndexType> _yMapping;
};

} // namespace

template <> size_t part1<2023, 11>(std::string_view const input) {
  LinewiseInput lines(input);
  Grid2d<char> grid = lines.makeCharGrid2d();

  CoordMapping const mapping(grid, 2);
  std::vector<Coord> galaxies = grid.findAll('#');
  std::ranges::transform(galaxies, galaxies.begin(), mapping);

  int64_t sum = 0;
  for (auto it = galaxies.begin(); it != galaxies.end(); ++it)
    for (auto it2 = std::next(it); it2 != galaxies.end(); ++it2)
      sum += std::abs(it->x() - it2->x()) + std::abs(it->y() - it2->y());

  return integerCast<size_t>(sum);
}

template <> size_t part2<2023, 11>(std::string_view const input) {
  LinewiseInput lines(input);
  Grid2d<char> grid = lines.makeCharGrid2d();
  CoordMapping const mapping(grid, 1000000);
  std::vector<Coord> galaxies = grid.findAll('#');
  std::ranges::transform(galaxies, galaxies.begin(), mapping);

  int64_t sum = 0;
  for (auto it = galaxies.begin(); it != galaxies.end(); ++it)
    for (auto it2 = std::next(it); it2 != galaxies.end(); ++it2)
      sum += std::abs(it->x() - it2->x()) + std::abs(it->y() - it2->y());

  return integerCast<size_t>(sum);
}
