#include "LinewiseInput.h"
#include "PuzzleImpl.h"

#include <re2/re2.h>

#include <libassert/assert.hpp>

#include <cmath>
#include <cstdint>
#include <numeric>
#include <vector>

namespace {

struct Race {
  uint64_t time = 0u;
  uint64_t distance = 0u;
};

std::vector<Race> parseRaces(std::string_view timeStr, std::string_view distanceStr) {
  static RE2 const timePattern = "Time: +";
  static RE2 const distancePattern = "Distance: +";
  static RE2 const numberPattern = R"((\d+) *)";
  ASSUME(RE2::Consume(&timeStr, timePattern));
  ASSUME(RE2::Consume(&distanceStr, distancePattern));

  std::vector<Race> races;
  uint64_t time = 0;
  uint64_t distance = 0;
  while (RE2::Consume(&timeStr, numberPattern, &time)) {
    ASSUME(RE2::Consume(&distanceStr, numberPattern, &distance));
    races.push_back(Race{.time = time, .distance = distance});
  }
  return races;
}

Race parseSingleRace(std::string timeStr, std::string distanceStr) {
  static RE2 const timePattern = R"(Time:(\d+))";
  static RE2 const distancePattern = R"(Distance:(\d+))";

  std::erase(timeStr, ' ');
  std::erase(distanceStr, ' ');

  Race race;
  ASSUME(RE2::FullMatch(timeStr, timePattern, &race.time));
  ASSUME(RE2::FullMatch(distanceStr, distancePattern, &race.distance));

  return race;
}

uint64_t numPossibleRaces(Race const &race) {
  double const b = -static_cast<double>(race.time);
  double const c = static_cast<double>(race.distance); // NOLINT

  double const sqrtTerm = std::sqrt(b * b - 4.0 * c);
  double const x1 = (-b - sqrtTerm) * 0.5;
  double const x2 = (-b + sqrtTerm) * 0.5;

  DEBUG_ASSERT(!std::isnan(x1));
  DEBUG_ASSERT(!std::isnan(x2));

  uint64_t minChargeTime = std::ceil(x1);
  uint64_t maxChargeTime = std::floor(x2);

  uint64_t const distanceTravelled = (race.time - minChargeTime) * minChargeTime;
  if (distanceTravelled == race.distance)
    ++minChargeTime;

  if (distanceTravelled == race.distance)
    --maxChargeTime;

  return maxChargeTime - minChargeTime + 1u;
}

} // namespace

template <> size_t part1<2023, 6>(std::string_view const input) {
  LinewiseInput const lines(input);

  std::vector<Race> const races = parseRaces(lines[0], lines[1]);

  return std::transform_reduce(races.begin(), races.end(), uint64_t(1), std::multiplies<>(),
                               numPossibleRaces);
}

template <> size_t part2<2023, 6>(std::string_view const input) {
  LinewiseInput const lines(input);

  Race race = parseSingleRace(std::string(lines[0]), std::string(lines[1]));

  return numPossibleRaces(race);
}
