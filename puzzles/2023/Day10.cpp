#include "Grid2d.h"
#include "LinewiseInput.h"
#include "PuzzleImpl.h"

#include <libassert/assert.hpp>

#include <cstdint>
#include <iostream>
#include <string_view>
#include <tuple>

namespace {

using Grid = Grid2d<char>;
using IndexType = Grid2d<char>::IndexType;
using Coord = Grid::Coord;

enum Dir : uint8_t { N, S, E, W };

constexpr Coord operator+(Coord const &c, Dir d) {
  switch (d) {
  case N:
    return c + Coord{0, 1};
  case S:
    return c + Coord{0, -1};
  case E:
    return c + Coord{1, 0};
  case W:
    return c + Coord{-1, 0};
  default:
    UNREACHABLE();
  }
}

constexpr Coord &operator+=(Coord &c, Dir const d) {
  switch (d) {
  case N:
    c[1] += 1;
    return c;
  case S:
    c[1] -= 1;
    return c;
  case E:
    c[0] += 1;
    return c;
  case W:
    c[0] -= 1;
    return c;
  default:
    UNREACHABLE();
  }
}

Dir findNextDir(char const curNode, Dir const d) {
  switch (d) {
  case N:
    switch (curNode) {
    case '|':
      return N;
    case 'F':
      return E;
    case '7':
      return W;
    default:
      UNREACHABLE(curNode, d);
    }
    break;
  case S:
    switch (curNode) {
    case '|':
      return S;
    case 'J':
      return W;
    case 'L':
      return E;
    default:
      UNREACHABLE(curNode, d);
    }
    break;
  case E:
    switch (curNode) {
    case '-':
      return E;
    case '7':
      return S;
    case 'J':
      return N;
    default:
      UNREACHABLE(curNode, d);
    }
    break;
  case W:
    switch (curNode) {
    case '-':
      return W;
    case 'F':
      return S;
    case 'L':
      return N;
    default:
      UNREACHABLE(curNode, d);
    }
  }

  return N;
}

bool hasNorthConnection(Grid const &grid, Coord const &c) {
  char const neighborPiece = grid[c + N];
  return neighborPiece == '|' || neighborPiece == 'F' || neighborPiece == '7';
}

bool hasSouthConnection(Grid const &grid, Coord const &c) {
  char const neighborPiece = grid[c + S];
  return neighborPiece == '|' || neighborPiece == 'L' || neighborPiece == 'J';
}

bool hasEastConnection(Grid const &grid, Coord const &c) {
  char const neighborPiece = grid[c + E];
  return neighborPiece == '-' || neighborPiece == 'J' || neighborPiece == '7';
}

bool hasWestConnection(Grid const &grid, Coord const &c) {
  char const neighborPiece = grid[c + E];
  return neighborPiece == '-' || neighborPiece == 'F' || neighborPiece == 'L';
}

Dir findStartDir(Grid const &grid, Coord const &c) {
  if (hasNorthConnection(grid, c))
    return N;
  else if (hasSouthConnection(grid, c))
    return S;
  else if (hasEastConnection(grid, c))
    return E;
  else if (hasWestConnection(grid, c))
    return W;
  else
    UNREACHABLE();

  return N;
}

char findStartPosPiece(Grid const &grid, Coord const &c) {

  bool const n = hasNorthConnection(grid, c);
  bool const s = hasSouthConnection(grid, c);
  if (n && s)
    return '|';
  bool const e = hasEastConnection(grid, c);
  if (n && e)
    return 'L';
  if (s && e)
    return 'F';
  bool const w = hasWestConnection(grid, c);
  if (e && w)
    return '-';
  if (n && w)
    return 'J';
  if (s && w)
    return '7';

  PANIC(c, n, s, e, w);
}

size_t findFarthestAwayPoint(Grid const &grid) {
  Coord c = grid.find('S');
  ASSUME(c.x() < grid.xSize());
  ASSUME(c.y() < grid.ySize());

  Dir d = findStartDir(grid, c);
  size_t ctr = 0;
  ++ctr;
  c += d;

  while (grid[c] != 'S') {
    d = findNextDir(grid[c], d);
    ++ctr;
    c += d;
  }

  return ctr / 2 + ctr % 2;
}

auto markLoopWithX(Grid const &grid) {
  Grid loopGrid(grid.xSize(), grid.ySize(), '.');
  Coord const startPos = grid.find('S');
  ASSUME(startPos.x() < grid.xSize());
  ASSUME(startPos.y() < grid.ySize());

  Coord c = startPos;

  Dir d = findStartDir(grid, c);
  loopGrid[c] = 'X';
  c += d;

  while (grid[c] != 'S') {
    d = findNextDir(grid[c], d);
    loopGrid[c] = 'X';
    c += d;
  }

  return std::make_tuple(std::move(loopGrid), startPos);
}

size_t scanline(Grid const &grid, Grid const &loopGrid) {
  size_t ctr = 0;
  for (IndexType y = 0; y < grid.ySize(); ++y) {
    bool isInner = false;
    bool edgeFromBelow = false;
    for (IndexType x = 0; x < grid.xSize(); ++x) {
      if (loopGrid[x, y] != 'X') {
        // loopGrid[x, y] = isInner ? 'I' : 'O';
        if (isInner)
          ++ctr;
      } else {
        switch (grid[x, y]) {
        case '|':
          isInner = !isInner;
          break;
        case '-':
          continue;
        case 'F':
          edgeFromBelow = true;
          break;
        case 'L':
          edgeFromBelow = false;
          break;
        case 'J':
          if (edgeFromBelow)
            isInner = !isInner;
          break;
        case '7':
          if (!edgeFromBelow)
            isInner = !isInner;
          break;
        default:
          PANIC(x, y, grid[x, y]);
        }
      }
    }
  }
  return ctr;
}

} // namespace

template <> size_t part1<2023, 10>(std::string_view const input) {
  LinewiseInput const lines(input);

  Grid grid = lines.makeCharGrid2dWithGhostLayers('.', 1);
  return findFarthestAwayPoint(grid);
}

template <> size_t part2<2023, 10>(std::string_view const input) {
  LinewiseInput const lines(input);
  Grid grid = lines.makeCharGrid2dWithGhostLayers('.', 1);
  auto [loopGrid, startPos] = markLoopWithX(grid);
  grid[startPos] = findStartPosPiece(grid, startPos);
  return scanline(grid, loopGrid);
}
