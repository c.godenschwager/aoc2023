#include "LinewiseInput.h"
#include "Parsing.h"
#include "PuzzleImpl.h"

#include <absl/container/flat_hash_map.h>
#include <libassert/assert.hpp>
#include <memory>
#include <numeric>
#include <re2/re2.h>

#include <array>
#include <span>
#include <string_view>
#include <tuple>

namespace {

constexpr std::array<unsigned, 128u> makeCategoryIndexMap() {
  std::array<unsigned, 128u> map{};
  map['x'] = 0;
  map['m'] = 1;
  map['a'] = 2;
  map['s'] = 3;
  return map;
}
constexpr std::array<unsigned, 128u> categoryIndexMap = makeCategoryIndexMap();

using PartRatings = std::array<unsigned, 4u>;

struct Rule {
  Rule(std::string_view const ruleStr) {
    static const RE2 pattern = R"(([xmas])([<>])(\d+)\:([a-zAR]+))";

    char category = 0;
    ASSUME(RE2::FullMatch(ruleStr, pattern, &category, &op, &threshold, &acceptedWorkflow));

    idx = categoryIndexMap[category];
  }

  unsigned idx = 0;
  unsigned threshold = 0;
  char op = 0;
  std::string_view acceptedWorkflow;
};

struct Workflow {
  Workflow(std::string_view const workflowStr) {
    auto parts = split(workflowStr);
    defaultWorkflow = parts.back();
    parts.pop_back();
    rules.assign(parts.begin(), parts.end());
  }

  std::vector<Rule> rules;
  std::string_view defaultWorkflow;
};

using Workflows = absl::flat_hash_map<std::string_view, Workflow>;

class Node {
public:
  virtual ~Node() = default;
  [[nodiscard]] virtual bool accept(PartRatings const &p) const = 0;
  [[nodiscard]] virtual size_t acceptedCombinations(PartRatings const &min,
                                                    PartRatings const &max) const = 0;
};

std::unique_ptr<Node> makeNode(Workflows const &wfs, std::string_view const wf);
std::unique_ptr<Node> makeNode(Workflows const &wfs, Workflow const &wf, auto it);

class AcceptingNode final : public Node {
public:
  [[nodiscard]] bool accept(PartRatings const &) const final { return true; }

  [[nodiscard]] size_t acceptedCombinations(PartRatings const &min,
                                            PartRatings const &max) const final {
    return size_t(max[0] - min[0] + 1) * size_t(max[1] - min[1] + 1) * size_t(max[2] - min[2] + 1) *
           size_t(max[3] - min[3] + 1);
  }
};

class RejectingNode final : public Node {
public:
  [[nodiscard]] bool accept(PartRatings const &) const final { return false; }

  [[nodiscard]] size_t acceptedCombinations(PartRatings const &, PartRatings const &) const final {
    return 0;
  };
};

class LessThanNode final : public Node {
public:
  LessThanNode(Workflows const &wfs, Workflow const &wf, auto it)
      : _idx(it->idx), _threshold(it->threshold) {

    ASSUME(it->op == '<');

    _ltNode = makeNode(wfs, it->acceptedWorkflow);

    if (std::next(it) == wf.rules.end())
      _geNode = makeNode(wfs, wf.defaultWorkflow);
    else {
      _geNode = makeNode(wfs, wf, std::next(it));
    }
  }

  [[nodiscard]] bool accept(PartRatings const &p) const final {
    return p[_idx] < _threshold ? _ltNode->accept(p) : _geNode->accept(p);
  }

  [[nodiscard]] size_t acceptedCombinations(PartRatings const &min,
                                            PartRatings const &max) const final {

    PartRatings newMax = max;
    newMax[_idx] = _threshold - 1;
    PartRatings newMin = min;
    newMin[_idx] = _threshold;

    return _ltNode->acceptedCombinations(min, newMax) + _geNode->acceptedCombinations(newMin, max);
  }

private:
  unsigned _idx;
  unsigned _threshold;
  std::unique_ptr<Node> _ltNode;
  std::unique_ptr<Node> _geNode;
};

class GreaterThanNode final : public Node {
public:
  GreaterThanNode(Workflows const &wfs, Workflow const &wf, auto it)
      : _idx(it->idx), _threshold(it->threshold) {

    ASSUME(it->op == '>');

    _gtNode = makeNode(wfs, it->acceptedWorkflow);

    if (std::next(it) == wf.rules.end())
      _leNode = makeNode(wfs, wf.defaultWorkflow);
    else {
      _leNode = makeNode(wfs, wf, std::next(it));
    }
  }

  [[nodiscard]] bool accept(PartRatings const &p) const final {
    return p[_idx] > _threshold ? _gtNode->accept(p) : _leNode->accept(p);
  }

  [[nodiscard]] size_t acceptedCombinations(PartRatings const &min,
                                            PartRatings const &max) const final {

    PartRatings newMax = max;
    newMax[_idx] = _threshold;
    PartRatings newMin = min;
    newMin[_idx] = _threshold + 1;

    return _gtNode->acceptedCombinations(newMin, max) + _leNode->acceptedCombinations(min, newMax);
  }

private:
  unsigned _idx;
  unsigned _threshold;
  std::unique_ptr<Node> _gtNode;
  std::unique_ptr<Node> _leNode;
};

std::unique_ptr<Node> makeNode(Workflows const &wfs, std::string_view const wf) {

  if (wf == "A")
    return std::make_unique<AcceptingNode>();
  else if (wf == "R")
    return std::make_unique<RejectingNode>();
  else {
    auto it = wfs.find(wf);
    ASSUME(it != wfs.end());
    return makeNode(wfs, it->second, it->second.rules.begin());
  }
}

std::unique_ptr<Node> makeNode(Workflows const &wfs, Workflow const &wf, auto it) {
  if (it->op == '<')
    return std::make_unique<LessThanNode>(wfs, wf, it);
  else
    return std::make_unique<GreaterThanNode>(wfs, wf, it);
}

std::unique_ptr<Node> parseWorkflows(std::span<std::string_view const> const input) {
  static const RE2 pattern = R"(([a-z]+){(.*)})";

  absl::flat_hash_map<std::string_view, Workflow> workflows;

  for (std::string_view const line : input) {
    std::string_view name, ruleStr;
    ASSUME(RE2::FullMatch(line, pattern, &name, &ruleStr));
    workflows.emplace(name, Workflow(ruleStr));
  }

  return makeNode(workflows, "in");
}

std::vector<PartRatings> parsePartRatings(std::span<std::string_view const> const input) {
  static const RE2 pattern = R"({x=(\d+),m=(\d+),a=(\d+),s=(\d+)})";

  std::vector<PartRatings> result;
  result.reserve(input.size());

  for (std::string_view const line : input) {
    PartRatings r;
    ASSUME(RE2::FullMatch(line, pattern, &r[0], &r[1], &r[2], &r[3]));
    result.push_back(r);
  }

  return result;
}

auto parse(std::string_view const input) {
  LinewiseInput lines(input);
  std::vector<std::span<std::string_view const>> splitLines = lines.splitOnEmptyLines();
  ASSUME(splitLines.size() == 2u);
  return std::make_tuple(parseWorkflows(splitLines[0]), parsePartRatings(splitLines[1]));
}

} // namespace

template <> size_t part1<2023, 19>(std::string_view const input) {
  auto [workflows, parts] = parse(input);
  size_t sum = 0;
  for (auto const &part : parts) {
    if (workflows->accept(part)) {
      sum += std::reduce(part.begin(), part.end());
    }
  }

  return sum;
}

template <> size_t part2<2023, 19>(std::string_view const input) {
  LinewiseInput lines(input);
  std::vector<std::span<std::string_view const>> splitLines = lines.splitOnEmptyLines();
  auto workflows = parseWorkflows(splitLines[0]);
  return workflows->acceptedCombinations({1, 1, 1, 1}, {4000, 4000, 4000, 4000});
}
