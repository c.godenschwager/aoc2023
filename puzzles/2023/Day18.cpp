#include "Grid2d.h"
#include "LinewiseInput.h"
#include "Parsing.h"
#include "PuzzleImpl.h"

#include <absl/container/flat_hash_set.h>
#include <libassert/assert.hpp>
#include <re2/re2.h>

#include <cstdint>
#include <iostream>
#include <limits>
#include <string_view>

namespace {

using Coord = Vec2<int64_t>;

enum Dir : uint8_t { R = 0, D = 1, L = 2, U = 3 };

Coord stencil(Dir dir) {
  switch (dir) {
  case R:
    return {1, 0};
  case D:
    return {0, -1};
  case L:
    return {-1, 0};
  case U:
    return {0, 1};
  default:
    UNREACHABLE();
  };
}

std::vector<std::pair<Dir, int>> parsePt1(std::string_view const input) {
  LinewiseInput lines(input);

  std::vector<std::pair<Dir, int>> result;
  result.reserve(lines.size());

  std::array<Dir, 128u> dirMap{};
  dirMap['R'] = R;
  dirMap['D'] = D;
  dirMap['L'] = L;
  dirMap['U'] = U;

  static RE2 const pattern = R"(([RLUD]) (\d+) \(#[a-z0-9]{6}\))";

  for (std::string_view line : lines) {
    char d = 0;
    int length = 0;
    ASSUME(RE2::FullMatch(line, pattern, &d, &length));
    result.emplace_back(dirMap[d], length);
  }

  return result;
}

std::vector<std::pair<Dir, int>> parsePt2(std::string_view const input) {
  LinewiseInput lines(input);

  std::vector<std::pair<Dir, int>> result;
  result.reserve(lines.size());

  static RE2 const pattern = R"([RLUD] \d+ \(#([a-z0-9]{5})([0-3])\))";

  for (std::string_view line : lines) {
    std::string_view lengthHexStr;
    int d = 0;
    ASSUME(RE2::FullMatch(line, pattern, &lengthHexStr, &d));
    result.emplace_back(Dir(d), parseHexInt<int>(lengthHexStr));
  }

  return result;
}

struct Horiziontal {
  int line;
  int offset;
  int length;
};

std::vector<Coord> getVertices(std::vector<std::pair<Dir, int>> const &digInstructions) {

  Coord pos{0, 0};

  std::vector<Coord> result;
  result.reserve(digInstructions.size());

  result.push_back(pos);
  for (auto const &instruction : digInstructions) {
    pos += instruction.second * stencil(instruction.first);
    result.push_back(pos);
  }

  return result;
}

size_t gaussianArea(std::vector<Coord> const &coords) {
  int64_t area = 0;
  for (size_t i = 0; i < coords.size() - 1u; ++i) {
    area += (coords[i].y() + coords[i + 1].y()) * (coords[i].x() - coords[i + 1].x());
  }
  area = std::abs(area / 2);

  int64_t perimeter = 0;
  for (size_t i = 0; i < coords.size() - 1u; ++i) {
    perimeter +=
        std::abs(coords[i].y() - coords[i + 1].y()) + std::abs(coords[i].x() - coords[i + 1].x());
  }

  return integerCast<size_t>(area + perimeter / 2 + 1);
}

} // namespace

template <> size_t part1<2023, 18>(std::string_view const input) {
  auto digInstructions = parsePt1(input);
  auto vertices = getVertices(digInstructions);
  return gaussianArea(vertices);
}

template <> size_t part2<2023, 18>(std::string_view const input) {
  auto digInstructions = parsePt2(input);
  auto vertices = getVertices(digInstructions);
  return gaussianArea(vertices);
}
