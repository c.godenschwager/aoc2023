#include "LinewiseInput.h"
#include "Parsing.h"
#include "PuzzleImpl.h"

#include <absl/container/flat_hash_map.h>
#include <algorithm>
#include <libassert/assert.hpp>
#include <re2/re2.h>

#if defined(__GNUC__) && !defined(__clang__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmaybe-uninitialized"
#endif
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/one_bit_color_map.hpp>
#include <boost/graph/stoer_wagner_min_cut.hpp>
#include <boost/property_map/property_map.hpp>
#if defined(__GNUC__) && !defined(__clang__)
#pragma GCC diagnostic pop
#endif

#include <string_view>
#include <vector>

namespace {

using UndirectedGraph = boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS>;

UndirectedGraph parseGraph(std::string_view const input) {
  LinewiseInput lines(input);

  std::vector<std::pair<unsigned, unsigned>> edges;

  absl::flat_hash_map<std::string_view, unsigned> vertices;

  unsigned vertexId = 0;
  for (std::string_view const line : lines) {
    static const RE2 pattern = R"((.{3}): (.+))";
    std::string_view fromVertex;
    std::string_view toVerticesStr;
    ASSUME(RE2::FullMatch(line, pattern, &fromVertex, &toVerticesStr));
    std::vector<std::string_view> toVertices = split(toVerticesStr, ' ');
    if (vertices.emplace(fromVertex, vertexId).second)
      ++vertexId;
    for (auto const toVertex : toVertices) {
      if (vertices.emplace(toVertex, vertexId).second)
        ++vertexId;
      edges.emplace_back(vertices[fromVertex], vertices[toVertex]);
    }
  }

  return {edges.begin(), edges.end(), vertices.size(), edges.size()};
}

} // namespace

template <> size_t part1<2023, 25>(std::string_view const input) {
  UndirectedGraph g = parseGraph(input);
  auto parities = boost::make_one_bit_color_map(num_vertices(g), get(boost::vertex_index, g));
  auto weights = boost::make_constant_property<UndirectedGraph::edge_descriptor>(1);

  int w = boost::stoer_wagner_min_cut(g, weights, boost::parity_map(parities));

  ASSERT(w == 3);
  size_t clusterASize = std::count_if(vertices(g).first, vertices(g).second,
                                      [&](auto const &i) { return get(parities, i) != 0; });
  size_t clusterBSize = num_vertices(g) - clusterASize;
  return clusterASize * clusterBSize;
}

template <> size_t part2<2023, 25>(std::string_view const /*input*/) { return 0; }
