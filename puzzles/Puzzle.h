#pragma once

#include <chrono>
#include <filesystem>
#include <string>
#include <string_view>
#include <vector>

class Puzzle {
public:
  using Day = std::chrono::day;
  using Year = std::chrono::year;

  Puzzle(Year const year_, Day const day_, std::filesystem::path inputFile)
      : _year(year_), _day(day_), _inputFile(std::move(inputFile)) {}

  virtual ~Puzzle() = default;

  [[nodiscard]] Year year() const { return _year; }
  [[nodiscard]] Day day() const { return _day; }
  [[nodiscard]] std::string const name() const;
  [[nodiscard]] std::filesystem::path const &inputFile() const { return _inputFile; }

  virtual std::string runPart1(std::string_view const input) const = 0; // NOLINT
  virtual std::string runPart2(std::string_view const input) const = 0; // NOLINT

  [[nodiscard]] virtual std::string_view solutionPart1() const = 0;
  [[nodiscard]] virtual std::string_view solutionPart2() const = 0;

  [[nodiscard]] static Year latestYear();
  [[nodiscard]] static Day latestDay(Year const year);
  [[nodiscard]] static std::unique_ptr<Puzzle> create(Year const year, Day const day);
  [[nodiscard]] static std::unique_ptr<Puzzle> create(Day const day) {
    return create(latestYear(), day);
  };
  [[nodiscard]] static std::unique_ptr<Puzzle> createLatest() {
    return create(latestDay(latestYear()));
  };
  [[nodiscard]] static std::vector<std::unique_ptr<Puzzle>> createAll();
  [[nodiscard]] static std::vector<std::unique_ptr<Puzzle>> createAll(Year const year);
  [[nodiscard]] static std::vector<std::unique_ptr<Puzzle>> createAllLatestYear() {
    return createAll(latestYear());
  };

private:
  Year _year;
  Day _day;
  std::filesystem::path _inputFile;
};