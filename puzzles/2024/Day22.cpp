#include "PuzzleImpl.h"

#include "IntegerCast.h"
#include "Parsing.h"
#include "Views.h"

#include "experimental/mdspan"
#include "fmt/format.h"
#include "fmt/ranges.h"

#include <algorithm>
#include <cstddef>
#include <map>
#include <set>
#include <string_view>
#include <vector>

using namespace std::literals;

namespace {

uint64_t next(uint64_t n) {
  constexpr uint64_t modulo = 16777216u;
  n ^= n << 6u;
  n %= modulo;
  n ^= n >> 5u;
  n %= modulo;
  n ^= n << 11u;
  return n % modulo;
}

} // namespace

template <> size_t part1<2024, 22>(std::string_view const input) {
  std::vector<uint64_t> secretNumbers = parseIntegerRange<uint64_t>(input, '\n');

  for (auto &n : secretNumbers)
    for (unsigned i = 0; i < 2000; ++i) {
      n = next(n);
    }

  return std::ranges::fold_left(secretNumbers, uint64_t(0), std::plus{});
}

template <> size_t part2<2024, 22>(std::string_view const input) {
  std::vector<uint64_t> secretNumbers = parseIntegerRange<uint64_t>(input, '\n');

  using Extents = std::experimental::extents<int, 19u, 19u, 19u, 19u>;
  std::array<bool, integerCast<std::size_t>(19u * 19u * 19u * 19u)> priceChangesSeenData{};
  std::experimental::mdspan<bool, Extents> priceChangesSeen(priceChangesSeenData.data());
  priceChangesSeen = std::experimental::mdspan<bool, Extents>(&priceChangesSeen[9, 9, 9, 9]);

  std::array<int, integerCast<std::size_t>(19u * 19u * 19u * 19u)> seqPricesData{};
  std::experimental::mdspan<int, Extents> seqPrices(seqPricesData.data());
  seqPrices = std::experimental::mdspan<int, Extents>(&seqPrices[9, 9, 9, 9]);

  for (auto &n : secretNumbers) {
    std::ranges::fill(priceChangesSeenData, false);
    int8_t lastPrice = 0;
    std::array<int8_t, 4u> priceDelta{};
    for (unsigned i = 0; i < 2000; ++i) {
      n = next(n);
      auto const price = integerCast<int8_t>(n % 10);
      std::ranges::rotate(priceDelta, std::next(priceDelta.begin()));
      priceDelta.back() = integerCast<int8_t>(lastPrice - price);
      lastPrice = price;

      if (i > 3 && !priceChangesSeen[priceDelta]) {
        priceChangesSeen[priceDelta] = true;
        seqPrices[priceDelta] += price;
      }
    }
  }
  return std::ranges::max(seqPricesData);
}