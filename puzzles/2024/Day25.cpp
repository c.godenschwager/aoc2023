#include "PuzzleImpl.h"

#include "Grid2d.h"
#include "LinewiseInput.h"
#include "Views.h"

#include <experimental/mdspan>
#include <fmt/format.h>
#include <fmt/ranges.h>

#include <vector>

namespace {
auto parse(std::string_view const input) {
  std::vector<uint32_t> locks;
  std::vector<uint32_t> keys;

  for (auto &&r : input | views::split("\n\n"sv)) {
    Grid2dSpan<char const> g(r.data(), 5, 7, 1, 6);
    uint32_t bits = 0;
    for (int y = 1; y < g.ySize() - 1; ++y)
      for (int x = 0; x < g.xSize(); ++x) {
        bits |= g[x, y] == '#' ? 1u : 0u;
        bits <<= 1u;
      }
    if (g[0, 0] == '#')
      keys.push_back(bits);
    else
      locks.push_back(bits);
  }

  return std::make_tuple(std::move(locks), std::move(keys));
}
} // namespace

template <> std::string solvePart1<2024, 25>(std::string_view const input) {
  auto [locks, keys] = parse(input);
  size_t ctr = 0;
  for (auto l : locks)
    for (auto k : keys)
      if ((l & k) == 0u)
        ++ctr;
  return std::to_string(ctr);
}

template <> std::string solvePart2<2024, 25>(std::string_view const /* input */) { return ""s; }

template <> std::string_view solution1<2024, 25>() { return "2854"sv; }
template <> std::string_view solution2<2024, 25>() { return ""sv; }
