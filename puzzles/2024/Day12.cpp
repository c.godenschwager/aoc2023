#include "PuzzleImpl.h"

#include "Grid2d.h"
#include "LinewiseInput.h"

#include <fmt/format.h>
#include <fmt/ranges.h>

#include <cassert>
#include <iostream>
#include <ranges>
#include <stack>

namespace {

std::array<Vec2<int>, 8u> constexpr dirs{{{0, 1}, {1, 0}, {0, -1}, {-1, 0}}};

} // namespace

template <> size_t part1<2024, 12>(std::string_view const input) {
  Grid2d<char> map = LinewiseInput(input).makeCharGrid2dWithGhostLayers(1, '.');
  Grid2d<unsigned char> visited(map.xSize(), map.ySize(), 0);
  std::stack<Grid2d<char>::Coord> samePlantNeighbors;

  uint64_t sum = 0;
  for (Grid2d<char>::Coord x = {0, 0}; x.y() < map.ySize(); ++x.y())
    for (x.x() = 0; x.x() < map.xSize(); ++x.x()) {
      if (visited[x] == 1)
        continue;
      char const curPlant = map[x];
      uint64_t area = 0;
      uint64_t perimeter = 0;
      samePlantNeighbors.push(x);
      while (!samePlantNeighbors.empty()) {
        auto xx = samePlantNeighbors.top();
        samePlantNeighbors.pop();
        if (visited[xx] == 1)
          continue;
        visited[xx] = 1;
        ++area;
        for (auto d : dirs) {
          auto n = xx + d;
          if (map[n] != curPlant)
            ++perimeter;
          else
            samePlantNeighbors.push(n);
        }
      }
      sum += area * perimeter;
    }

  return sum;
}

template <> size_t part2<2024, 12>(std::string_view const input) {
  Grid2d<char> map = LinewiseInput(input).makeCharGrid2dWithGhostLayers(1, '.');
  Grid2d<unsigned char> visited(map.xSize(), map.ySize(), 0);
  std::stack<Grid2d<char>::Coord> samePlantNeighbors;

  uint64_t sum = 0;
  for (Grid2d<char>::Coord x = {0, 0}; x.y() < map.ySize(); ++x.y())
    for (x.x() = 0; x.x() < map.xSize(); ++x.x()) {
      if (visited[x] == 1)
        continue;
      char const curPlant = map[x];
      uint64_t area = 0;
      uint64_t corners = 0;
      samePlantNeighbors.push(x);
      while (!samePlantNeighbors.empty()) {
        auto xx = samePlantNeighbors.top();
        samePlantNeighbors.pop();
        if (visited[xx] == 1)
          continue;
        visited[xx] = 1;
        ++area;
        for (unsigned i = 0; i < dirs.size(); ++i) {
          unsigned ii = (i + 1) % 4;
          auto n = xx + dirs[i];
          if (map[xx + dirs[i]] == curPlant) {
            samePlantNeighbors.push(n);
            if (map[xx + dirs[ii]] == curPlant && map[xx + dirs[i] + dirs[ii]] != curPlant)
              ++corners; // inner corner
          } else {
            if (map[xx + dirs[ii]] != curPlant)
              ++corners; // outer
          }
        }
      }
      sum += area * corners;
    }

  return sum;
}