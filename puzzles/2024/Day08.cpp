#include "Grid2d.h"
#include "LinewiseInput.h"
#include "PuzzleImpl.h"

#include <libassert/assert.hpp>

#include <cctype>
#include <unordered_map>

namespace {
std::unordered_map<char, std::vector<Vec2<int>>> findAntennas(Grid2d<char> const &map) {
  std::unordered_map<char, std::vector<Vec2<int>>> antennas;
  for (int y = 0; y < map.ySize(); ++y)
    for (int x = 0; x < map.xSize(); ++x)
      if (std::isalnum(map[x, y]))
        antennas[map[x, y]].emplace_back(x, y);
  return antennas;
}

Grid2d<char> findAntinodesPt1(Grid2d<char> const &map,
                              std::unordered_map<char, std::vector<Vec2<int>>> &antennas) {
  Grid2d<char> antinodes(map.xSize(), map.ySize(), '.');
  for (auto const &[frequency, locations] : antennas) {
    for (auto it = locations.begin(); it != locations.end(); ++it)
      for (auto it2 = std::next(it); it2 != locations.end(); ++it2) {
        Vec2<int> const delta = *it2 - *it;
        Vec2<int> candidate0 = *it - delta;
        Vec2<int> candidate1 = *it2 + delta;
        if (antinodes.inBounds(candidate0))
          antinodes[candidate0] = '#';
        if (antinodes.inBounds(candidate1))
          antinodes[candidate1] = '#';
      }
  }
  return antinodes;
}

Grid2d<char> findAntinodesPt2(Grid2d<char> const &map,
                              std::unordered_map<char, std::vector<Vec2<int>>> &antennas) {
  Grid2d<char> antinodes(map.xSize(), map.ySize(), '.');
  for (auto const &[frequency, locations] : antennas) {
    for (auto it = locations.begin(); it != locations.end(); ++it)
      for (auto it2 = std::next(it); it2 != locations.end(); ++it2) {
        Vec2<int> const delta = *it2 - *it;
        for (Vec2<int> pos = *it; antinodes.inBounds(pos); pos -= delta)
          antinodes[pos] = '#';
        for (Vec2<int> pos = *it2; antinodes.inBounds(pos); pos += delta)
          antinodes[pos] = '#';
      }
  }
  return antinodes;
}

} // namespace

template <> size_t part1<2024, 8>(std::string_view const input) {
  Grid2d<char> const map = LinewiseInput(input).makeCharGrid2d();
  std::unordered_map<char, std::vector<Vec2<int>>> antennas = findAntennas(map);
  Grid2d<char> antinodes = findAntinodesPt1(map, antennas);
  return antinodes.count('#');
}

template <> size_t part2<2024, 8>(std::string_view const input) {
  Grid2d<char> const map = LinewiseInput(input).makeCharGrid2d();
  std::unordered_map<char, std::vector<Vec2<int>>> antennas = findAntennas(map);
  Grid2d<char> antinodes = findAntinodesPt2(map, antennas);
  return antinodes.count('#');
  return 0;
}