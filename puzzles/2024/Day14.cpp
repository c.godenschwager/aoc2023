#include "PuzzleImpl.h"

#include "Grid2d.h"
#include "Regex.h"
#include "Vector.h"

#include <fmt/format.h>
#include <fmt/ranges.h>

#include <chrono>
#include <thread>

#include <numeric>

namespace {

constexpr Vec2<int64_t> bathroomSize{101, 103};

struct Robot {
  Vec2<int64_t> p;
  Vec2<int64_t> v;
};

std::vector<Robot> parse(std::string_view const input) {
  std::vector<Robot> result;
  std::string regex = R"(p=(\d+),(\d+) v=(-?\d+),(-?\d+))";
  FindAndConsume<int64_t, int64_t, int64_t, int64_t> consume(regex);
  for (auto [px, py, vx, vy] : consume(input)) {
    result.emplace_back(Vec2<int64_t>{px, py}, Vec2<int64_t>{vx, vy});
  }
  return result;
}

} // namespace

template <> size_t part1<2024, 14>(std::string_view const input) {
  std::vector<Robot> const robots = parse(input);

  constexpr Vec2<int64_t> center = bathroomSize / 2;
  std::array<unsigned, 4u> quadrantCount{};

  constexpr int seconds = 100;

  for (auto &r : robots) {
    Vec2<int64_t> p = ((r.p + seconds * r.v) % bathroomSize + bathroomSize) % bathroomSize;
    if (p.x() > center.x()) {
      if (p.y() > center.y()) {
        ++quadrantCount[0];
      } else if (p.y() < center.y()) {
        ++quadrantCount[1];
      }
    } else if (p.x() < center.x()) {
      if (p.y() > center.y()) {
        ++quadrantCount[2];
      } else if (p.y() < center.y()) {
        ++quadrantCount[3];
      }
    }
  }

  return std::ranges::fold_left(quadrantCount, int64_t(1), std::multiplies{});
}

template <> size_t part2<2024, 14>(std::string_view const) {

  // Code used for pattern evaluation with HI

  // std::vector<Robot> const robots = parse(input);

  // Grid2d<char> pic(bathroomSize, ' ');
  // auto const flippedPicView = pic.flipY();

  // constexpr int fps = 10;
  // constexpr std::chrono::duration<double> frameTime{1.0 / fps};

  // // for (int i = 90; i <= 6888; i += 103) { // horizontal pattern
  // // for (int i = 20; i <= 6888; i += 101) { // vertical pattern
  // for (int i = 0; i <= 6888; ++i) { // all frames
  //   auto const frameStart = std::chrono::steady_clock::now();
  //   pic.fill(' ');
  //   for (auto &r : robots) {
  //     Vec2<int64_t> p = ((r.p + i * r.v) % bathroomSize + bathroomSize) % bathroomSize;
  //     pic[p] = 'X';
  //   }
  //   fmt::println("Time: {}s\n:{}\n\n\n", i, flippedPicView);
  //   std::this_thread::sleep_until(frameStart + frameTime);
  // }

  return 6888;
}