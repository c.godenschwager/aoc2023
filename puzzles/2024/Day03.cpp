#include "PuzzleImpl.h"
#include "Regex.h"

#include <re2/re2.h>

#include <ranges>

namespace {

uint64_t parseMults(std::string_view const s) {
  static FindAndConsume<uint64_t, uint64_t> fac(R"(mul\((\d{1,3}),(\d{1,3})\))");

  auto tupleProduct = [](auto const &t) { return get<0>(t) * get<1>(t); };

  return std::ranges::fold_left(fac(s) | std::views::transform(tupleProduct), uint64_t(0),
                                std::plus{});
}

} // namespace

template <> size_t part1<2024, 3>(std::string_view const input) { return parseMults(input); }

template <> size_t part2<2024, 3>(std::string_view const input) {
  std::string_view s = input;
  uint64_t result = 0;

  RE2::Options o;
  o.set_dot_nl(true);
  RE2 dontPattern(R"((.*?)don't\(\))", o);
  RE2 doPattern(R"(do\(\))");

  bool enabled = true;
  while (enabled) {
    std::string_view enabledMults;
    if (RE2::Consume(&s, dontPattern, &enabledMults)) {
      result += parseMults(enabledMults);
      enabled = RE2::FindAndConsume(&s, doPattern);
    } else {
      result += parseMults(s);
      enabled = false;
    }
  }

  return result;
}
