#include "PuzzleImpl.h"

#include <algorithm>
#include <ranges>
#include <vector>

namespace {

struct File {
  int idx;
  int size;
  int pos;
};

struct FreeSpace {
  int size;
  int pos;
};

} // namespace

template <> size_t part1<2024, 9>(std::string_view const input) {
  int constexpr FREE = -1;

  std::vector<int> deflated;
  int fileIdx = 0;
  for (auto it = input.begin(); it != input.end(); ++it) {
    deflated.insert(deflated.end(), *it++ - '0', fileIdx++);
    if (it == input.end())
      break;
    deflated.insert(deflated.end(), *it - '0', FREE);
  }

  auto notFree = [](int const i) { return i != FREE; };
  auto left = std::ranges::find(deflated, FREE);
  auto right = std::ranges::find_last_if(deflated, notFree).begin();

  while (left < right) {
    std::iter_swap(left, right);
    left = std::ranges::find(std::next(left), deflated.end(), FREE);
    right = std::ranges::find_last_if(deflated.begin(), right, notFree).begin();
  }

  size_t checksum = 0;
  for (size_t i = 0; i < deflated.size(); ++i) {
    if (deflated[i] != FREE)
      checksum += i * static_cast<size_t>(deflated[i]);
  }

  return checksum;
}

template <> size_t part2<2024, 9>(std::string_view const input) {
  std::vector<File> fileList;
  std::vector<FreeSpace> freeList;

  int fileIdx = 0;
  int pos = 0;
  for (auto it = input.begin(); it != input.end(); ++it) {
    int size = *it++ - '0';
    fileList.emplace_back(fileIdx++, size, pos);
    pos += size;
    if (it == input.end())
      break;
    size = *it - '0';
    if (size > 0) {
      freeList.emplace_back(size, pos);
      pos += size;
    }
  }

  size_t checksum = 0;
  for (auto &file : fileList | std::views::reverse) {
    auto it = freeList.begin();
    while (it->pos < file.pos && it->size < file.size)
      ++it;

    if (it->pos < file.pos) {
      file.pos = it->pos;
      it->pos += file.size;
      it->size -= file.size;
    }
    if (it->size == 0)
      freeList.erase(it);

    for (int i = 0; i < file.size; ++i)
      checksum += static_cast<size_t>((file.pos + i)) * static_cast<size_t>(file.idx);
  }

  return checksum;
}