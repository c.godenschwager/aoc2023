#include "PuzzleImpl.h"

#include "Grid2d.h"
#include "LinewiseInput.h"
#include "Parsing.h"
#include "Views.h"

#include <queue>
#include <ranges>
#include <string_view>
#include <vector>

using namespace std::literals;

namespace {
int constexpr mapSize = 71;
int constexpr numBytes = 1024;

using VertexId = Vec2<int>;
int constexpr numDirections = 4;
std::array<Vec2<int>, numDirections> constexpr dirs{{{0, 1}, {1, 0}, {0, -1}, {-1, 0}}};

struct VertexScore {
  constexpr bool operator<(VertexScore const &other) const { return score > other.score; }

  VertexId id;
  int64_t score;
};

VertexId constexpr InvalidVertexId{-1, -1};

Grid2d<char> parseMap(std::string_view const input) {
  Grid2d<char> map(mapSize, mapSize, '#', 1);
  map.fill('.');
  LinewiseInput linewise(input);
  for (auto l : linewise | views::take(numBytes)) {
    auto v = parseIntegerRange<int>(l);
    map[v[0], v[1]] = '#';
  }

  return map;
}

auto parseMap2(std::string_view const input) {
  Grid2d<char> map(mapSize, mapSize, '#', 1);
  map.fill('.');
  LinewiseInput linewise(input);
  for (auto l : linewise | views::take(numBytes)) {
    auto v = parseIntegerRange<int>(l);
    map[v[0], v[1]] = '#';
  }

  std::vector<Vec2<int>> remainingBytes;
  for (auto l : linewise | views::drop(numBytes)) {
    auto v = parseIntegerRange<int>(l);
    remainingBytes.emplace_back(v[0], v[1]);
  }

  return std::make_tuple(std::move(map), std::move(remainingBytes));
}

} // namespace

template <> std::string solvePart1<2024, 18>(std::string_view const input) {
  Grid2d<char> const map = parseMap(input);

  VertexId const start{0, 0};
  VertexId const goal{mapSize - 1, mapSize - 1};

  Grid2d<VertexId> pred(map.xSize(), map.ySize(), InvalidVertexId);
  Grid2d<int64_t> gScore(map.xSize(), map.ySize(), std::numeric_limits<int64_t>::max());
  Grid2d<int64_t> fScore(map.xSize(), map.ySize(), std::numeric_limits<int64_t>::max());
  std::priority_queue<VertexScore> queue;
  std::vector<VertexId> shortestPath;

  auto h = [&](VertexId const &v) {
    return std::abs(v.x() - goal.x()) + std::abs(v.y() - goal.y());
  };

  auto reconstructPath = [&](VertexId v) {
    shortestPath.clear();
    while (v != start) {
      shortestPath.push_back(v);
      ASSUME(pred[v] != InvalidVertexId);
      v = pred[v];
    }
  };

  gScore[start] = 0;
  fScore[start] = h(start);
  queue.emplace(start, fScore[start]);

  while (!queue.empty()) {
    auto [cur, oldFscore] = queue.top();
    queue.pop();
    if (oldFscore != fScore[cur])
      continue;

    if (cur == goal) {
      reconstructPath(cur);
      break;
    }

    for (auto d : dirs) {
      VertexId neighbor = cur + d;
      if (map[neighbor] == '#')
        continue;
      int64_t tentiative_gScore = gScore[cur] + 1;
      if (tentiative_gScore < gScore[neighbor]) {
        pred[neighbor] = cur;
        gScore[neighbor] = tentiative_gScore;
        fScore[neighbor] = tentiative_gScore + h(neighbor);
        queue.emplace(neighbor, fScore[neighbor]);
      }
    }
  }

  return std::to_string(shortestPath.size());
}

template <> std::string solvePart2<2024, 18>(std::string_view const input) {
  auto [map, remainingBytes] = parseMap2(input);

  VertexId const start{0, 0};
  VertexId const goal{mapSize - 1, mapSize - 1};

  Grid2d<VertexId> pred(map.xSize(), map.ySize(), InvalidVertexId);
  Grid2d<int64_t> gScore(map.xSize(), map.ySize(), std::numeric_limits<int64_t>::max());
  Grid2d<int64_t> fScore(map.xSize(), map.ySize(), std::numeric_limits<int64_t>::max());
  std::priority_queue<VertexScore> queue;
  std::vector<VertexId> shortestPath;

  auto h = [&](VertexId const &v) {
    return std::abs(v.x() - goal.x()) + std::abs(v.y() - goal.y());
  };

  auto reconstructPath = [&](VertexId v) {
    shortestPath.clear();
    while (v != start) {
      shortestPath.push_back(v);
      ASSUME(pred[v] != InvalidVertexId);
      v = pred[v];
    }
  };

  auto left = remainingBytes.begin();
  auto right = remainingBytes.end();
  while (left != right) {
    auto it = std::next(left, std::distance(left, right) / 2);

    Grid2d<char> updatedMap = map;
    for (auto it2 = remainingBytes.begin(); it2 <= it; ++it2)
      updatedMap[*it2] = '#';

    pred.fill(InvalidVertexId);
    gScore.fill(std::numeric_limits<int64_t>::max());
    fScore.fill(std::numeric_limits<int64_t>::max());
    shortestPath.clear();

    gScore[start] = 0;
    fScore[start] = h(start);
    queue.emplace(start, fScore[start]);

    while (!queue.empty()) {
      auto [cur, oldFscore] = queue.top();
      queue.pop();
      if (oldFscore != fScore[cur])
        continue;

      if (cur == goal) {
        reconstructPath(cur);
        break;
      }

      for (auto d : dirs) {
        VertexId neighbor = cur + d;
        if (updatedMap[neighbor] == '#')
          continue;
        int64_t tentiative_gScore = gScore[cur] + 1;
        if (tentiative_gScore < gScore[neighbor]) {
          pred[neighbor] = cur;
          gScore[neighbor] = tentiative_gScore;
          fScore[neighbor] = tentiative_gScore + h(neighbor);
          queue.emplace(neighbor, fScore[neighbor]);
        }
      }
    }
    if (shortestPath.empty())
      right = it;
    else
      left = std::next(it);
  }

  return fmt::format("{},{}", left->x(), left->y());
}

template <> std::string_view solution1<2024, 18>() { return "276"sv; }
template <> std::string_view solution2<2024, 18>() { return "60,37"sv; }