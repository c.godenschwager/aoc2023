#include "PuzzleImpl.h"

#include "Grid2d.h"
#include "LinewiseInput.h"

#include "fmt/format.h"
#include "fmt/ranges.h"

#include <algorithm>
#include <string_view>
#include <vector>

using namespace std::literals;

namespace {

std::vector<Vec2<int>> findPath(Grid2d<char> const &map) {
  static std::array<Vec2<int>, 4u> constexpr dirs{{{0, 1}, {1, 0}, {0, -1}, {-1, 0}}};

  std::vector<Vec2<int>> path;

  Vec2<int> pos = map.find('S');
  Vec2<int> predPos{-1, -1};
  path.push_back(pos);

  while (map[pos] != 'E') {
    for (auto d : dirs) {
      auto n = pos + d;
      if (n != predPos && map[n] != '#') {
        predPos = pos;
        pos = n;
        path.push_back(pos);
        break;
      }
    }
  }

  return path;
}

struct PosIdx {
  Vec2<int> pos;
  int idx;
};

size_t countCheats(Grid2d<char> const &map, int const radius, int const minSaving) {
  std::vector<Vec2<int>> path = findPath(map);

  using ParcelGrid = Grid2d<std::vector<PosIdx>>;
  Vec2<int> constexpr parcelSize{5, 5};
  Vec2<int> parcelRadius = (radius + (parcelSize - 1)) / parcelSize;
  ParcelGrid parcels((map.size() + (parcelSize - 1)) / parcelSize, {},
                     std::max(parcelRadius.x(), parcelRadius.y()));

  for (int i = 0; i < std::ssize(path); ++i)
    parcels[path[i] / parcelSize].emplace_back(path[i], i);

  size_t ctr = 0;

  for (ParcelGrid::Coord p1{0, 0}; p1.y() < parcels.ySize(); ++p1.y())
    for (p1.x() = 0; p1.x() < parcels.xSize(); ++p1.x()) {
      // for each parcel p1

      // handle local neighbors
      auto const &parcel = parcels[p1];
      for (auto itStart = parcel.begin(); itStart != parcel.end(); ++itStart)
        for (auto itEnd = std::next(itStart); itEnd != parcel.end(); ++itEnd) {
          int const cheatLength = (itEnd->pos - itStart->pos).l1norm();
          if (cheatLength <= radius) {
            auto saving = (itEnd->idx - itStart->idx) - cheatLength;
            if (saving >= minSaving)
              ++ctr;
          }
        }

      // Handle neighbors in neighboring parcels
      for (ParcelGrid::Coord p2 = p1; p2.y() <= p1.y() + parcelRadius.y(); ++p2.y())
        for (p2.x() = (p2.y() == p1.y() ? p1.x() + 1 : p1.x() - parcelRadius.x());
             p2.x() <= p1.x() + parcelRadius.x(); ++p2.x()) {
          // for each parcel p2 in the neighborhood of p1 (located after p1 in grid order)

          for (auto const &pp1 : parcels[p1])
            for (auto const &pp2 : parcels[p2]) {
              auto pathDist = std::abs(pp2.idx - pp1.idx);
              auto cheatLength = (pp2.pos - pp1.pos).l1norm();
              if (cheatLength <= radius && pathDist - cheatLength >= minSaving)
                ++ctr;
            }
        }
    }

  return ctr;
}

} // namespace

template <> size_t part1<2024, 20>(std::string_view const input) {
  Grid2d<char> const map = LinewiseInput(input).makeCharGrid2d();
  return countCheats(map, 2, 100);
}

template <> size_t part2<2024, 20>(std::string_view const input) {
  Grid2d<char> const map = LinewiseInput(input).makeCharGrid2d();
  return countCheats(map, 20, 100);
}