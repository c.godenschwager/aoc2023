#include "Grid2d.h"
#include "LinewiseInput.h"
#include "Parsing.h"
#include "PuzzleImpl.h"

#include <fmt/format.h>
#include <fmt/ranges.h>

#include <libassert/assert.hpp>

#include <ranges>
#include <vector>

namespace {
using Rules = Grid2d<char>;
using Update = std::vector<int>;
using Updates = std::vector<Update>;

Rules parseRules(std::string_view const input) {
  Grid2d<char> rules(100, 100, 0);

  auto tuples = input | std::views::split('\n') | std::views::transform([](auto &&r) {
                  return r | std::views::split('|') |
                         std::views::transform([](auto &&s) { return parseInt<int>(s); }) |
                         std::views::adjacent<2>;
                }) |
                std::views::join;

  for (auto [x, y] : tuples) {
    rules[x, y] = 1;
  }

  return rules;
}

Updates parseUpdates(std::string_view const input) {
  Updates updates;

  for (auto &&line : input | std::views::split('\n')) {
    updates.push_back(parseIntegerRange<int>(std::string_view{line}));
  }

  return updates;
}

std::tuple<Rules, std::vector<Update>> parse(std::string_view const input) {

  auto breaks = std::ranges::search(input, std::string_view("\n\n"));

  ASSUME(!breaks.empty());

  return std::make_tuple(parseRules({input.begin(), breaks.begin()}),
                         parseUpdates({breaks.end(), input.end()}));
}

bool isValid(Update const &update, Rules const &rules) {
  for (auto it = update.begin(); it != update.end(); ++it) {
    for (auto it2 = std::next(it); it2 != update.end(); ++it2) {
      if (rules[*it2, *it] == 1)
        return false;
    }
  }
  return true;
}

void sort(Update &update, Rules const &rules) {
  for (auto it = update.begin(); it != update.end(); ++it) {
    for (auto it2 = std::next(it); it2 != update.end(); ++it2) {
      if (rules[*it2, *it] == 1) {
        std::iter_swap(it, it2);
        it2 = it;
      }
    }
  }
}

int middleElement(Update const &update) { return update[update.size() / 2]; }

} // namespace

template <> size_t part1<2024, 5>(std::string_view const input) {
  auto [rules, updates] = parse(input);

  uint64_t sum = 0;
  for (auto const &u : updates) {
    if (isValid(u, rules))
      sum += middleElement(u);
  }
  return sum;
}

template <> size_t part2<2024, 5>(std::string_view const input) {
  auto [rules, updates] = parse(input);

  uint64_t sum = 0;
  for (auto &u : updates) {
    if (!isValid(u, rules)) {
      sort(u, rules);
      sum += middleElement(u);
    }
  }
  return sum;
}