#include "PuzzleImpl.h"

#include "Regex.h"
#include "Vector.h"

#include <numeric>

namespace {

struct ClawMachine {
  Vec2<int64_t> a;
  Vec2<int64_t> b;
  Vec2<int64_t> p;
};

std::vector<ClawMachine> parse(std::string_view const input, Vec2<int64_t> add = {0, 0}) {
  std::vector<ClawMachine> result;
  std::string regex =
      R"(Button A\: X\+(\d+), Y\+(\d+)\nButton B\: X\+(\d+), Y\+(\d+)\nPrize\: X=(\d+), Y=(\d+))";
  FindAndConsume<int64_t, int64_t, int64_t, int64_t, int64_t, int64_t> consume(regex);
  for (auto [ax, ay, bx, by, px, py] : consume(input)) {
    result.emplace_back(Vec2<int64_t>{ax, ay}, Vec2<int64_t>{bx, by}, Vec2<int64_t>{px, py} + add);
  }
  return result;
}

size_t solve(ClawMachine const &machine) {
  Vec2<double> const a{machine.a};
  Vec2<double> const b{machine.b};
  Vec2<double> const p{machine.p};

  Vec2<double> const n =
      Vec2<double>{b.y() * p.x() - b.x() * p.y(), a.x() * p.y() - a.y() * p.x()} /
      (a.x() * b.y() - a.y() * b.x());

  Vec2<int64_t> in{static_cast<int64_t>(std::round(n.x())),
                   static_cast<int64_t>(std::round(n.y()))};

  if (in.x() * machine.a + in.y() * machine.b == machine.p)
    return in.x() * 3 + in.y();
  else
    return 0;
}

} // namespace

template <> size_t part1<2024, 13>(std::string_view const input) {
  auto machines = parse(input);
  return std::transform_reduce(machines.begin(), machines.end(), size_t(0), std::plus{}, solve);
}

template <> size_t part2<2024, 13>(std::string_view const input) {
  auto machines = parse(input, Vec2<int64_t>(10000000000000, 10000000000000));
  return std::transform_reduce(machines.begin(), machines.end(), size_t(0), std::plus{}, solve);
}