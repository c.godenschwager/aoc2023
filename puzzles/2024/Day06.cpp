#include "Grid2d.h"
#include "LinewiseInput.h"
#include "PuzzleImpl.h"

#include <fmt/format.h>
#include <fmt/ostream.h>
#include <fmt/ranges.h>

#include <algorithm>

namespace {

std::array<Vec2<int>, 4u> constexpr dirs{{{0, 1}, {1, 0}, {0, -1}, {-1, 0}}};

struct Guard {

  [[nodiscard]] Grid2d<char>::Coord next() const { return pos + dirs[dirIdx]; }
  void turn() { dirIdx = (dirIdx + 1) % 4; }
  void move() { pos = pos + dirs[dirIdx]; }

  Grid2d<char>::Coord pos;
  int dirIdx;
};

void moveGuard(Guard &guard, Grid2d<char> const &grid) {
  if (grid[guard.next()] == '#')
    guard.turn();
  else
    guard.move();
}

bool guardToPath(Grid2d<unsigned char> &path, Guard const &guard) {
  unsigned int const mask = 1 << guard.dirIdx;
  unsigned char &v = path[guard.pos];
  if ((v & mask) != 0u)
    return false;

  v = v | mask;
  return true;
}

bool pathHasCycle(Guard guard, Grid2d<char> const &grid, Grid2d<unsigned char> path) {
  for (; grid[guard.pos] != '*'; moveGuard(guard, grid)) {
    if (!guardToPath(path, guard))
      return true;
  }
  return false;
}

} // namespace

template <> size_t part1<2024, 6>(std::string_view const input) {
  LinewiseInput linewise(input);
  Grid2d<char> grid = linewise.makeCharGrid2dWithGhostLayers(1, '*');

  for (Guard guard{.pos = grid.find('^'), .dirIdx = 0}; grid[guard.pos] != '*';
       moveGuard(guard, grid)) {
    grid[guard.pos] = 'X';
  }

  return grid.count('X');
}

template <> size_t part2<2024, 6>(std::string_view const input) {
  LinewiseInput linewise(input);
  Grid2d<char> grid = linewise.makeCharGrid2dWithGhostLayers(1, '*');
  Grid2d<unsigned char> path(grid.xSize(), grid.ySize());

  int cnt = 0;
  for (Guard guard{.pos = grid.find('^'), .dirIdx = 0}; grid[guard.pos] != '*';
       moveGuard(guard, grid)) {
    if (grid[guard.next()] == '.') {
      grid[guard.next()] = '#';
      if (pathHasCycle(guard, grid, path))
        ++cnt;
      grid[guard.next()] = '.';
    }
    guardToPath(path, guard);
    grid[guard.pos] = 'X';
  }

  return cnt;
}