#include "LinewiseInput.h"
#include "Parsing.h"
#include "PuzzleImpl.h"

#include <algorithm>
#include <ranges>

namespace {

std::vector<std::vector<int>> parse(std::string_view const input) {
  return input | std::views::split('\n') | std::views::transform([](auto &&r) {
           return r | std::views::split(' ') |
                  std::views::transform([](auto &&r) { return parseInt<int>(r); });
         }) |
         std::ranges::to<std::vector<std::vector<int>>>();
}

bool isSafe(std::ranges::input_range auto &&r) {
  auto differences = r | std::views::pairwise_transform(std::minus{});
  if (differences.front() > 0)
    return std::ranges::all_of(differences, [](int const d) { return d >= 1 && d <= 3; });
  else
    return std::ranges::all_of(differences, [](int const d) { return d >= -3 && d <= -1; });
}

bool isSafeWithTolerance(std::ranges::input_range auto &&r) {
  if (isSafe(r))
    return true;

  for (int i = 0; i < std::ranges::ssize(r); ++i) {
    auto filtered = r | std::views::enumerate |
                    std::views::filter([i](auto const &t) { return get<0>(t) != i; }) |
                    std::views::values;
    if (isSafe(filtered))
      return true;
  }
  return false;
}

} // namespace

template <> size_t part1<2024, 2>(std::string_view const input) {
  auto v = parse(input);
  return std::ranges::count_if(v, [](auto &&r) { return isSafe(r); });
}

template <> size_t part2<2024, 2>(std::string_view const input) {
  auto v = parse(input);
  return std::ranges::count_if(v, [](auto &&r) { return isSafeWithTolerance(r); });
}
