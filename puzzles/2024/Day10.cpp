#include "PuzzleImpl.h"

#include "Grid2d.h"
#include "LinewiseInput.h"

#include <fmt/format.h>
#include <fmt/ostream.h>

#include <algorithm>
#include <ranges>
#include <stack>

namespace {
std::array<Vec2<int>, 4u> constexpr dirs{{{0, 1}, {1, 0}, {0, -1}, {-1, 0}}};
} // namespace

template <> size_t part1<2024, 10>(std::string_view const input) {
  Grid2d<char> map = LinewiseInput(input).makeCharGrid2dWithGhostLayers(1, '.');

  uint64_t cnt = 0;
  std::stack<Grid2d<char>::Coord> stack;
  for (auto const &trailhead : map.findAll('0')) {
    std::unordered_set<Grid2d<char>::Coord> trailends;
    stack.push(trailhead);
    while (!stack.empty()) {
      auto c = stack.top();
      stack.pop();
      if (map[c] == '9') {
        trailends.insert(c);
      } else {
        for (auto const d : dirs) {
          auto n = c + d;
          if (map[n] == map[c] + 1) {
            stack.push(n);
          }
        }
      }
    }
    cnt += trailends.size();
  }

  return cnt;
}

template <> size_t part2<2024, 10>(std::string_view const input) {
  Grid2d<char> map = LinewiseInput(input).makeCharGrid2dWithGhostLayers(1, '.');

  uint64_t cnt = 0;
  std::stack<Grid2d<char>::Coord> stack;
  for (auto const &trailhead : map.findAll('0')) {
    stack.push(trailhead);
    while (!stack.empty()) {
      auto c = stack.top();
      stack.pop();
      if (map[c] == '9') {
        ++cnt;
      } else {
        for (auto const d : dirs) {
          auto n = c + d;
          if (map[n] == map[c] + 1) {
            stack.push(n);
          }
        }
      }
    }
  }

  return cnt;
}