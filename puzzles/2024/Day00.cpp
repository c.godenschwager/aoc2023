#include "PuzzleImpl.h"

#include "Grid2d.h"
#include "IntegerCast.h"
#include "LinewiseInput.h"
#include "Parsing.h"
#include "Views.h"

#include <fmt/format.h>
#include <fmt/ranges.h>
#include <re2/re2.h>

#include <algorithm>
#include <map>
#include <set>
#include <string_view>
#include <unordered_map>
#include <unordered_set>
#include <vector>

namespace {}

template <> std::string solvePart1<2024, 0>(std::string_view const input) {
  return std::string{input};
}

template <> std::string solvePart2<2024, 0>(std::string_view const input) {
  return std::string{input};
}

template <> std::string_view solution1<2024, 0>() { return ""sv; }
template <> std::string_view solution2<2024, 0>() { return ""sv; }
