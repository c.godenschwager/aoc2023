#include "PuzzleImpl.h"

#include "Views.h"

#include "fmt/format.h"
#include "fmt/ranges.h"

#include <algorithm>
#include <map>
#include <ranges>
#include <string_view>
#include <vector>

using namespace std::literals;

namespace {

auto parse(std::string_view const input) {
  auto lines = input | views::split('\n');

  return std::make_tuple(lines.front() | views::split(", "sv) | views::transform([](auto &&r) {
                           return std::string_view(r);
                         }) | std::ranges::to<std::vector<std::string_view>>(),
                         lines | views::drop(2) | views::transform([](auto &&r) {
                           return std::string_view(r);
                         }) | std::ranges::to<std::vector<std::string_view>>());
}

struct Tester {

  Tester(std::vector<std::string_view> const &prefixes) : _prefixes(&prefixes) {}

  size_t operator()(std::string_view word) {
    if (word.empty())
      return true;

    if (auto it = _cache.find(word); it != _cache.end())
      return it->second;

    auto prefixFind = [&](std::string_view prefix) { return word.starts_with(prefix); };

    auto it = std::ranges::find_if(*_prefixes, prefixFind);

    size_t ctr = 0;
    while (it != _prefixes->end()) {
      ctr += (*this)(word | views::drop(it->size()));

      ++it;

      it = std::ranges::find_if(it, _prefixes->end(), prefixFind);
    }

    _cache.emplace(word, ctr);
    return ctr;
  }

  std::vector<std::string_view> const *_prefixes;
  std::map<std::string_view, size_t> _cache;
};

} // namespace

template <> size_t part1<2024, 19>(std::string_view const input) {
  auto [prefixes, words] = parse(input);
  Tester t(prefixes);

  return std::ranges::count_if(words, [&](auto w) { return t(w) > 0; });
}

template <> size_t part2<2024, 19>(std::string_view const input) {

  auto [prefixes, words] = parse(input);
  Tester t(prefixes);

  auto counts = words | views::transform([&](auto w) { return t(w); });

  return std::ranges::fold_left(counts, size_t(0), std::plus{});
}