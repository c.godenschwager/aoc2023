#include "PuzzleImpl.h"

#include "IntegerCast.h"
#include "Parsing.h"

#include <fmt/format.h>
#include <fmt/ranges.h>

#include <numeric>
#include <ranges>
#include <unordered_map>

namespace {

std::optional<std::pair<uint64_t, uint64_t>> split(uint64_t const x) {
  static std::array<char, std::numeric_limits<uint64_t>::digits10> buffer;

  auto result = std::to_chars(buffer.data(), buffer.data() + buffer.size(), x);
  ASSERT(result.ec == std::errc{});
  std::string_view const s{buffer.data(), result.ptr};

  if (s.size() % 2u != 0)
    return std::nullopt;

  std::pair<uint64_t, uint64_t> splitValues;

  ASSERT(std::from_chars(s.data(), s.data() + s.size() / 2u, splitValues.first).ec == std::errc{});
  ASSERT(std::from_chars(s.data() + s.size() / 2u, s.data() + s.size(), splitValues.second).ec ==
         std::errc{});

  return {splitValues};
}

size_t run(std::string_view const input, size_t const times) {
  std::unordered_map<uint64_t, uint64_t> line, tmp;

  for (auto const &s : input | std::views::split(' ') |
                           std::views::transform([](auto &&r) { return parseInt<uint64_t>(r); }))
    ++line[s];

  for (size_t i = 0; i < times; ++i) {
    for (auto &[key, value] : tmp)
      value = 0u;

    for (auto const &[x, count] : line) {
      if (x == 0) {
        tmp[1] += count;
      } else if (auto splitResult = split(x)) {
        tmp[splitResult->first] += count;
        tmp[splitResult->second] += count;
      } else {
        tmp[x * 2024u] += count;
      }
    }

    line.swap(tmp);
  }

  return std::transform_reduce(line.begin(), line.end(), uint64_t(0), std::plus{},
                               [&](auto const &p) { return p.second; });
}

} // namespace

template <> size_t part1<2024, 11>(std::string_view const input) { return run(input, 25u); }

template <> size_t part2<2024, 11>(std::string_view const input) { return run(input, 75u); }