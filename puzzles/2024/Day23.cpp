#include "PuzzleImpl.h"

#include "Grid2d.h"
#include "Views.h"

#include <boost/dynamic_bitset.hpp>
#include <fmt/format.h>
#include <fmt/ranges.h>
#include <re2/re2.h>

#include <algorithm>
#include <string_view>
#include <vector>

namespace {

using Vertex = uint16_t;

[[maybe_unused]] std::string toString(Vertex v) {
  std::string s = "  "s;
  s[0] = static_cast<char>('a' + (v % 26));
  s[1] = static_cast<char>('a' + (v / 26));
  return s;
}

Vertex fromString(std::string_view const s) { return (s[0] - 'a') + ((s[1] - 'a') * 26); }

std::pair<Vertex, Vertex> parseEdge(std::string_view const s) {
  static RE2 const pattern = R"(([a-z]{2})-([a-z]{2}))";
  std::string_view v0, v1;
  RE2::FullMatch(s, pattern, &v0, &v1);
  return {fromString(v0), fromString(v1)};
}

auto parse(std::string_view const input) {
  std::vector<Vertex> vertices;
  Grid2d<uint8_t> adjacency(fromString("zz") + 1, fromString("zz") + 1, 0u);

  for (std::string_view const s : input | views::splitLines) {
    auto [v0, v1] = parseEdge(s);
    vertices.push_back(v0);
    vertices.push_back(v1);
    adjacency[v0, v1] = 1u;
    adjacency[v1, v0] = 1u;
  }
  std::ranges::sort(vertices);
  vertices.erase(std::ranges::unique(vertices).begin(), vertices.end());

  return std::make_tuple(std::move(vertices), std::move(adjacency));
}

class BronKerbosch {
private:
  using VertexSet = boost::dynamic_bitset<>;

public:
  BronKerbosch(std::vector<Vertex> const &vertices, Grid2d<uint8_t> const &adjacency)
      : _vertices(vertices), _adjMap(_vertices.size(), VertexSet(_vertices.size())) {
    for (unsigned i = 0; i < _vertices.size(); ++i)
      for (unsigned j = i + 1; j < _vertices.size(); ++j)
        if (adjacency[_vertices[i], _vertices[j]]) {
          _adjMap[i][j] = true;
          _adjMap[j][i] = true;
        }
  }

  std::vector<Vertex> operator()() const {

    VertexSet allVertices(_vertices.size());
    allVertices.set();
    VertexSet maximumClique;
    run(VertexSet(_vertices.size()), allVertices, VertexSet(_vertices.size()), maximumClique);

    std::vector<Vertex> resultVertices;
    resultVertices.reserve(maximumClique.count());
    for (size_t i = maximumClique.find_first(); i < maximumClique.size();
         i = maximumClique.find_next(i))
      resultVertices.push_back(_vertices[i]);
    return resultVertices;
  }

private:
  void run(VertexSet const &r, VertexSet p, VertexSet x, VertexSet &maximumClique) const {
    size_t const pCount = p.count();
    size_t const rCount = r.count();
    size_t const maximumCliqueCount = maximumClique.count();
    if (rCount + pCount <= maximumCliqueCount)
      return;
    if (pCount == 0u && x.none()) {
      if (rCount > maximumCliqueCount)
        maximumClique = r;
      return;
    }
    for (size_t i = p.find_first(); i < p.size(); i = p.find_next(i)) {
      VertexSet rNew = r;
      rNew.set(i);
      run(rNew, p & _adjMap[i], x & _adjMap[i], maximumClique);
      p.reset(i);
      x.set(i);
    }
  }

  std::vector<Vertex> _vertices;
  std::vector<VertexSet> _adjMap;
};

} // namespace

template <> std::string solvePart1<2024, 23>(std::string_view const input) {
  auto [vertices, adjacency] = parse(input);

  auto notStartingWithT = std::ranges::partition(vertices, [](Vertex const v) {
    constexpr uint16_t t = 't' - 'a';
    return v % 26 == t;
  });

  uint64_t ctr = 0;
  for (auto it0 = vertices.begin(); it0 != notStartingWithT.begin(); ++it0)
    for (auto it1 = std::next(it0); it1 != vertices.end(); ++it1)
      if (adjacency[*it0, *it1])
        for (auto it2 = std::next(it1); it2 != vertices.end(); ++it2)
          if (adjacency[*it0, *it2] && adjacency[*it1, *it2])
            ++ctr;

  return std::to_string(ctr);
}

template <> std::string solvePart2<2024, 23>(std::string_view const input) {
  auto const [vertices, adjacency] = parse(input);

  BronKerbosch bk(vertices, adjacency);

  std::vector<Vertex> maxClique = bk();

  auto maxCliqueStr = maxClique | views::transform([](Vertex const v) { return toString(v); }) |
                      std::ranges::to<std::vector<std::string>>();
  std::ranges::sort(maxCliqueStr);

  return maxCliqueStr | views::join_with(',') | std::ranges::to<std::string>();
}

template <> std::string_view solution1<2024, 23>() { return "1327"sv; }

template <> std::string_view solution2<2024, 23>() {
  return "df,kg,la,mp,pb,qh,sk,th,vn,ww,xp,yp,zk"sv;
}
