#include "PuzzleImpl.h"

#include "Parsing.h"
#include "Views.h"

#include "fmt/format.h"
#include "fmt/ranges.h"

#include <re2/re2.h>

#include <algorithm>
#include <cstddef>
#include <ranges>
#include <string_view>

using namespace std::literals;

namespace {

class Computer {
public:
  Computer(uint64_t const a, uint64_t const b, uint64_t const c,
           std::vector<uint64_t> const &program)
      : _a(a), _b(b), _c(c), _program(program) {}

  [[nodiscard]] auto const &output() const { return _output; }
  [[nodiscard]] auto const &program() const { return _program; }

  std::vector<uint64_t> const &run() { return run(_a); }

  std::vector<uint64_t> const &run(uint64_t const a) {
    _a = a;
    _ip = 0;
    _output.clear();
    while (_ip < _program.size())
      step();

    return output();
  }

private:
  uint64_t op() {
    ASSUME(_ip < _program.size());
    return _program[_ip];
  }

  void step() {
    switch (op()) {
    case 0: // adv
      _a >>= comboOperand();
      _ip += 2;
      break;
    case 1: // bxl
      _b ^= literalOperand();
      _ip += 2;
      break;
    case 2: // bst
      _b = comboOperand() % 8u;
      _ip += 2;
      break;
    case 3: // jnz
      if (_a == 0)
        _ip += 2;
      else
        _ip = literalOperand();
      break;
    case 4: // bxc
      _b ^= _c;
      _ip += 2;
      break;
    case 5: // out
      _output.push_back(comboOperand() % 8u);
      _ip += 2;
      break;
    case 6: // bdv
      _b = _a >> comboOperand();
      _ip += 2;
      break;
    case 7: // cdv
      _c = _a >> comboOperand();
      _ip += 2;
      break;
    default:
      UNREACHABLE(op());
    }
  }

  uint64_t literalOperand() {
    ASSUME(_ip + 1u < _program.size());
    return _program[_ip + 1u];
  }

  uint64_t comboOperand() {
    ASSUME(_ip + 1u < _program.size());
    uint64_t const val = _program[_ip + 1u];
    switch (val) {
    case 0:
    case 1:
    case 2:
    case 3:
      return val;
    case 4:
      return _a;
    case 5:
      return _b;
    case 6:
      return _c;
    default:
      UNREACHABLE(val);
    }
  }

  uint64_t _a = 0;
  uint64_t _b = 0;
  uint64_t _c = 0;
  uint64_t _ip = 0;
  std::vector<uint64_t> _program;
  std::vector<uint64_t> _output;
};

Computer parse(std::string_view const input) {
  RE2 regex =
      R"(Register A: (\d+)\nRegister B: (\d+)\nRegister C: (\d+)\n\nProgram: ((?:\d+,)*\d+))"sv;

  uint64_t a = 0;
  uint64_t b = 0;
  uint64_t c = 0;
  std::string_view programStr;

  RE2::FullMatch(input, regex, &a, &b, &c, &programStr);

  return {a, b, c, parseIntegerRange<uint64_t>(programStr)};
}

} // namespace

template <> std::string solvePart1<2024, 17>(std::string_view const input) {
  Computer c = parse(input);
  c.run();

  return c.output() | views::toStringAndJoinWith(",") | std::ranges::to<std::string>();
}

template <> std::string solvePart2<2024, 17>(std::string_view const input) {
  Computer c = parse(input);

  uint64_t a = 0;
  for (auto it = c.program().rbegin(); it != c.program().rend();) {
    uint64_t i = a & 0b111;
    for (; i < 8u; ++i, ++a) {
      if (c.run(a).front() == *it) {
        break;
      }
    }
    if (i < 8u) {
      ++it;
      if (it != c.program().rend())
        a <<= 3u;
    } else {
      ASSUME(it != c.program().rbegin());
      --it;
      a >>= 3u;
    }
  }

  return std::to_string(a);
}

template <> std::string_view solution1<2024, 17>() { return "1,3,7,4,6,4,2,3,5"sv; }
template <> std::string_view solution2<2024, 17>() { return "202367025818154"sv; }