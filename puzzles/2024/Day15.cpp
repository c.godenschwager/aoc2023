#include "PuzzleImpl.h"

#include "AsciiMap.h"
#include "Grid2d.h"
#include "LinewiseInput.h"

#include <libassert/assert.hpp>

#include <algorithm>
#include <ranges>
#include <set>
#include <string_view>
#include <tuple>

using namespace std::literals;

using Coord = Grid2d<char>::Coord;

namespace {

constexpr AsciiMap<Vec2<int>> makeMovementMap() {
  AsciiMap<Vec2<int>> map{{0, 0}};
  map['^'] = {0, 1};
  map['v'] = {0, -1};
  map['<'] = {-1, 0};
  map['>'] = {1, 0};
  return map;
};

constexpr AsciiMap<Vec2<int>> movementMap = makeMovementMap();

auto parse(std::string_view const input) {
  auto const r = std::ranges::search(input, "\n\n"sv);
  std::string_view const gridStr{input.begin(), r.begin()};
  std::string_view const gridMovement{r.end(), input.end()};

  return std::make_tuple(LinewiseInput(gridStr).makeCharGrid2d(),
                         gridMovement | std::views::filter([](char const c) { return c != '\n'; }) |
                             std::views::transform([&](char const c) { return movementMap[c]; }) |
                             std::ranges::to<std::vector<Vec2<int>>>());
}

void moveSimple(Grid2d<char> &map, Coord &robotPos, Vec2<int> const &move) {
  Coord pos = robotPos;
  while (map[pos] != '#' && map[pos] != '.')
    pos += move;

  if (map[pos] == '#')
    return;

  ASSUME(map[pos] == '.');

  for (; pos != robotPos; pos -= move)
    std::swap(map[pos], map[pos - move]);

  robotPos += move;
}

class VerticalMover {
public:
  VerticalMover(Grid2d<char> &map) : _map(&map), _columnStart(map.xSize()) {
    _curActiveColumns.reserve(map.xSize());
  }

  Coord operator()(Coord robotPos, Vec2<int> const move) {
    ASSUME(move.y() != 0);
    Grid2d<char> &map = *_map;

    std::ranges::fill(_columnStart, -1);
    std::set<int> activeColumns;
    activeColumns.insert(robotPos.x());
    _columnStart[robotPos.x()] = robotPos.y();
    int y = robotPos.y();

    _pendingShifts.clear();

    while (!activeColumns.empty()) {
      int nextY = y + move.y();
      _curActiveColumns.assign(activeColumns.begin(), activeColumns.end());
      for (int const x : _curActiveColumns) {
        switch (map[x, nextY]) {
        case '#':
          return robotPos;
        case '.':
          ASSUME(_columnStart[x] != -1);
          _pendingShifts.emplace_back(Coord{x, _columnStart[x]}, Coord{x, nextY});
          _columnStart[x] = -1;
          activeColumns.erase(x);
          break;
        case '[':
          if (activeColumns.insert(x + 1).second) {
            ASSUME(_columnStart[x + 1] == -1);
            _columnStart[x + 1] = nextY;
          }
          break;
        case ']':
          if (activeColumns.insert(x - 1).second) {
            ASSUME(_columnStart[x - 1] == -1);
            _columnStart[x - 1] = nextY;
          }
          break;
        default:
          UNREACHABLE(map[x, nextY]);
        }
      }
      y = nextY;
    }

    for (auto const &c : _pendingShifts)
      for (auto pos = c.second; pos != c.first; pos -= move)
        std::swap(map[pos], map[pos - move]);

    return robotPos + move;
  }

private:
  Grid2d<char> *_map;
  std::vector<int> _columnStart;
  std::vector<std::pair<Coord, Coord>> _pendingShifts;
  std::vector<int> _curActiveColumns;
};

Grid2d<char> widenMap(Grid2d<char> const &map) {
  Grid2d<char> widenedMap(map.xSize() * 2, map.ySize());

  for (int y = 0; y < map.ySize(); ++y)
    for (int x = 0; x < map.xSize(); ++x)
      switch (map[x, y]) {
      case '@':
        widenedMap[x * 2, y] = '@';
        widenedMap[x * 2 + 1, y] = '.';
        break;
      case 'O':
        widenedMap[x * 2, y] = '[';
        widenedMap[x * 2 + 1, y] = ']';
        break;
      default:
        widenedMap[x * 2, y] = map[x, y];
        widenedMap[x * 2 + 1, y] = map[x, y];
      }
  return widenedMap;
}

} // namespace

template <> size_t part1<2024, 15>(std::string_view const input) {
  auto [map, moves] = parse(input);

  auto robotPos = map.find('@');

  for (auto m : moves)
    moveSimple(map, robotPos, m);

  return std::ranges::fold_left(
      map.flipY().findAll('O') | std::views::transform([&](auto c) { return c.y() * 100 + c.x(); }),
      int64_t(0), std::plus{});
}

template <> size_t part2<2024, 15>(std::string_view const input) {
  auto [map, moves] = parse(input);

  map = widenMap(map);

  auto robotPos = map.find('@');

  VerticalMover verticalMover(map);

  for (auto move : moves) {
    if (move.y() == 0)
      moveSimple(map, robotPos, move);
    else
      robotPos = verticalMover(robotPos, move);
  }

  return std::ranges::fold_left(
      map.flipY().findAll('[') | std::views::transform([&](auto c) { return c.y() * 100 + c.x(); }),
      int64_t(0), std::plus{});
}