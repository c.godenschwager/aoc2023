#include "Grid2d.h"
#include "LinewiseInput.h"
#include "PuzzleImpl.h"

#include <algorithm>

template <> size_t part1<2024, 4>(std::string_view const input) {
  LinewiseInput linewise(input);
  Grid2d<char> grid = linewise.makeCharGrid2dWithGhostLayers(3, '.');

  std::array<Grid2d<char>::Coord, 8u> constexpr dirs = {
      {{1, 0}, {0, 1}, {-1, 0}, {0, -1}, {1, 1}, {-1, -1}, {1, -1}, {-1, 1}}};

  std::string_view constexpr word = "XMAS";

  uint64_t count = 0;
  for (Grid2d<char>::Coord c{0, 0}; c.y() < grid.ySize(); ++c.y())
    for (c.x() = 0; c.x() < grid.xSize(); ++c.x())
      if (grid[c] == 'X') {
        count += std::ranges::count_if(dirs, [&](auto const &d) {
          for (int i = 1; i < 4; ++i)
            if (grid[c + i * d] != word[i])
              return false;
          return true;
        });
      }

  return count;
}

template <> size_t part2<2024, 4>(std::string_view const input) {
  LinewiseInput linewise(input);
  Grid2d<char> grid = linewise.makeCharGrid2d();

  uint64_t cnt = 0;
  for (int y = 1; y < grid.ySize() - 1; ++y)
    for (int x = 1; x < grid.xSize() - 1; ++x)
      if (grid[x, y] == 'A' &&
          ((grid[x - 1, y - 1] == 'M' && grid[x + 1, y + 1] == 'S') ||
           (grid[x - 1, y - 1] == 'S' && grid[x + 1, y + 1] == 'M')) &&
          ((grid[x - 1, y + 1] == 'M' && grid[x + 1, y - 1] == 'S') ||
           (grid[x - 1, y + 1] == 'S' && grid[x + 1, y - 1] == 'M'))) {
        ++cnt;
      }

  return cnt;
}