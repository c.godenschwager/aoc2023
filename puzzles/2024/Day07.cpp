#include "LinewiseInput.h"
#include "Parsing.h"
#include "PuzzleImpl.h"

#include <libassert/assert.hpp>

#include <algorithm>

namespace {

struct Calibration {
  uint64_t result;
  std::vector<uint64_t> operands;
};

auto parse(std::string_view const input) {
  std::vector<Calibration> result;
  for (auto l : getLineWise(input)) {
    auto it = std::ranges::find(l, ':');
    ASSERT(it != l.end());
    std::string_view resultStr(l.begin(), it);
    std::string_view numberStr(std::next(it, 2), l.end());

    result.push_back({parseInt<uint64_t>(resultStr), parseIntegerRange<uint64_t>(numberStr, ' ')});
  }

  return result;
}

uint64_t concat(uint64_t const lhs, uint64_t const rhs) {
  uint64_t n = 1;
  while (n <= rhs)
    n *= 10u;
  return lhs * n + rhs;
}

bool isValidPt1(uint64_t const expectedResult, uint64_t const intermediateResult,
                std::span<uint64_t const> const operands) {

  if (intermediateResult > expectedResult)
    return false;
  if (operands.empty())
    return intermediateResult == expectedResult;

  std::span<uint64_t const> const tail{std::next(operands.begin()), operands.end()};
  return isValidPt1(expectedResult, intermediateResult * operands.front(), tail) ||
         isValidPt1(expectedResult, intermediateResult + operands.front(), tail);
}

bool isValidPt2(uint64_t const expectedResult, uint64_t const intermediateResult,
                std::span<uint64_t const> const operands) {

  if (intermediateResult > expectedResult)
    return false;
  if (operands.empty())
    return intermediateResult == expectedResult;

  std::span<uint64_t const> const tail{std::next(operands.begin()), operands.end()};
  return isValidPt2(expectedResult, intermediateResult * operands.front(), tail) ||
         isValidPt2(expectedResult, intermediateResult + operands.front(), tail) ||
         isValidPt2(expectedResult, concat(intermediateResult, operands.front()), tail);
}

} // namespace

template <> size_t part1<2024, 7>(std::string_view const input) {
  std::vector<Calibration> calibrations = parse(input);

  uint64_t result = 0;
  for (auto &c : calibrations) {
    if (isValidPt1(c.result, c.operands.front(), {std::next(c.operands.begin()), c.operands.end()}))
      result += c.result;
  }
  return result;
}

template <> size_t part2<2024, 7>(std::string_view const input) {
  std::vector<Calibration> calibrations = parse(input);

  uint64_t result = 0;
  for (auto &c : calibrations) {
    if (isValidPt2(c.result, c.operands.front(), {std::next(c.operands.begin()), c.operands.end()}))
      result += c.result;
  }
  return result;
}