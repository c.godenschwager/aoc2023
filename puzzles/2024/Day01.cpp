#include "LinewiseInput.h"
#include "PuzzleImpl.h"

#include <bits/ranges_algo.h>
#include <functional>
#include <libassert/assert.hpp>
#include <ranges>
#include <re2/re2.h>

#include <fmt/format.h>
#include <fmt/ranges.h>

#include <algorithm>
#include <numeric>
#include <tuple>

namespace {

std::array<int, 2u> parse(std::string_view const s) {
  static RE2 const pattern = R"((\d+)   (\d+))";
  std::array<int, 2u> numbers{0, 0};
  ASSUME(RE2::FullMatch(s, pattern, &numbers[0], &numbers[1]));
  return numbers;
}

std::array<std::vector<int>, 2u> parseAndSortVectors(std::string_view const input) {
  std::vector<int> left, right;

  for (auto [l, r] : getLineWise(input) | std::views::transform(parse)) {
    left.push_back(l);
    right.push_back(r);
  }

  std::ranges::sort(left);
  std::ranges::sort(right);

  return {std::move(left), std::move(right)};
}

} // namespace

template <> size_t part1<2024, 1>(std::string_view const input) {
  auto [left, right] = parseAndSortVectors(input);

  return std::ranges::fold_left(
      std::views::zip_transform([](int const l, int const r) { return std::abs(l - r); }, left,
                                right),
      size_t(0), std::plus{});
}

template <> size_t part2<2024, 1>(std::string_view const input) {
  auto [left, right] = parseAndSortVectors(input);

  return std::ranges::fold_left(
      left | std::views::chunk_by(std::ranges::equal_to{}) |
          std::views::transform([rIt = right.begin(), &right](auto &&chunkFromLeft) mutable {
            int const l = *std::ranges::begin(chunkFromLeft);
            auto chunkFromRight = std::ranges::equal_range(rIt, right.end(), l);
            rIt = std::ranges::end(chunkFromRight);
            return l * std::ranges::size(chunkFromLeft) * std::ranges::size(chunkFromRight);
          }),
      size_t(0), std::plus{});
}
