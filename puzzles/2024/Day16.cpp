#include "PuzzleImpl.h"

#include "Grid2d.h"
#include "LinewiseInput.h"

#include <experimental/mdspan>

#include <algorithm>
#include <cstddef>
#include <queue>
#include <ranges>
#include <set>
#include <string_view>

using namespace std::literals;

namespace {

using VertexId = std::array<int, 3u>;

int constexpr numDirections = 4;
std::array<Vec2<int>, numDirections> constexpr dirs{{{0, 1}, {1, 0}, {0, -1}, {-1, 0}}};

constexpr VertexId turnRight(VertexId const vid) {
  auto [x, y, d] = vid;
  return {x, y, d == 3 ? 0 : d + 1};
}

constexpr VertexId turnLeft(VertexId const vid) {
  auto [x, y, d] = vid;
  return {x, y, d == 0 ? 3 : d - 1};
}

constexpr VertexId moveForward(VertexId const vid) {
  auto [x, y, d] = vid;
  return {x + dirs[d].x(), y + dirs[d].y(), d};
}

struct VertexScore {
  constexpr bool operator<(VertexScore const &other) const { return score > other.score; }

  VertexId id;
  int score;
};

VertexId startVertex(Grid2d<char> const &map) {
  auto [sourceX, sourceY] = map.find('S');
  return {sourceX, sourceY, 1};
}

std::array<VertexId, 4u> endVertices(Grid2d<char> const &map) {
  auto [endX, endY] = map.find('E');
  return {{{endX, endY, 0}, {endX, endY, 1}, {endX, endY, 2}, {endX, endY, 3}}};
}

} // namespace

template <> size_t part1<2024, 16>(std::string_view const input) {
  Grid2d<char> const map = LinewiseInput(input).makeCharGrid2d();
  std::vector<int> distData(static_cast<size_t>(map.xSize() * map.ySize() * numDirections),
                            std::numeric_limits<int>::max());
  std::experimental::mdspan const dist(distData.data(), map.xSize(), map.ySize(), numDirections);

  VertexId const source = startVertex(map);

  std::priority_queue<VertexScore> queue;

  dist[source] = 0;
  queue.emplace(source, 0);

  while (!queue.empty()) {
    auto [u, p] = queue.top();
    queue.pop();
    if (p != dist[u])
      continue;
    {
      auto v = moveForward(u);
      if (map[v[0], v[1]] != '#') {
        int alt = dist[u] + 1;
        if (alt < dist[v]) {
          dist[v] = alt;
          queue.emplace(v, alt);
        }
      }
    }
    for (auto v : {turnLeft(u), turnRight(u)}) {
      int alt = dist[u] + 1000;
      if (alt < dist[v]) {
        dist[v] = alt;
        queue.emplace(v, alt);
      }
    }
  }

  return std::ranges::min(endVertices(map) |
                          std::views::transform([&](auto const &v) { return dist[v]; }));
}

template <> size_t part2<2024, 16>(std::string_view const input) {
  Grid2d<char> const map = LinewiseInput(input).makeCharGrid2d();
  std::vector<int> distData(static_cast<size_t>(map.xSize() * map.ySize() * numDirections),
                            std::numeric_limits<int>::max());
  std::vector<std::vector<VertexId>> predData(
      static_cast<size_t>(map.xSize() * map.ySize() * numDirections));
  std::experimental::mdspan const dist(distData.data(), map.xSize(), map.ySize(), numDirections);
  std::experimental::mdspan const pred(predData.data(), map.xSize(), map.ySize(), numDirections);

  VertexId const source = startVertex(map);

  std::priority_queue<VertexScore> queue;

  dist[source] = 0;
  queue.emplace(source, 0);

  while (!queue.empty()) {
    auto [u, p] = queue.top();
    queue.pop();
    if (p != dist[u])
      continue;
    {
      auto v = moveForward(u);
      if (map[v[0], v[1]] != '#') {
        int alt = dist[u] + 1;
        if (alt < dist[v]) {
          pred[v].assign(1u, u);
          dist[v] = alt;
          queue.emplace(v, alt);
        } else if (alt == dist[v]) {
          pred[v].push_back(u);
        }
      }
    }
    for (auto v : {turnLeft(u), turnRight(u)}) {
      int alt = dist[u] + 1000;
      if (alt < dist[v]) {
        pred[v].assign(1u, u);
        dist[v] = alt;
        queue.emplace(v, alt);
      } else if (alt == dist[v]) {
        pred[v].push_back(u);
      }
    }
  }

  auto ev = endVertices(map);
  int const minScore =
      std::ranges::min(ev | std::views::transform([&](auto const &v) { return dist[v]; }));

  std::set<Vec2<int>> goodSeats;
  std::queue<VertexId> q;

  for (auto const &e : ev) {
    if (dist[e] == minScore)
      q.push(e);
  }

  while (!q.empty()) {
    VertexId cur = q.front();
    q.pop();
    goodSeats.emplace(cur[0], cur[1]);

    for (VertexId p : pred[cur])
      q.push(p);
  }

  return goodSeats.size();
}