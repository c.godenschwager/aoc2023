#include "Puzzle.h"
#include "TextBuffer.h"

#include <gtest/gtest.h>

#include <algorithm>

class PuzzleTest : public testing::Test {
public:
  PuzzleTest(std::shared_ptr<Puzzle> const &puzzle)
      : _puzzle(puzzle), _input(_puzzle->inputFile()) {}

protected:
  Puzzle const &puzzle() { return *_puzzle; }
  TextBuffer const &input() { return _input; }

private:
  std::shared_ptr<Puzzle> _puzzle;
  TextBuffer _input;
};

class PuzzleTestPt1 : public PuzzleTest {
public:
  using PuzzleTest::PuzzleTest;

  void TestBody() override {
    std::string const result = puzzle().runPart1(input().getText());
    ASSERT_EQ(result, puzzle().solutionPart1());
  }
};

class PuzzleTestPt2 : public PuzzleTest {
public:
  using PuzzleTest::PuzzleTest;

  void TestBody() override {
    std::string const result = puzzle().runPart2(input().getText());
    ASSERT_EQ(result, puzzle().solutionPart2());
  }
};

void registerTests() {
  std::vector<std::unique_ptr<Puzzle>> puzzles = Puzzle::createAll();
  std::vector<std::shared_ptr<Puzzle>> sharedPuzzles;
  std::ranges::move(puzzles, std::back_inserter(sharedPuzzles));

  for (auto const &p : sharedPuzzles) {
    std::string suiteName = p->name();
    testing::RegisterTest(suiteName.c_str(), "Pt1", nullptr, nullptr, __FILE__, __LINE__,
                          // Important to use the fixture type as the return type here.
                          [=]() -> PuzzleTest * { return new PuzzleTestPt1(p); });

    testing::RegisterTest(suiteName.c_str(), "Pt2", nullptr, nullptr, __FILE__, __LINE__,
                          // Important to use the fixture type as the return type here.
                          [=]() -> PuzzleTest * { return new PuzzleTestPt2(p); });
  }
}

int main(int argc, char **argv) {

  testing::InitGoogleTest(&argc, argv);
  registerTests();
  return RUN_ALL_TESTS();
}