file(GLOB_RECURSE sources CONFIGURE_DEPENDS 20[12][0-9]/*.cpp)

add_library(puzzles)
target_sources(puzzles PRIVATE Puzzle.cpp ${sources})
target_include_directories(puzzles PUBLIC .)
target_link_libraries(puzzles PUBLIC aoclib hash-library::hash-library Boost::graph)

add_executable(puzzle_tests)
target_sources(puzzle_tests PRIVATE Tests.cpp)
target_link_libraries(puzzle_tests PUBLIC puzzles GTest::gtest)
include(GoogleTest)
gtest_discover_tests(puzzle_tests)
