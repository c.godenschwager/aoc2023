#pragma once

#include <cstddef>
#include <string_view>

using namespace std::literals;

template <int year, int day> size_t part1(std::string_view const input);
template <int year, int day> size_t part2(std::string_view const input);

template <int year, int day> std::string solvePart1(std::string_view const input);
template <int year, int day> std::string solvePart2(std::string_view const input);
template <int year, int day> std::string_view solution1();
template <int year, int day> std::string_view solution2();